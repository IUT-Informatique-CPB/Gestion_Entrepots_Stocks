# Projet Entrepôt - GStock

## Pour tester l'application :

Utiliser le compte :
> Login: 4pi
> Mot de passe: 4pi

#### Pour lancer la dernière version de l'application sans IDE (vous devez seulement avoir jre 1.8) :
### LINUX UNIQUEMENT
Se deplacer dans le dossier runGstock :
```bash
cd runGstock
```
Lancer le serveur :
```bash
./serveur.sh
```
Lancer 1 ou (ou plusieurs) client(s) :
```bash
./client.sh
```

#### Pourquoi linux uniquement ?

> Car il faut ajouter les drivers au CLASSPATH nécessaire pour la connexion à la base de données, cela reste possible sous windows mais nous avons eu des difficultés pour le mettre en place

##### Deux script de création de la base de données (un script avec une BD vide et un autre avec une BD remplie) est aussi présent dans le dossier ./BD

### Les développeurs :

* Bascouzaraix Julien
* Colin Simon
* Dumartin Lucas
* Dupuy Sylvain
* Pacaud-Boehm Corentin
* Trochut Nicolas
