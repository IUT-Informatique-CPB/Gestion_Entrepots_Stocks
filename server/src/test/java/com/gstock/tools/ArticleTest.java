package com.gstock.tools;


import com.gstock.tools.article.Article;
import com.gstock.tools.article.ArticleCategory;
import com.gstock.tools.article.ArticleFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.*;

public class ArticleTest {
    @Test
    public void articleTest() {
        ArticleCategory ac = new ArticleCategory("1234567891021", "Pates", 100,100,100);
        Article a1 = new Article(1, 2, "01/01/2001", "01/01/2001", ac );
        a1.setId(2);
        Article a2 = new Article(1,1, 2, "01/01/2001", "01/01/2001", ac );

        assertNotNull(a1.show());
        assertNotNull(a1.toString());
        assertEquals(a1.getArticleCategory(), ac);
        assertTrue(a1.compareTo(a2) > 0);
    }

}
