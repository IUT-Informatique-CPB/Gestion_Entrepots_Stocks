package com.gstock.tools;

import com.gstock.tools.order.Order;
import com.gstock.tools.order.OrderFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;

public class OrderTest {
    @Before
    public void init() {

    }

    @Test
    public void simpleDataBaseAccess( ) {
        Order order = OrderFactory.getOrder(2);
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, order.toString());
        assertEquals(order.getId(), 2);
    }
}
