package com.gstock.tools;

import com.gstock.tools.article.Article;
import com.gstock.tools.article.ArticleFactory;
import org.junit.Test;

import java.util.logging.Level;
import java.util.logging.Logger;

import static org.junit.Assert.assertEquals;

public class ArticleFactoryTest {

    @Test
    public void simpleDataBaseAccess( ) {
        Tool.activateDebug();
        Tool.setDebugMode(0);

        Article article = ArticleFactory.getArticle(2);
        Logger.getLogger(this.getClass().getName()).log(Level.INFO, article.toString());
        assertEquals(article.getId(), 2);
    }
}
