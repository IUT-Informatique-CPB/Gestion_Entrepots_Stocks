package com.gstock.tools;


import com.gstock.tools.address.Address;
import org.junit.Test;

import java.util.TreeSet;

public class ToolTest {
    @Test
    public  void test(){
        TreeSet<Address> addresses = new TreeSet<>();
        addresses.add(new Address(20,0, 1,1,2,"","" ));
        addresses.add(new Address(21,0, 2,1,2,"","" ));
        addresses.add(new Address(22,0, 3,1,2,"","" ));
        addresses.add(new Address(23,0, 1,2,2,"","" ));
        addresses.add(new Address(24,0, 2,2,2,"","" ));
        addresses.add(new Address(25,0, 3,2,2,"","" ));
        addresses.add(new Address(26,0, 1,3,2,"","" ));
        addresses.add(new Address(27,0, 2,3,2,"","" ));
        addresses.add(new Address(28,0, 3,3,2,"","" ));
        addresses.add(new Address(10,0, 1,1,1,"","" ));
        addresses.add(new Address(11,0, 2,1,1,"","" ));
        addresses.add(new Address(12,0, 3,1,1,"","" ));
        addresses.add(new Address(13,0, 1,2,1,"","" ));
        addresses.add(new Address(14,0, 2,2,1,"","" ));
        addresses.add(new Address(15,0, 3,2,1,"","" ));
        addresses.add(new Address(16,0, 1,3,1,"","" ));
        addresses.add(new Address(17,0, 2,3,1,"","" ));
        addresses.add(new Address(18,0, 3,3,1,"","" ));

        for (Address a : addresses)
            System.out.println(a.show());
    }

    @Test
    public void testRandom() {
        for (int i = 0; i < 4; i++)
            System.out.println(randomBarCode());
    }

    public String randomBarCode() {
        double i = Math.round(Math.random() * 10000000000000d);
        return String.format ("%13.0f", i).replaceAll("\\s", "0");
    }
}
