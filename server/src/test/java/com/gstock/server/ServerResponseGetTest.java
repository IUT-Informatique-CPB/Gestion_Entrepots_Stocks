package com.gstock.server;

import com.gstock.tools.Exception.RequestException;
import com.gstock.tools.Request;
import com.gstock.tools.Tool;
import com.gstock.tools.address.Address;
import com.gstock.tools.article.Article;
import com.gstock.tools.article.ArticleCategory;
import com.gstock.tools.deposit.Deposit;
import com.gstock.tools.order.Order;
import org.junit.Test;

import java.io.IOException;
import java.util.*;
import java.util.logging.Level;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ServerResponseGetTest extends ServerResponseTest {
    @Test
    public void testGAC() throws IOException, ClassNotFoundException, InterruptedException, RequestException {
        String requestName = "GAC";
        setUser(1);

        String barcode = "7919460566833";
        Request requestToSend = new Request(requestName, barcode);
        Request requestReceived = sendAndReceive(requestToSend);
        assertEquals("The server didn't respond with the correct Parameter", requestName, requestReceived.getParameters());
        ArticleCategory article = (ArticleCategory) requestReceived.getData();
        assertEquals("The ID of the article is incorrect", 1, article.getId());
        logger.log(Level.INFO, article.show());

        barcode = "1919460566833";
        requestToSend = new Request(requestName, barcode);
        requestReceived = sendAndReceive(requestToSend);
        assertEquals("The server didn't respond with the correct Parameter", requestName, requestReceived.getParameters());
        article = (ArticleCategory) requestReceived.getData();
        assertEquals("The ID of the article is incorrect", null, article);
    }

    @Test
    public void testGACN() throws IOException, ClassNotFoundException, InterruptedException, RequestException {
        String requestName = "GACN";
        setUser(1);

        String barcode = "Fri";
        Request requestToSend = new Request(requestName, barcode);
        Request requestReceived = sendAndReceive(requestToSend);
        assertEquals("The server didn't respond with the correct Parameter", requestName, requestReceived.getParameters());
        ArrayList<ArticleCategory> articleCategories = (ArrayList<ArticleCategory>) requestReceived.getData();
        assertEquals("The ID of the article is incorrect", 1, articleCategories.size());

        barcode = "HP";
        requestToSend = new Request(requestName, barcode);
        requestReceived = sendAndReceive(requestToSend);
        assertEquals("The server didn't respond with the correct Parameter", requestName, requestReceived.getParameters());
        articleCategories = (ArrayList<ArticleCategory>) requestReceived.getData();
        assertEquals("The ID of the article is incorrect", 2, articleCategories.size());
    }

    @Test
    public void testGAE() throws IOException, ClassNotFoundException, InterruptedException, RequestException {
        String requestName = "GAE";
        setUser(1);

        int articleID = 1;
        Request requestToSend = new Request(requestName, articleID);
        Request requestReceived = sendAndReceive(requestToSend);
        assertEquals("The server didn't respond with the correct Parameter", requestName, requestReceived.getParameters());
        List<Address> addresses = (List<Address>) requestReceived.getData();
        assertEquals("The number of addresses is incorrect", 5, addresses.size());
        for (Address a : addresses)
            logger.log(Level.INFO, a.show());
    }

    @Test
    public void testGAIL() throws IOException, ClassNotFoundException, InterruptedException, RequestException {
        String requestName = "GAIL";
        setUser(1);

        ArrayList<Integer> articlesID = new ArrayList<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8));
        Request requestToSend = new Request(requestName, articlesID);
        Request requestReceived = sendAndReceive(requestToSend);
        assertEquals("The server didn't respond with the correct Parameter", requestName, requestReceived.getParameters());
        List<Article> articles = (List<Article>) requestReceived.getData();
        assertEquals("The number of articles is incorrect", 8, articles.size());
        for (Article a : articles)
            logger.log(Level.INFO, a.show());
    }

    @Test
    public void testGAMEL() throws IOException, ClassNotFoundException, InterruptedException, RequestException {
        String requestName = "GAMEL";
        setUser(1);

        int articleCategoryID = 1;
        Request requestToSend = new Request(requestName, articleCategoryID);
        Request requestReceived = sendAndReceive(requestToSend);
        assertEquals("The server didn't respond with the correct Parameter", requestName, requestReceived.getParameters());
        TreeMap<Address, Integer> path = (TreeMap<Address, Integer>) requestReceived.getData();
        assertEquals("The number of addresses is incorrect", 2, path.size());

        for (Map.Entry<Address, Integer> a : path.entrySet())
            logger.log(Level.INFO, a.getKey().getId() + "");
    }

    @Test
    public void testGAML() throws IOException, ClassNotFoundException, InterruptedException, RequestException {
        String requestName = "GAML";
        setUser(1);

        Request requestToSend = new Request(requestName, null);
        Request requestReceived = sendAndReceive(requestToSend);
        assertEquals("The server didn't respond with the correct Parameter", requestName, requestReceived.getParameters());
        Set<ArticleCategory> articles = (Set<ArticleCategory>) requestReceived.getData();
        assertEquals("The number of articlesCategories is incorrect", 3, articles.size());
        for (ArticleCategory a : articles)
            logger.log(Level.INFO, a.show());
    }

    @Test
    public void testGAREL() throws IOException, ClassNotFoundException, InterruptedException, RequestException {
        String requestName = "GAREL";
        setUser(1);

        Integer[] values = {1, 621};

        Request requestToSend = new Request(requestName, values);
        Request requestReceived = sendAndReceive(requestToSend);
        assertEquals("The server didn't respond with the correct Parameter", requestName, requestReceived.getParameters());
        TreeMap<Address, Integer> path = (TreeMap<Address, Integer>) requestReceived.getData();
        assertEquals("The number of entry is incorrect", 1, path.size());

        for (Map.Entry<Address, Integer> a : path.entrySet())
            logger.log(Level.INFO, a.getValue() + " AT " + a.getKey().getId());
    }

    @Test
    public void testGAP() throws IOException, ClassNotFoundException, InterruptedException, RequestException {
        String requestName = "GAP";
        setUser(1);

        Request requestToSend = new Request(requestName, null);
        Request requestReceived = sendAndReceive(requestToSend);
        assertEquals("The server didn't respond with the correct Parameter", requestName, requestReceived.getParameters());
        List<Address> addresses = (List<Address>) requestReceived.getData();
        assertEquals("The number of addresses is incorrect", 150, addresses.size());
        for (Address a : addresses)
            logger.log(Level.INFO, a.show());
    }

    @Test
    public void testGAPR() throws IOException, ClassNotFoundException, InterruptedException, RequestException {
        String requestName = "GAPR";
        setUser(1);

        Request requestToSend = new Request(requestName, null);
        Request requestReceived = sendAndReceive(requestToSend);
        assertEquals("The server didn't respond with the correct Parameter", requestName, requestReceived.getParameters());
        List<Address> addresses = (List<Address>) requestReceived.getData();
        assertEquals("The number of addresses is incorrect", 375, addresses.size());
        for (Address a : addresses)
            logger.log(Level.INFO, a.show());
    }

    @Test
    public void testGAR() throws IOException, ClassNotFoundException, InterruptedException, RequestException {
        String requestName = "GAR";
        setUser(1);

        Request requestToSend = new Request(requestName, null);
        Request requestReceived = sendAndReceive(requestToSend);
        assertEquals("The server didn't respond with the correct Parameter", requestName, requestReceived.getParameters());
        List<Address> addresses = (List<Address>) requestReceived.getData();
        assertEquals("The number of addresses is incorrect", 225, addresses.size());
        for (Address a : addresses)
            logger.log(Level.INFO, a.show());
    }

    @Test
    public void testGCLA() throws IOException, ClassNotFoundException, InterruptedException, RequestException {
        String requestName = "GCLA";
        setUser(1);

        int orderID = 1;
        Request requestToSend = new Request(requestName, orderID);
        Request requestReceived = sendAndReceive(requestToSend);
        assertEquals("The server didn't respond with the correct Parameter", requestName, requestReceived.getParameters());
        Map<ArticleCategory, Integer> articles = (Map<ArticleCategory, Integer>) requestReceived.getData();
        assertEquals("The number of articles is incorrect", 1, articles.size());
        assertEquals("The quantity for the article is incorrect", 5, articles.entrySet().iterator().next().getValue().intValue());
        assertEquals("The ID of the article is incorrect", 1, articles.entrySet().iterator().next().getKey().getId());
        for (Map.Entry<ArticleCategory, Integer> a : articles.entrySet())
            logger.log(Level.INFO, a.getValue() + " * " + a.getKey().show());
    }

    @Test
    public void testGCLC() throws IOException, ClassNotFoundException, InterruptedException, RequestException {
        String requestName = "GCLC";
        setUser(1);

        Request requestToSend = new Request(requestName, null);
        Request requestReceived = sendAndReceive(requestToSend);
        assertEquals("The server didn't respond with the correct Parameter", requestName, requestReceived.getParameters());
        List<Order> orders = (List<Order>) requestReceived.getData();
        assertEquals("The number of orders is incorrect", 1, orders.size());
        for (Order o : orders)
            logger.log(Level.INFO, o.show());
    }

    @Test
    public void testGCLU() throws IOException, ClassNotFoundException, InterruptedException, RequestException {
        String requestName = "GCLU";
        setUser(1);

        Request requestToSend = new Request(requestName, null);
        Request requestReceived = sendAndReceive(requestToSend);
        assertEquals("The server didn't respond with the correct Parameter", requestName, requestReceived.getParameters());
        List<Order> orders = (List<Order>) requestReceived.getData();
        assertEquals("The number of orders is incorrect", 1, orders.size());
        for (Order o : orders)
            logger.log(Level.INFO, o.show());
    }

    @Test
    public void testGCN() throws IOException, ClassNotFoundException, InterruptedException, RequestException {
        String requestName = "GCN";
        setUser(1);

        int orderID = 1;
        Request requestToSend = new Request(requestName, orderID);
        Request requestReceived = sendAndReceive(requestToSend);
        assertEquals("The server didn't respond with the correct Parameter", requestName, requestReceived.getParameters());
        String clientName = (String) requestReceived.getData();
        assertEquals("The client name is incorrect", "Simon Colin", clientName);
        logger.log(Level.INFO, clientName);
    }

    @Test
    public void testGDL() throws IOException, ClassNotFoundException, InterruptedException, RequestException {
        String requestName = "GDL";
        setUser(1);

        Request requestToSend = new Request(requestName, null);
        Request requestReceived = sendAndReceive(requestToSend);
        assertEquals("The server didn't respond with the correct Parameter", requestName, requestReceived.getParameters());
        List<Deposit> deposits = (List<Deposit>) requestReceived.getData();
        assertEquals("The number of deposits is incorrect", 2, deposits.size());
        for (Deposit d : deposits)
            logger.log(Level.INFO, d.show());
    }

    @Test
    public void testGE() throws IOException, ClassNotFoundException, InterruptedException, RequestException {
        String requestName = "GE";
        setUser(1);

        int addressID = 620;
        Request requestToSend = new Request(requestName, addressID);
        Request requestReceived = sendAndReceive(requestToSend);
        assertEquals("The server didn't respond with the correct Parameter", requestName, requestReceived.getParameters());
        Map<Article, Integer> articlesInAddress = (Map<Article, Integer>) requestReceived.getData();
        assertEquals("The number of Articles in the Address is incorrect", 2, articlesInAddress.size());
        for (Map.Entry<Article, Integer> e : articlesInAddress.entrySet())
            logger.log(Level.INFO, e.getValue() + " * " + e.getKey().show());
    }

    @Test
    public void testGELA() throws IOException, ClassNotFoundException, InterruptedException, RequestException {
        String requestName = "GELA";
        setUser(1);

        Request requestToSend = new Request(requestName, null);
        Request requestReceived = sendAndReceive(requestToSend);
        assertEquals("The server didn't respond with the correct Parameter", requestName, requestReceived.getParameters());
        List<Address> emptyAddresses = (List<Address>) requestReceived.getData();
        assertEquals("The number of empty Addresses is incorrect", 100, emptyAddresses.size());
        for (Address a : emptyAddresses)
            logger.log(Level.INFO, a.pos());
    }

    @Override
    protected void setDebugMode() {
        Tool.setDebugMode(0);
    }
}
