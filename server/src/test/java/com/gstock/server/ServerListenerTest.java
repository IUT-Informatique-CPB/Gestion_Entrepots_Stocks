package com.gstock.server;


import com.gstock.tools.Exception.RequestException;
import com.gstock.tools.Request;
import com.gstock.tools.Tool;
import com.gstock.tools.user.User;
import org.junit.Test;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.ConsoleHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class ServerListenerTest extends ServerListener{

    @Test
    public void successAuthentification() throws IOException, RequestException, ClassNotFoundException, InterruptedException {
        System.setProperty("java.util.logging.SimpleFormatter.format",
                "[%1$tF %1$tT] [%4$-7s] %5$s%6$s%n");

        initLogger();

        Tool.activateDebug();
        Tool.setDebugMode(0);

        ServerListenerTest sl = new ServerListenerTest();
        sl.start();

        Thread.sleep(100);

        Socket clientSocket = new Socket("localHost", 2048);

        String[] id = {"ldumartin", "lucas"};
        Request loginRequest = new Request("ULI", id);
        ObjectOutputStream outputStream = new ObjectOutputStream(clientSocket.getOutputStream());
        outputStream.writeObject(loginRequest);

        ObjectInputStream inputStream = new ObjectInputStream(clientSocket.getInputStream());
        Request requestReceived = (Request) inputStream.readObject();
        if (requestReceived.getParameters().equals("E")) {
            sl.interrupt();
            throw new RequestException("Error received from Server");
        }

        User user = (User) requestReceived.getData();
        assertNotNull("Fail to authentify ", user);
        assertEquals("Wrong Identity", "ldumartin", user.getLogin());

        sl.interrupt();
    }

    @Test
    public void failedAuthentification() throws IOException, RequestException, ClassNotFoundException, InterruptedException {
        System.setProperty("java.util.logging.SimpleFormatter.format",
                "[%1$tF %1$tT] [%4$-7s] %5$s%6$s%n");

        initLogger();

        Tool.activateDebug();
        Tool.setDebugMode(0);

        ServerListenerTest sl = new ServerListenerTest();
        sl.start();

        Thread.sleep(100);

        Socket clientSocket = new Socket("localHost", 2048);

        String[] id = {"l", "l"};
        Request loginRequest = new Request("ULI", id);
        ObjectOutputStream outputStream = new ObjectOutputStream(clientSocket.getOutputStream());
        outputStream.writeObject(loginRequest);

        ObjectInputStream inputStream = new ObjectInputStream(clientSocket.getInputStream());
        Request requestReceived = (Request) inputStream.readObject();
        if (requestReceived.getParameters().equals("E")) {
            sl.interrupt();
            throw new RequestException("Error received from Server");
        }

        User user = (User) requestReceived.getData();
        assertNull("Wrong data ", user);

        sl.interrupt();
    }

    private void initLogger() throws IOException {
        ConsoleHandler consoleHandler = new ConsoleHandler();
        consoleHandler.setFormatter(new SimpleFormatter());

        Logger.getLogger("com.gstock").setUseParentHandlers(false);
        Logger.getLogger("com.gstock").addHandler(consoleHandler);
    }
}
