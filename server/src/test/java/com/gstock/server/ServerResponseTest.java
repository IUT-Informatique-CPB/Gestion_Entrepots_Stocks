package com.gstock.server;

import com.gstock.tools.Exception.RequestException;
import com.gstock.tools.Request;
import com.gstock.tools.Tool;
import com.gstock.tools.user.ServerUser;
import com.gstock.tools.user.UserFactory;
import org.junit.After;
import org.junit.Before;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public abstract class ServerResponseTest {
    ServerRequest serverRequest = new ServerRequest();
    ServerSocket serverSocket;
    Socket clientSocketOnServer;

    Socket clientSocket;
    ServerUser clientUser = null;

    Logger logger = Logger.getLogger(ServerResponse.class.getName());

    @Before
    public void init () throws IOException {
        System.setProperty("java.util.logging.SimpleFormatter.format",
                "[%1$tF %1$tT] [%4$-7s] %5$s%6$s%n");

        initLogger();

        Tool.activateDebug();
        setDebugMode();

        serverRequest.start();
        serverSocket =  new ServerSocket(2049);

        clientSocket = new Socket("localHost", 2049);

        clientSocketOnServer = serverSocket.accept();
    }

    @After
    public void stopServer() throws InterruptedException, IOException {
        serverSocket.close();
        serverRequest.interrupt();
        serverRequest.join();
    }

    protected Request sendAndReceive(Request requestToSend) throws IOException, ClassNotFoundException, RequestException {
        ObjectOutputStream outputStream = new ObjectOutputStream(clientSocket.getOutputStream());
        outputStream.writeObject(requestToSend);

        ObjectInputStream inputStream = new ObjectInputStream(clientSocket.getInputStream());
        Request requestReceived = (Request) inputStream.readObject();
        if (requestReceived.getParameters().equals("E")) {
            serverSocket.close();
            serverRequest.interrupt();
            throw new RequestException("Error received from Server");
        }
        return requestReceived;
    }

    protected void setUser(int i) {
        clientUser = UserFactory.getUser(i);
        clientUser.setClientSocket(clientSocketOnServer);
        serverRequest.addClient(clientUser);
    }

    private void initLogger() throws IOException {
        ConsoleHandler consoleHandler = new ConsoleHandler();
        consoleHandler.setFormatter(new SimpleFormatter());

        logger.setUseParentHandlers(false);
        logger.addHandler(consoleHandler);
    }

    protected abstract void setDebugMode();
}
