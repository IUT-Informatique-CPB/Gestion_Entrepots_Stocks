package com.gstock.server;


import com.gstock.tools.Exception.RequestException;
import com.gstock.tools.Request;
import com.gstock.tools.Tool;
import com.gstock.tools.address.Address;
import com.gstock.tools.article.Article;
import com.gstock.tools.article.ArticleCategory;
import org.junit.Test;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;

public class ServerResponseUtilitiesTest extends  ServerResponseTest {

    @Test
    public void testU() throws IOException, ClassNotFoundException, RequestException {
        setUser(1);

        int orderID = 1;
        Request requestToSend = new Request("UCP", orderID);

        Request requestReceived = sendAndReceive(requestToSend);

        TreeMap<Address, HashMap<ArticleCategory, Integer>> result = (TreeMap<Address, HashMap<ArticleCategory, Integer>>) requestReceived.getData();

        //TODO Check if the return is correct, but before that we need to add interresting data to the Test DataBase
        StringBuilder str = new StringBuilder();
        for (Map.Entry<Address, HashMap<ArticleCategory, Integer>> e1 : result.entrySet()) {
            str.append("\n").append(e1.getKey().pos()).append(" :");
            for (Map.Entry<ArticleCategory, Integer> e2 : e1.getValue().entrySet()) {
                str.append("\n    ").append(e2.getValue()).append(" * ").append(e2.getKey().show());
            }
        }
        logger.log(Level.INFO, str.toString());

        orderID = 2;
        requestToSend = new Request("UCP", orderID);

        requestReceived = sendAndReceive(requestToSend);

        result = (TreeMap<Address, HashMap<ArticleCategory, Integer>>) requestReceived.getData();

        //TODO Check if the return is correct, but before that we need to add interresting data to the Test DataBase
        str = new StringBuilder();
        for (Map.Entry<Address, HashMap<ArticleCategory, Integer>> e1 : result.entrySet()) {
            str.append("\n").append(e1.getKey().pos()).append(" :");
            for (Map.Entry<ArticleCategory, Integer> e2 : e1.getValue().entrySet()) {
                str.append("\n    ").append(e2.getValue()).append(" * ").append(e2.getKey().show());
            }
        }
        logger.log(Level.INFO, str.toString());
    }

    @Override
    protected void setDebugMode() {
        Tool.setDebugMode(0);
    }
}