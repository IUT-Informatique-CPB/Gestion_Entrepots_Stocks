package com.gstock.server;


import com.gstock.tools.DatabaseConnection;
import com.gstock.tools.Exception.RequestException;
import com.gstock.tools.Request;
import com.gstock.tools.Tool;
import com.gstock.tools.address.Address;
import com.gstock.tools.address.AddressFactory;
import com.gstock.tools.article.Article;
import com.gstock.tools.article.ArticleCategory;
import com.gstock.tools.article.ArticleCategoryFactory;
import com.gstock.tools.article.ArticleFactory;
import com.gstock.tools.order.Order;
import com.gstock.tools.order.OrderFactory;
import org.junit.Test;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;

import static org.junit.Assert.*;

public class ServerResponseSetTest extends ServerResponseTest {

    @Test
    public void testSCMS() throws IOException, ClassNotFoundException, InterruptedException, RequestException {
        setUser(1);

        String oldDate = null;
        String newDate = "28/02/2017";
        Order order = OrderFactory.getOrder(2);
        logger.log(Level.INFO, order.show());
        assertNotNull(order);
        oldDate = order.getDate();
        order.setDate(newDate);

        Request requestToSend = new Request("SCMS", order);
        Boolean response = (Boolean) sendAndReceive(requestToSend).getData();

        assertTrue(response);

        order = OrderFactory.getOrder(2);
        logger.log(Level.INFO, order.show());
        assertNotNull(order);
        assertEquals(newDate, order.getDate());

        order.setDate(oldDate);

        requestToSend = new Request("SCMS", order);
        response = (Boolean) sendAndReceive(requestToSend).getData();

        assertTrue(response);
        order = OrderFactory.getOrder(2);
        logger.log(Level.INFO, order.show());
        assertNotNull(order);
        assertEquals(oldDate, order.getDate());
    }

    @Test
    public void testSASBandSASE() throws IOException, ClassNotFoundException, RequestException{
        setUser(1);
        Boolean exist = false;
        Integer nbArticlesPresent=0;
        DatabaseConnection db = new DatabaseConnection();

        Article article = ArticleFactory.getArticle(2);
        Address address = AddressFactory.getAddress(620);

        final StringBuilder getIfExist = new StringBuilder();
        getIfExist.append("SELECT Articles_Number FROM POSSESSES ");
        getIfExist.append("WHERE Article_id = ? ");
        getIfExist.append("AND Address_id = ?");

        try (PreparedStatement ps = db.getConnection().prepareStatement(getIfExist.toString())) {
            ps.setObject(1, article.getId());
            ps.setObject(2, address.getId());

            ResultSet result = ps.executeQuery();
            if (result.next()) {
                exist = true;
                nbArticlesPresent = result.getInt(1);
            } else {
                exist = false;
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }

        Request requestToSend = new Request("SASB", article.getId());
        Boolean response = (Boolean) sendAndReceive(requestToSend).getData();
        assertTrue(response);
        assertTrue(Tool.getArticlesBeingStocked().contains(article.getId()));

        ArrayList<Integer> data = new ArrayList<>();

        data.add(article.getId());
        data.add(address.getId());
        data.add(10);

        requestToSend = new Request("SASE", data);
        response = (Boolean) sendAndReceive(requestToSend).getData();
        assertTrue(response);
        //assertFalse(Tool.getArticlesBeingStocked().contains(article.getId()));

        if (exist) {
            final StringBuilder updatePossesses = new StringBuilder();
            updatePossesses.append("UPDATE POSSESSES ");
            updatePossesses.append("SET Articles_Number = ? ");
            updatePossesses.append("WHERE Article_id = ? ");
            updatePossesses.append("AND Address_id = ?");

            try (PreparedStatement ps = db.getConnection().prepareStatement(updatePossesses.toString())) {
                ps.setInt(1, nbArticlesPresent);
                ps.setInt(2, article.getId());
                ps.setInt(3, address.getId());

                assertTrue(ps.executeUpdate() == 1);
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
            }
        } else {
            final StringBuilder addArticleAddress = new StringBuilder();
            addArticleAddress.append("DELETE FROM POSSESSES ");
            addArticleAddress.append("WHERE Address_id = ? ");
            addArticleAddress.append("AND Article_id = ?");

            try (PreparedStatement ps = db.getConnection().prepareStatement(addArticleAddress.toString())) {
                ps.setObject(1, address.getId());
                ps.setObject(2, article.getId());

                assertTrue(ps.executeUpdate() == 1);

            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
            }
        }

        logger.log(Level.INFO, "Addressid :"+address.getId()+", nb article present : "+nbArticlesPresent);
    }

    @Test
    public void testSAMBandSAME() throws IOException, ClassNotFoundException, RequestException{
        setUser(1);
        DatabaseConnection db = new DatabaseConnection();
        Integer oldAdress = 0;

        Article article = ArticleFactory.getArticle(2);

        final StringBuilder getAddress = new StringBuilder();
        getAddress.append("SELECT Address_id FROM POSSESSES");
        getAddress.append(" WHERE POSSESSES.Article_id = ?");

        try (PreparedStatement ps = db.getConnection().prepareStatement(getAddress.toString())) {
            Boolean update = Boolean.FALSE;
            ps.setInt(1,article.getId());
            ResultSet result = ps.executeQuery();
            if (result.next()) {
                oldAdress = result.getInt(1);
            }else{
                assertTrue(false);
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }

        ArrayList<Integer> data = new ArrayList<>();
        Address address = AddressFactory.getAddress(623);

        data.add(article.getId());
        data.add(address.getId());
        data.add(oldAdress);
        data.add(10);

        Request requestToSend = new Request("SAME", data);
        Boolean response = (Boolean) sendAndReceive(requestToSend).getData();
        assertTrue(response);
        assertFalse(Tool.getArticlesBeingMoved().contains(article.getId()));

        data.clear();
        data.add(article.getId());
        data.add(oldAdress);
        data.add(address.getId());
        data.add(10);

        requestToSend = new Request("SAME", data);
        response = (Boolean) sendAndReceive(requestToSend).getData();
        assertTrue(response);
        assertFalse(Tool.getArticlesBeingMoved().contains(article.getId()));

        logger.log(Level.INFO, "new Addressid :"+address.getId()+", old : "+oldAdress);

    }

    @Test
    public void testSCPBandSCPE() throws IOException, ClassNotFoundException, RequestException {

        setUser(1);
        DatabaseConnection db = new DatabaseConnection();

        Order order = OrderFactory.getOrder(1);
        logger.log(Level.INFO, order.show());

        assertTrue(order.getPickerId() == null);

        Request requestToSend = new Request("SCPB", order);
        Boolean response = (Boolean) sendAndReceive(requestToSend).getData();
        assertTrue(response);

        order = OrderFactory.getOrder(1);

        assertTrue(order.getPickerId() == 1);
        assertTrue(!order.getCompleted());

        requestToSend = new Request("SCPE",order);
        response = (Boolean) sendAndReceive(requestToSend).getData();

        assertTrue(response);
        order = OrderFactory.getOrder(1);
        logger.log(Level.INFO, order.show());
        assertTrue(order.getCompleted());

        final StringBuilder resetPickerArticle = new StringBuilder();
        resetPickerArticle.append("UPDATE ORDERS ");
        resetPickerArticle.append("SET ORDERS.Picker_id = NULL ");
        resetPickerArticle.append(", ORDERS.Completed = 0 ");
        resetPickerArticle.append("WHERE ORDERS.id = 1");

        try (PreparedStatement ps = db.getConnection().prepareStatement(resetPickerArticle.toString())) {
            Boolean update = Boolean.FALSE;

            if (ps.executeUpdate() == 1) {
                update = true;
            }
            assertTrue(update);
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
        order = OrderFactory.getOrder(1);
        assertTrue(order.getPickerId() == null);
        assertTrue(!order.getCompleted());
    }

    @Test
    public void testSCMBandSCME() throws IOException, ClassNotFoundException, RequestException {

        setUser(1);

        Order order = OrderFactory.getOrder(1);

        Request requestToSend = new Request("SCMB", order);
        Boolean response = (Boolean) sendAndReceive(requestToSend).getData();
        assertTrue(response);
        assertTrue(Tool.getOrderBeingEdited().contains(order.getId()));

        requestToSend = new Request("SCMB", order);
        response = (Boolean) sendAndReceive(requestToSend).getData();
        assertFalse(response);

        requestToSend = new Request("SCME", order);
        response = (Boolean) sendAndReceive(requestToSend).getData();
        assertTrue(response);
        assertFalse(Tool.getOrderBeingEdited().contains(order.getId()));
    }

    @Test
    public void testSA() throws IOException, ClassNotFoundException, RequestException {
        setUser(1);
        DatabaseConnection db = new DatabaseConnection();

        Article article = new Article(125, 456, null, "30/03/2019", ArticleCategoryFactory.getArticleCategory(1));

        logger.log(Level.INFO, article.show());

        Request requestToSend = new Request("SA", article);
        Integer articleId = (Integer) sendAndReceive(requestToSend).getData();
        logger.log(Level.INFO,"article id : "+articleId);

        final StringBuilder deleteArticle = new StringBuilder();
        deleteArticle.append("DELETE FROM ARTICLE ");
        deleteArticle.append("WHERE ARTICLE.Customs_Number = ? ");
        deleteArticle.append("AND ARTICLE.Serial_Number = ? ");
        deleteArticle.append("AND ARTICLE.Expiration_Date IS NULL ");
        deleteArticle.append("AND ARTICLE.Receipt_Date = ? ");
        deleteArticle.append("AND ARTICLE.ArticleCategory_id = ?");

        try (PreparedStatement ps = db.getConnection().prepareStatement(deleteArticle.toString())) {
            ps.setObject(1, article.getCustomsNumber());
            ps.setObject(2, article.getSerialNumber());
            ps.setString(3, article.getReceiptDate());
            ps.setObject(4, article.getArticleCategory().getId());
            Boolean update = Boolean.FALSE;
            if (ps.executeUpdate() == 1) {
                update = true;
            }
            assertTrue(update);
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
    }

    @Test
    public void testSCC() throws IOException, ClassNotFoundException, RequestException {
        setUser(1);
        DatabaseConnection db = new DatabaseConnection();

        ArticleCategory category = new ArticleCategory("5326194529156","Ceci est un produit de test",10,10,10);

        logger.log(Level.INFO, category.show());

        Request requestToSend = new Request("SCC", category);
        Boolean response = (Boolean) sendAndReceive(requestToSend).getData();
        assertTrue(response);

        final StringBuilder deleteCategory = new StringBuilder();
        deleteCategory.append("DELETE FROM ARTICLE_CATEGORY ");
        deleteCategory.append("WHERE ARTICLE_CATEGORY.Bar_Code = ? ");

        try (PreparedStatement ps = db.getConnection().prepareStatement(deleteCategory.toString())) {
            ps.setString(1, category.getBarCode());
            Boolean update = Boolean.FALSE;
            if (ps.executeUpdate() == 1) {
                update = true;
            }
            assertTrue(update);
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
    }

    @Override
    protected void setDebugMode() {
        Tool.setDebugMode(1);
    }
}
