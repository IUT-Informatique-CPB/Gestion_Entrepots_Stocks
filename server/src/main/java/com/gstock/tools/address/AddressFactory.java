package com.gstock.tools.address;

import com.gstock.tools.DatabaseConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class AddressFactory {
    private static final Logger LOGGER = Logger.getLogger( AddressFactory.class.getName() );

    public static Address getAddress(int id){
        DatabaseConnection dc = new DatabaseConnection();
        Connection conn = dc.getConnection();
        String getArticle = "SELECT * FROM ADDRESS WHERE ADDRESS.id = ?";
        try ( PreparedStatement ps = conn.prepareStatement(getArticle) ) {
            ps.setInt(1, id);
            try  ( ResultSet result = ps.executeQuery() ) {
                if (result.next())
                    return getAddress(result);
                else
                    LOGGER.log(Level.FINE, "The Address with ID \"" + id + "\" was nowhere to be found" );
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
        return null;
    }

    public static Address getAddress(ResultSet resultSet) {
        try {
            int id = resultSet.getInt(1);
            int shopId = resultSet.getInt(2);
            int drivewayCode = resultSet.getInt(3);
            int rankNumber = resultSet.getInt(4);
            int positionNumber = resultSet.getInt(5);
            String size = resultSet.getString(6);
            String barCode = resultSet.getString(7);

            return new Address(id, shopId, drivewayCode, rankNumber, positionNumber, size, barCode);
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
        return null;
    }
}
