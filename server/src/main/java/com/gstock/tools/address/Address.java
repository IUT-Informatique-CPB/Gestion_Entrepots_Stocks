package com.gstock.tools.address;

import java.io.Serializable;

public class Address implements Serializable, Comparable<Address> {
    //REAL ATTRIBUTES
    private int id;
    private int shopId;
    private int drivewayCode;
    private int rankNumber;
    private int positionNumber;
    private String size;
    private String barCode;

    public Address(int id, int shopId, int drivewayCode, int rankNumber, int positionNumber, String size, String barCode) {
        this.id = id;
        this.shopId = shopId;
        this.drivewayCode = drivewayCode;
        this.rankNumber = rankNumber;
        this.positionNumber = positionNumber;
        this.size = size;
        this.barCode = barCode;
    }

    public int getId() {
        return id;
    }

    public int getDrivewayCode() {
        return drivewayCode;
    }

    public int getPositionNumber() {
        return positionNumber;
    }

    public int getRankNumber() {
        return rankNumber;
    }

    public int getShopId() {
        return shopId;
    }

    public String getBarCode() {
        return barCode;
    }

    public String getSize() {
        return size;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public void setDrivewayCode(int drivewayCode) {
        this.drivewayCode = drivewayCode;
    }

    public void setPositionNumber(int positionNumber) {
        this.positionNumber = positionNumber;
    }

    public void setRankNumber(int rankNumber) {
        this.rankNumber = rankNumber;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String show() {
        StringBuilder str = new StringBuilder();
        str.append('[').append(this.getClass().getSimpleName()).append("] { ");
        str.append("id='").append(id).append("', ");
        str.append("barcode='").append(barCode).append("', ");
        str.append("size='").append(size).append("', ");
        str.append("shopId='").append(shopId).append("', ");
        str.append("drivewayCode='").append(drivewayCode).append("', ");
        str.append("rankNumber='").append(rankNumber).append("', ");
        str.append("positionNumber='").append(positionNumber).append("' };");
        return str.toString();
    }

    public String pos() {
        StringBuilder str = new StringBuilder();
        str.append("[ ").append(shopId);
        str.append(", ").append(drivewayCode);
        str.append(", ").append(rankNumber);
        str.append(", ").append(positionNumber);
        str.append(" ]");
        return str.toString();
    }

    @Override
    public String toString() {
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        return "Mag:"+shopId+" "+alphabet.charAt(drivewayCode-1)+rankNumber+" Pos:"+positionNumber;
    }

    @Override
    public int compareTo(Address a) {
        if (shopId == a.shopId) {
            if (drivewayCode == a.drivewayCode) {
                if (rankNumber == a.rankNumber) {
                    return positionNumber - a.positionNumber;
                } else {
                    if (drivewayCode % 2 == 1) {
                        return rankNumber - a.rankNumber;
                    } else
                        return a.rankNumber - rankNumber;
                }
            } else
                return drivewayCode - a.drivewayCode;
        } else
            return shopId - a.shopId;
    }
}
