package com.gstock.tools;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DatabaseConnection {
    /**
     * Logger of this class. Used to log everything in the console and a log file
     */
    private Logger LOGGER = Logger.getLogger( this.getClass().getName() );

    /**
     * Connection to the DataBase
     */
    Connection conn = null;

    private final String JDBC_DRIVER = "org.mariadb.jdbc.Driver";

    private final String DB_URL = "jdbc:mariadb://dupuysylvain.fr/PT4";
    private final String USER = "PT4";
    private final String PASS = "PT4";

    private final String[] DB_TEST_URL = {"jdbc:mariadb://dupuysylvain.fr/PT4_Test_GET", "jdbc:mariadb://dupuysylvain.fr/PT4_Test_SET"};
    private final String TEST_USER = "PT4";
    private final String TEST_PASS = "PT4";

    /**
     * Create a connection with the database
     */
    public DatabaseConnection() {
        //TODO Get Login and Address from JSON
        try {
            //STEP 1: Register JDBC driver
            Class.forName(JDBC_DRIVER);
            //STEP 2: Open a connection
            if (Tool.isDebug() && Tool.getDebugMode() != -1)
                conn = DriverManager.getConnection(DB_TEST_URL[Tool.getDebugMode()], TEST_USER, TEST_PASS);
            else
                conn = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        } catch (ClassNotFoundException e) {
            LOGGER.log(Level.SEVERE, "Couldn't Find the class for the Database Driver", e);
        }
    }

    /**
     * Getter of the database connection object
     * @return Connection with the database
     */
    public Connection getConnection() {
        return conn;
    }
}