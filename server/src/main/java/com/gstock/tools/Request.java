package com.gstock.tools;

import java.io.Serializable;

public class Request implements Serializable {
    /**
     * Parameter of the Request, define the way teh request must be processed
     */
    private String parameters;

    /**
     * The possible data that can be specified as an argument
     */
    private Object data;

    /**
     * Constructor
     * @param parameters
     * @param data
     */
    public Request(String parameters, Object data) {
        this.parameters = parameters;
        this.data = data;
    }

    /**
     * Getter of the parameters
     * @return The parameters of the request
     */
    public String getParameters() {
        return parameters;
    }

    /**
     * Getter of the Data
     * @return The data of the request
     */
    public Object getData() {
        return data;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append('[').append(this.getClass().getSimpleName()).append("] { ");
        str.append("parameters='").append(parameters).append("', ");
        str.append("data='").append(data.toString()).append("', ");
        return str.toString();
    }
}
