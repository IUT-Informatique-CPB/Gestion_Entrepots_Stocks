package com.gstock.tools.deposit;

import com.gstock.tools.DatabaseConnection;
import com.gstock.tools.article.ArticleFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DepositFactory {

    private static final Logger LOGGER = Logger.getLogger( ArticleFactory.class.getName() );

    public static Deposit getDeposit(int id){
        DatabaseConnection dc = new DatabaseConnection();
        Connection conn = dc.getConnection();
        String getArticle = "SELECT * FROM DEPOSIT WHERE DEPOSIT.id = ?";
        try ( PreparedStatement ps = conn.prepareStatement(getArticle) ) {
            ps.setInt(1, id);
            try  ( ResultSet result = ps.executeQuery() ) {
                if (result.next())
                    return getDeposit(result);
                else
                    LOGGER.log(Level.FINE, "The Deposit with ID \"" + id + "\" was nowhere to be found" );
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
        return null;
    }

    public static Deposit getDeposit(ResultSet result){
        try {
            int id = result.getInt(1);
            String address = result.getString(2);
            String name = result.getString(3);
            return new Deposit(id, address, name);
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
        return null;
    }
}
