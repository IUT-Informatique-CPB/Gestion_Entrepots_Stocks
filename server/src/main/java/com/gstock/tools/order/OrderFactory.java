package com.gstock.tools.order;

import com.gstock.tools.DatabaseConnection;
import com.gstock.tools.article.ArticleFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class OrderFactory {
    private static final Logger LOGGER = Logger.getLogger( ArticleFactory.class.getName() );

    public static Order getOrder(int id){
        DatabaseConnection dc = new DatabaseConnection();
        Connection conn = dc.getConnection();

        String getOrder = "SELECT * FROM ORDERS WHERE ORDERS.id = ?";

        try ( PreparedStatement ps = conn.prepareStatement(getOrder) ) {
            ps.setInt(1, id);
            try  ( ResultSet result = ps.executeQuery() ) {
                if (result.next())
                    return getOrder(result);
                else
                    LOGGER.log(Level.FINE, "The Order with ID \"" + id + "\" was nowhere to be found" );
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
        return null;
    }

    public static Order getOrder(ResultSet resultSet) {
        DatabaseConnection dc = new DatabaseConnection();
        Connection conn = dc.getConnection();
        try {
            int id = resultSet.getInt(1);
            String barCode = resultSet.getString(2);
            String date = resultSet.getString(3);
            Integer pickerId = (Integer)resultSet.getObject(4);
            Integer operatorId = (Integer)resultSet.getObject(5);
            Integer providerId = (Integer)resultSet.getObject(6);
            Integer customerId = (Integer)resultSet.getObject(7);
            Boolean completed = resultSet.getBoolean(8);

            String getOrder = "SELECT ARTICLE_CATEGORY.id, CONTAINS.Articles_Number" +
                    " FROM ARTICLE_CATEGORY" +
                    " INNER JOIN CONTAINS ON CONTAINS.Article_id = ARTICLE_CATEGORY.id" +
                    " INNER JOIN ORDERS ON CONTAINS.Order_id= ORDERS.id" +
                    " WHERE ORDERS.id = " + id;

            try ( PreparedStatement ps = conn.prepareStatement(getOrder) ) {
                try ( ResultSet articleRequest = ps.executeQuery() ) {
                    HashMap<Integer, Integer> articles = new HashMap<>();

                    while (articleRequest.next()) {
                        articles.put(articleRequest.getInt(1), articleRequest.getInt(2));
                    }

                    return new Order(id, barCode,date,pickerId,operatorId,providerId,customerId, completed, articles);
                }
            }
        } catch (Exception e) {
            LOGGER.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
        return null;
    }
}
