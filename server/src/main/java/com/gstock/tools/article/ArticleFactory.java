package com.gstock.tools.article;

import com.gstock.tools.DatabaseConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ArticleFactory {
    private static final Logger LOGGER = Logger.getLogger( ArticleFactory.class.getName() );

    public static Article getArticle(int id){
        DatabaseConnection dc = new DatabaseConnection();
        Connection conn = dc.getConnection();
        String getArticle = "SELECT * FROM ARTICLE WHERE ARTICLE.id = ?";
        try ( PreparedStatement ps = conn.prepareStatement(getArticle) ) {
            ps.setInt(1, id);
            try  ( ResultSet result = ps.executeQuery() ) {
                if (result.next())
                    return getArticle(result);
                else
                    LOGGER.log(Level.FINE, "The Article with ID \"" + id + "\" was nowhere to be found" );
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
        return null;
    }

    public static Article getArticle(ResultSet resultSet) {
        return getArticle(resultSet, 0);
    }

    public static Article getArticle(ResultSet resultSet, int pos) {
        try {
            int id = resultSet.getInt(1 + pos);
            int customsNumber = resultSet.getInt(2 + pos);
            int serialNumber = resultSet.getInt(3 + pos);
            String expirationDate = resultSet.getString(4 + pos);
            String receiptDate = resultSet.getString(5 + pos);
            int articleCategoryid = resultSet.getInt(6 + pos);

            return new Article(id, customsNumber, serialNumber, expirationDate, receiptDate, ArticleCategoryFactory.getArticleCategory(articleCategoryid));
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
        return null;
    }
}
