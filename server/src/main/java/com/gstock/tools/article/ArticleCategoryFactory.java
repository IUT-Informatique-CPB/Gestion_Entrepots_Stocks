package com.gstock.tools.article;

import com.gstock.tools.DatabaseConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ArticleCategoryFactory {

    private static final Logger LOGGER = Logger.getLogger( ArticleFactory.class.getName() );

    public static ArticleCategory getArticleCategory(int id){
        DatabaseConnection dc = new DatabaseConnection();
        Connection conn = dc.getConnection();
        String getArticle = "SELECT * FROM ARTICLE_CATEGORY WHERE ARTICLE_CATEGORY.id = ?";
        try ( PreparedStatement ps = conn.prepareStatement(getArticle) ) {
            ps.setInt(1, id);
            try  ( ResultSet result = ps.executeQuery() ) {
                if (result.next())
                    return getArticleCategory(result);
                else
                    LOGGER.log(Level.FINE, "The Article_category with ID \"" + id + "\" was nowhere to be found" );
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
        return null;
    }

    public static ArticleCategory getArticleCategory(ResultSet resultSet) {
        return getArticleCategory(resultSet, 0);
    }

    public static ArticleCategory getArticleCategory(ResultSet resultSet, int pos) {
        try {
            int id = resultSet.getInt(1 + pos);
            String barCode = resultSet.getString(2 + pos);
            String name = resultSet.getString(3 + pos);
            int maxPalette = resultSet.getInt(4 + pos);
            int maxPaletteUSA = resultSet.getInt(5 + pos);
            int maxPaletteEurope = resultSet.getInt(6 + pos);

            return new ArticleCategory(id, barCode, name, maxPalette, maxPaletteUSA, maxPaletteEurope);
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
        return null;
    }

}
