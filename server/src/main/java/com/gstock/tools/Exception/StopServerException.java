package com.gstock.tools.Exception;

public class StopServerException extends Exception{
    public StopServerException(String str) {
        super(str);
    }
}
