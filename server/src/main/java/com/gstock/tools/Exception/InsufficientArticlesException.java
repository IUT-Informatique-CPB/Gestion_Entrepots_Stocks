package com.gstock.tools.Exception;

public class InsufficientArticlesException extends Exception {
    public InsufficientArticlesException(String str) {
        super(str);
    }
}
