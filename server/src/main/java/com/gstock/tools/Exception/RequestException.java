package com.gstock.tools.Exception;

public class RequestException extends Exception {
    public RequestException(String str) {
        super(str);
    }
}
