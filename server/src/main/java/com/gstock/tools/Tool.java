package com.gstock.tools;

import java.util.Collections;
import java.util.Set;
import java.util.TreeSet;

public class Tool {
    private Tool(){
        //Make this class abstract
    }

    private static boolean debug = false;
    private static int debugMode = -1;


    private static final Set<Integer> orderBeingEdited = Collections.synchronizedSet(new TreeSet<Integer>());
    private static final Set<Integer> articlesBeingMoved = Collections.synchronizedSet(new TreeSet<Integer>());
    private static final Set<Integer> articlesBeingStocked = Collections.synchronizedSet(new TreeSet<Integer>());

    public static Set<Integer> getOrderBeingEdited() {
        return orderBeingEdited;
    }

    public static Set<Integer> getArticlesBeingMoved() {
        return articlesBeingMoved;
    }

    public static Set<Integer> getArticlesBeingStocked() {
        return articlesBeingStocked;
    }

    public static int getDebugMode() {
        return debugMode;
    }

    public static void setDebugMode(int debugMode) {
        Tool.debugMode = debugMode;
    }

    public static boolean isDebug() {
        return debug;
    }

    public static void activateDebug() {
        Tool.debug = true;
    }
}
