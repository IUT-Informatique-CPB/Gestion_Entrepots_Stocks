package com.gstock.tools.user;

import com.gstock.tools.DatabaseConnection;
import com.gstock.tools.article.ArticleFactory;
import com.gstock.tools.deposit.DepositFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserFactory {

    private static final Logger LOGGER = Logger.getLogger( ArticleFactory.class.getName() );

    public static ServerUser getUser(int id){
        DatabaseConnection dc = new DatabaseConnection();
        Connection conn = dc.getConnection();
        String getArticle = "SELECT * FROM USER WHERE USER.id = ?";
        try ( PreparedStatement ps = conn.prepareStatement(getArticle) ) {
            ps.setInt(1, id);
            try  ( ResultSet result = ps.executeQuery() ) {
                if (result.next()) {
                    String username = result.getString(2);
                    String login = result.getString(3);
                    String password = result.getString(4);
                    int depositId = result.getInt(5);
                    return new ServerUser(id, username, login, password, DepositFactory.getDeposit(depositId));
                }
                else
                    LOGGER.log(Level.FINE, "The User with ID \"" + id + "\" was nowhere to be found" );
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
        return null;
    }


}
