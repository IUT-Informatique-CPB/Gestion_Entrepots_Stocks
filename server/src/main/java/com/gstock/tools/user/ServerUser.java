package com.gstock.tools.user;

import com.gstock.tools.deposit.Deposit;

import java.io.Serializable;
import java.net.Socket;

public class ServerUser implements Serializable {

    //REAL ATTRIBUTES
    private int id;
    private String username;
    private String login;
    private String password;
    private Deposit deposit;

    private Socket clientSocket;

    public ServerUser(int id, String username, String login, String password, Deposit deposit) {
        this.id = id;
        this.username = username;
        this.login = login;
        this.password = password;
        this.deposit = deposit;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public Deposit getDeposit() {
        return deposit;
    }

    public String getLogin() {
        return login;
    }

    public String getPassword() {
        return password;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setDeposit(Deposit deposit) {
        this.deposit = deposit;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Socket getClientSocket() {
        return clientSocket;
    }

    public void setClientSocket(Socket clientSocket) {
        this.clientSocket = clientSocket;
    }

    public User getSimpleUser() {
        return new User(id, username, login, deposit);
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append('[').append(this.getClass().getSimpleName()).append("] { ");
        str.append("id='").append(id).append("', ");
        str.append("username='").append(username).append("', ");
        str.append("login='").append(login).append("', ");
        str.append("password='").append(password).append("', ");
        str.append("deposit='").append(deposit).append("' };");
        return str.toString();
    }
}
