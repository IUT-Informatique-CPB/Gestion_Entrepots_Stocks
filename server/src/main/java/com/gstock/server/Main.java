package com.gstock.server;

import java.io.IOException;
import java.util.logging.*;

/**
 * Launcher for the whole server
 */
public class Main
{
    /**
     * Thread Listener for the server
     */
    private ServerListener serverListener = new ServerListener();

    /**
     * Logger for this class
     */
    private Logger logger = Logger.getLogger("com.gstock");

    public static void main( String[] args ) {
        System.setProperty("java.util.logging.SimpleFormatter.format",
                "[%1$tF %1$tT] [%4$-7s] %5$s%6$s%n");

        Main launcher = new Main();
        launcher.launch();
    }

    /**
     * launch the server himself and add a SIGInt Handler
     */
    private void launch() {
        try {
            initLogger();

            Runtime.getRuntime().addShutdownHook(new SIGINTCatcher());

            serverListener.start();

            serverListener.join();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Couldn't create the fileHandler to save the Logs", e);
        } catch (InterruptedException e) {
            logger.log(Level.SEVERE, "Error thrown while trying to interrupt the ServerListener", e);
            serverListener.interrupt();
        }
    }

    /**
     * Init the main Logger Outputs and Style
     * @throws IOException
     */
    private void initLogger() throws IOException {
        FileHandler fileHandler = new FileHandler("Server.log");
        fileHandler.setFormatter(new SimpleFormatter());

        ConsoleHandler consoleHandler = new ConsoleHandler();
        consoleHandler.setFormatter(new SimpleFormatter());

        logger.setUseParentHandlers(false);
        logger.addHandler(fileHandler);
        logger.addHandler(consoleHandler);
    }

    /**
     * Thread used to stop teh server when a ctrl+c is catch
     */
    private class SIGINTCatcher extends Thread {
        @Override
        public void run() {
            logger.log(Level.INFO, "Trying to Interrupt the Server");
            serverListener.interrupt();
            try {
                serverListener.join();
            } catch (InterruptedException e) {
                logger.log(Level.SEVERE, "Error thrown while trying to interrupt the ServerListener", e);
                serverListener.interrupt();
            }
        }
    }

}
