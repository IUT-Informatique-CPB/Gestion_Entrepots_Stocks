package com.gstock.server;

import com.gstock.tools.Exception.StopServerException;
import com.gstock.tools.Request;
import com.gstock.tools.user.ServerUser;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerRequest extends Thread {
    /**
     * Logger of this class. Used to log everything in the console and a log file
     */
    private Logger logger = Logger.getLogger( this.getClass().getName() );

    /**
     * List of connected Users
     */
    private ArrayList<ServerUser> clients = new ArrayList<>();

    /**
     * Queue of new Socket to add  to the clients vector
     */
    private ConcurrentLinkedQueue<ServerUser> newClients = new ConcurrentLinkedQueue<>();

    /**
     * Set of all closed socket to process
     */
    private HashSet<ServerUser> closedClients = new HashSet<>();

    /**
     * List of launched thread processing a client's request
     */
    private HashSet<ServerResponse> runningThreads = new HashSet<>();

    /**
     * Listening loop of the server for request from the clients
     */
    @Override
    public void run() {
        try {
            while (true) {
                for (ServerUser client : clients) {
                    if (!checkClient(client))
                        closedClients.add(client);
                }
                cleanUpClientList();
                addNewClients();
                if (isInterrupted())
                    throw new StopServerException("Stopping ServerRequest");
            }
        } catch (StopServerException e) {
            stopThread();
        }

        for (ServerUser client : clients) {
            try {
                client.getClientSocket().close();
            } catch (IOException e) {
                logger.log(Level.WARNING, "Exception while closing connection to client", e);
            }
        }
    }

    /**
     * Check if any socket is present in the Thread Safe Queue 'newClients', if there is, add it to the Vector 'clients'
     */
    private void addNewClients() {
        while (!newClients.isEmpty()) {
            ServerUser client = newClients.poll();
            if (checkClient(client))
                clients.add(client);
        }
    }

    /**
     * Remove any User present in the 'closedClient' Set
     */
    private void cleanUpClientList(){
        if (!closedClients.isEmpty()) {
            //For each client marked as disconnected remove it from the client
            for (ServerUser client : closedClients) {
                clients.remove(client);
                if (!client.getClientSocket().isClosed())
                    try {
                        client.getClientSocket().close();
                    } catch (IOException e) {
                        logger.log(Level.WARNING, "Exception while closing connection to client", e);
                    }
            }
            closedClients.clear();
        }
    }

    /**
     * Check if the client send something and if he did send it to the ServerRespond Thread to analyse and process.
     * @param client A User
     * @return false if the client doesn't respond or close the connection
     */
    private boolean checkClient(ServerUser client) {
        if (client.getClientSocket().isClosed())
            return false;
        boolean dataPresent = false;

        try {
            dataPresent = client.getClientSocket().getInputStream().available() > 0;
        } catch (IOException e) {
            logger.log(Level.WARNING, "Exception while reading the content in buffer from a given client socket", e);
        }
        if (dataPresent) {
            try {
                ObjectInputStream input = new ObjectInputStream(client.getClientSocket().getInputStream());
                Request clientRequest = (Request) input.readObject();
                if (clientRequest.getParameters() != null) {
                    ServerResponse responseThread = null;
                    switch (clientRequest.getParameters().charAt(0)) {
                        case 'U':
                            responseThread = new ServerResponseUtilities(this, client, clientRequest);
                            break;
                        case 'G':
                            responseThread = new ServerResponseGet(this, client, clientRequest);
                            break;
                        case 'S':
                            responseThread = new ServerResponseSet(this, client, clientRequest);
                            break;
                        default:
                            logger.log(Level.WARNING, "Unknown Request : \"" + clientRequest.getParameters() + "\"");
                            break;
                    }
                    if (responseThread != null) {
                        runningThreads.add(responseThread);
                        responseThread.start();
                    }
                }
            } catch (IOException | ClassNotFoundException e) {
                logger.log(Level.SEVERE, "Exception on the reception of a request", e);
            }
        }
        return true;
    }

    /**
     * Add a client to the clients list
     * @param newClient The client
     */
    public void addClient(ServerUser newClient) {
        if (newClient != null && !newClient.getClientSocket().isClosed() && !isInterrupted())
            newClients.add(newClient);
    }

    /**
     * Called by a ServerResponse Thread that finished processing a request, will remove it from the runningThread Set
     * @param finishedThread the Thread that launched finished processing the request
     */
    public void removeRunningThread(ServerResponse finishedThread) {
        if (!isInterrupted())
            runningThreads.remove(finishedThread);
    }


    /**
     * Method called to stop the thread safely
     */
    private void stopThread() {
        for (Thread t : runningThreads)
            t.interrupt();
        super.interrupt();

        logger.log(Level.INFO, "ServerRequest Stopped");
    }
}
