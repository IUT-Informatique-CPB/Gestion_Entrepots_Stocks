package com.gstock.server;

import com.gstock.tools.Exception.InsufficientArticlesException;
import com.gstock.tools.Request;
import com.gstock.tools.address.Address;
import com.gstock.tools.address.AddressFactory;
import com.gstock.tools.article.ArticleCategory;
import com.gstock.tools.article.ArticleCategoryFactory;
import com.gstock.tools.order.Order;
import com.gstock.tools.order.OrderFactory;
import com.gstock.tools.user.ServerUser;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServerResponseUtilities extends ServerResponse {

    /**
     * Create a new ServerResponse that will treat the request of the client.
     * @param requestThread To notify it that the thread has finished its job
     * @param client The client that issued the request
     * @param clientRequest The request issued by the client
     */
    public ServerResponseUtilities(ServerRequest requestThread, ServerUser client, Request clientRequest) {
        super(requestThread, client, clientRequest);
    }

    /**
     * Select the correct method to execute for this specific request of a client
     */
    @Override
    public void execRequest(Connection conn) {
        switch (clientRequest.getParameters()) {
            case "ULI":
                //Do nothing used only on the first connection
                break;
            case "ULO":
                try {
                    client.getClientSocket().close();
                    logger.log(Level.INFO, "a client disconnect successfully");
                } catch (IOException e) {
                    logger.log(Level.WARNING, "Error while disconnecting a client", e);
                }
                break;
            case "UCP":
                rUCP(conn);
                break;
            default:
                break;
        }
    }

    /**
     * Send a Map of Address to pick from with the number of ArticleCategory to pick
     * The client specify the Address to refill and the ArticleCategory
     * @param conn Connexion to the database
     */
    private void rUCP(Connection conn) {

        int orderID = (int) clientRequest.getData();

        TreeMap<Address, HashMap<ArticleCategory, Integer>> result = new TreeMap<>();
        Order o = OrderFactory.getOrder(orderID);

        try {
            for (Map.Entry<Integer, Integer> article : o.getArticles().entrySet()) {
                ArticleCategory ac = ArticleCategoryFactory.getArticleCategory(article.getKey());
                if (ac != null) {
                    Map<Address, Integer> addresses = getArticlesAddresses(getClass().getName(), conn, client, ac.getId(), article.getValue(), true);
                    for (Map.Entry<Address, Integer> address : addresses.entrySet()) {
                        if (!result.containsKey(address.getKey()))
                            result.put(address.getKey(), new HashMap<>());
                        result.get(address.getKey()).put(ArticleCategoryFactory.getArticleCategory(article.getKey()), address.getValue());
                    }
                }
            }
            response = new Request("UCP", result);
        } catch (InsufficientArticlesException e) {
            logger.log(Level.WARNING, "Not Enough Items", e);
        }
    }

    public static Map<Address, Integer> getArticlesAddresses(String className, Connection conn, ServerUser client, Integer articleID, Integer nbArticlesNeeded, boolean picking) throws InsufficientArticlesException{
        final StringBuilder getArticleLocation = new StringBuilder();
        getArticleLocation.append("SELECT ADDRESS.*, POSSESSES.Articles_Number FROM POSSESSES ")
        .append("INNER JOIN ADDRESS ON POSSESSES.Address_id = ADDRESS.id ")
        .append("INNER JOIN ARTICLE ON POSSESSES.Article_id = ARTICLE.id ")
        .append("INNER JOIN SHOP ON ADDRESS.Shop_id = SHOP.id ")
        .append("INNER JOIN DEPOSIT ON SHOP.Deposit_id = DEPOSIT.id ")
        .append("INNER JOIN ARTICLE_CATEGORY ON ARTICLE_CATEGORY.id = ARTICLE.ArticleCategory_id ")
        .append("WHERE ARTICLE_CATEGORY.id = ? ")
        .append("AND DEPOSIT.id = ? ");

        if (picking){
            getArticleLocation.append("AND ( ADDRESS.Position_Number = 0 ");
            getArticleLocation.append("OR ADDRESS.Position_Number = 1 ) ");
        } else {
            getArticleLocation.append("AND ( ADDRESS.Position_Number = 2 ");
            getArticleLocation.append("OR ADDRESS.Position_Number = 3 ");
            getArticleLocation.append("OR ADDRESS.Position_Number = 4 ) ");
        }

        TreeMap<Address, Integer> addresses = new TreeMap<>();

        try (PreparedStatement ps = conn.prepareStatement(getArticleLocation.toString())) {
            ps.setInt(1, articleID);
            ps.setInt(2, client.getDeposit().getId());
            try (ResultSet result = ps.executeQuery()) {
                while (result.next()) {
                    Address a = AddressFactory.getAddress(result);
                    addresses.put( a, result.getInt(8));
                }
            }
        } catch (SQLException e) {
            Logger.getLogger(className).log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }

        if (nbArticlesNeeded > 0 && addresses.size() == 0) {
            if (picking)
                throw new InsufficientArticlesException("Needed " + nbArticlesNeeded + " could not find any PICKING address with this article [" + articleID + "]");
            else
                throw new InsufficientArticlesException("Needed " + nbArticlesNeeded + " could not find any RESERVE address with this article [" + articleID + "]");
        }

        TreeMap<Address, Integer> usedAddresses = new TreeMap<>();
        int picked = 0;
        int needed = nbArticlesNeeded;
        for (Map.Entry<Address, Integer> e : addresses.entrySet()) {
            int pickedNumberHere = Math.min(needed, e.getValue());
            picked += pickedNumberHere;
            needed -= pickedNumberHere;
            if (pickedNumberHere > 0) {
                usedAddresses.put(e.getKey(), pickedNumberHere);
            }
        }

        if (needed > 0) {
            if (picking) {
                TreeMap<Address, Integer> result = (TreeMap<Address, Integer>) getArticlesAddresses(className, conn, client, articleID, needed, false);
                for (Map.Entry<Address, Integer> e : result.entrySet()) {
                    usedAddresses.put(e.getKey(), e.getValue());
                    needed -= e.getValue();
                }
                if (needed > 0)
                    throw new InsufficientArticlesException("Needed " + nbArticlesNeeded + " could only find " + picked + " in the PICKING addresses with the article [" + articleID + "]");
            } else {
                throw new InsufficientArticlesException("Needed " + nbArticlesNeeded + " could only find " + picked + " in the RESERVE addresses with the article [" + articleID + "]");
            }
        }

        return usedAddresses;
    }
}
