package com.gstock.server;

import com.gstock.tools.DatabaseConnection;
import com.gstock.tools.Request;
import com.gstock.tools.user.ServerUser;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Abstract Class that process a client request in a separated Thread.
 */
public abstract class ServerResponse extends Thread {
    /**
     * Logger of this class. Used to log everything in the console and a log file
     */
    protected Logger logger = Logger.getLogger(this.getClass().getName());

    /**
     * The Thread that create this one, used to remove a finished ServerResponse from the list of active one
     */
    private ServerRequest parentThread;

    /**
     * The client that sent the Request
     */
    protected ServerUser client;

    /**
     * The Request sent by the client that will now by processed
     */
    protected Request clientRequest;

    /**
     * The Request sent by the client that will now by processed
     */
    protected Request response = null;

    /**
     * Create a new ServerResponse that will treat the request of the client.
     *
     * @param requestThread To notify it that the thread has finished its job
     * @param client        The client that issued the request
     * @param clientRequest The request issued by the client
     */
    ServerResponse(ServerRequest requestThread, ServerUser client, Request clientRequest) {
        this.parentThread = requestThread;
        this.client = client;
        this.clientRequest = clientRequest;
    }

    /**
     * Method called on Thread Startup
     */
    @Override
    public void run() {
        DatabaseConnection dc = new DatabaseConnection();
        try (Connection conn = dc.getConnection()) {
            execRequest(conn);
            sendRequestToClient();
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't connect to the DB", e);
        }
        parentThread.removeRunningThread(this);
    }

    /**
     * Send Data a request to the client
     */
    private void sendRequestToClient() {
        if (response == null)
            response = new Request("E", null);
        try {
            ObjectOutputStream output = new ObjectOutputStream(client.getClientSocket().getOutputStream());
            output.writeObject(response);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error while sending a Request back to the client", e);
        }
    }

    /**
     * Abstract method that should process some specifics Requests from a client
     */
    public abstract void execRequest(Connection conn);
}
