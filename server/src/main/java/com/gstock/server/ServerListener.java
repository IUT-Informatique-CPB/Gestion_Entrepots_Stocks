package com.gstock.server;

import com.gstock.tools.DatabaseConnection;
import com.gstock.tools.Exception.RequestException;
import com.gstock.tools.Exception.StopServerException;
import com.gstock.tools.Request;
import com.gstock.tools.user.ServerUser;
import com.gstock.tools.user.UserFactory;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * A thread that look for any incoming connection
 */
public class ServerListener extends Thread {
    /**
     * Logger of this class. Used to log everything in the console and a log file
     */
    private Logger logger = Logger.getLogger( this.getClass().getName() );

    /**
     * Socket used by the server to accept new Connections
     */
    private ServerSocket serverSocket = null;

    /**
     * The thread that will get and process the Request of the clients
     */
    private ServerRequest requestThread = null;

    private Vector<Thread> authenticators = new Vector<>();

    /**
     * Loop waiting for a new connection from a client.
     */
    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(2048);

            requestThread = new ServerRequest();
            requestThread.start();

            logger.log(Level.INFO, "Starting the server listener");

            while (true) {
                logger.log(Level.INFO, "Waiting for connection");

                waitForConnection();

                if (isInterrupted())
                    throw new StopServerException("Stopping ServerListener");
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error while opening the server Socket", e);
        } catch (StopServerException e) {
            stopThread();
        }
    }

    /**
     * Accept a connection and create an Authenticator thread to see if the login is correct
     */
    private void waitForConnection() {
        try {
            Socket newClient = serverSocket.accept();
            logger.log(Level.INFO, "Client trying to connect");
            Authenticator a = new Authenticator(newClient);
            authenticators.add(a);
            a.start();
        } catch (IOException e) {
            if (!serverSocket.isClosed())
                logger.log(Level.WARNING, "Couldn't accept incoming connection", e);
        }
    }

    /**
     * Override of the Thread interrupt method
     */
    @Override
    public void interrupt() {
        if (serverSocket != null && !serverSocket.isClosed()) {
            try {
                serverSocket.close();
            } catch (IOException ignored) {
                logger.log(Level.SEVERE, "Couldn't close the serverSocket");
            }
        }
        super.interrupt();
    }

    /**
     * Method called to stop the thread safely
     */
    private void stopThread() {
        try {
            serverSocket.close();

            requestThread.interrupt();
            requestThread.join();
        } catch (InterruptedException e) {
            logger.log(Level.INFO, "ServerListener InterruptedException");
            requestThread.interrupt();
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Error while closing the server Socket", e);
        }

        logger.log(Level.INFO, "ServerListener Stopped");
    }

    /**
     * Check if the client authentication is correct.
     * If the client sent correct credentials, the socket is added to the ServerRequest Thread.
     * Else the connection is closed
     */
    private class Authenticator extends Thread {
        /**
         * The client socket
         */
        Socket client;

        /**
         * Boolean used to save if the client sent correct ids
         */
        boolean isValidated = false;

        /**
         * Request sent by the client
         */
        Request login = null;

        /**
         * User representing the client that will be saved on the server
         */
        ServerUser user = null;

        /**
         * @param client the socket that connect with a client that try to authentify himself on the server
         */
        Authenticator(Socket client) {
            this.client = client;
        }

        /**
         * Core method of the Thread
         */
        @Override
        public void run() {
            try {
                getClientRequest();
                processRequest();
            } catch (IOException | ClassNotFoundException e) {
                logger.log(Level.SEVERE, "Exception while receiving the client login information", e);
            } catch (RequestException e) {
                logger.log(Level.WARNING, "Incorrect Request", e);
            } catch (SQLException e) {
                logger.log(Level.WARNING, "Error while making a request to the DB", e);
            }
            sendToClient();
            authenticators.remove(this);
        }

        /**
         * Used to receive the client's Request and store it
         * @throws IOException
         * @throws ClassNotFoundException
         * @throws RequestException
         */
        private void getClientRequest() throws IOException, ClassNotFoundException, RequestException {
            ObjectInputStream inStream = new ObjectInputStream(client.getInputStream());
            login = (Request) inStream.readObject();

            if (login == null)
                throw new RequestException("The client didn't send any connection information");
            if (!login.getParameters().equals("ULI"))
                throw new RequestException("The client didn't send the correct Request");
        }

        /**
         * Ask the database if the client identity is correct
         * @throws SQLException
         */
        private void processRequest() throws SQLException {
            String[] userInformation = (String[]) login.getData();
            String id = userInformation[0];
            String password = userInformation[1];

            DatabaseConnection database = new DatabaseConnection();
            Connection dbConnection = database.getConnection();

            try (PreparedStatement ps = dbConnection.prepareStatement
                    ("SELECT id FROM USER WHERE Login = ? AND Password = ?")) {

                ps.setString(1, id);
                ps.setString(2, password);

                try (ResultSet result = ps.executeQuery()) {
                    if (result.next()) {
                        user = UserFactory.getUser(result.getInt(1));
                        if (result.getObject(1) != null)
                            isValidated = true;
                    }
                }
            }
        }

        /**
         * Send back a response to the client to let him know if teh login was a success
         */
        private void sendToClient() {
            try {
                ObjectOutputStream outputStream = new ObjectOutputStream(client.getOutputStream());
                if (isValidated && user != null) {
                    user.setClientSocket(client);
                    Request validation = new Request("ULI", user.getSimpleUser());
                    outputStream.writeObject(validation);
                    requestThread.addClient(user);
                    logger.log(Level.INFO, "Client authentication succeed");
                } else {
                    Request validation = new Request("ULI", null);
                    outputStream.writeObject(validation);
                    client.close();
                    logger.log(Level.INFO, "Client authentication failed");
                }
            } catch (IOException e) {
                logger.log(Level.SEVERE, "Error while sending a response to the client after the connection", e);
            }
        }
    }
}
