package com.gstock.server;

import com.gstock.tools.Exception.InsufficientArticlesException;
import com.gstock.tools.address.Address;
import com.gstock.tools.address.AddressFactory;
import com.gstock.tools.article.Article;
import com.gstock.tools.Request;
import com.gstock.tools.article.ArticleCategory;
import com.gstock.tools.article.ArticleCategoryFactory;
import com.gstock.tools.article.ArticleFactory;
import com.gstock.tools.deposit.Deposit;
import com.gstock.tools.deposit.DepositFactory;
import com.gstock.tools.order.Order;
import com.gstock.tools.order.OrderFactory;
import com.gstock.tools.user.ServerUser;
import javafx.util.Pair;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.logging.Level;

/**
 * Treat a client Requests that only ask for data from the database in a different thread
 */
public class ServerResponseGet extends ServerResponse {

    /**
     * Create a new ServerResponse that will treat the request of the client.
     *
     * @param requestThread To notify it that the thread has finished its job
     * @param client        The client that issued the request
     * @param clientRequest The request issued by the client
     */
    public ServerResponseGet(ServerRequest requestThread, ServerUser client, Request clientRequest) {
        super(requestThread, client, clientRequest);
    }

    /**
     * Select the correct method to execute for this specific request of a client
     */
    @Override
    public void execRequest(Connection conn) {
        switch (clientRequest.getParameters()) {
            case "GAC":
                rGAC(conn);
                break;
            case "GACN":
                rGACN(conn);
                break;
            case "GAE":
                rGAE(conn);
                break;
            case "GAIL":
                rGAIL();
                break;
            case "GAMEL":
                rGAMEL(conn);
                break;
            case "GAML":
                rGAML(conn);
                break;
            case "GAREL":
                rGAREL(conn);
                break;
            case "GAP":
                rGAP(conn);
                break;
            case "GAPR":
                rGAPR(conn);
                break;
            case "GAR":
                rGAR(conn);
                break;
            case "GCLA":
                rGCLA();
                break;
            case "GCLC":
                rGCLC(conn, false);
                break;
            case "GCLU":
                rGCLU(conn);
                break;
            case "GCN":
                rGCN(conn);
                break;
            case "GDL":
                rGDL(conn);
                break;
            case "GE":
                rGE(conn);
                break;
            case "GELA":
                rGELA(conn);
                break;
            default:
                logger.log(Level.SEVERE, "[" + getClass().getSimpleName() + "] Unknown Request parameter : '" + clientRequest.getParameters() + "'");
                break;
        }
    }

    /**
     * Return the articleCategory according to the barCode
     * @param conn Connexion to the database
     */
    private void rGAC(Connection conn) {
        final StringBuilder getArticleCategory = new StringBuilder();
        getArticleCategory.append("SELECT ARTICLE_CATEGORY.id FROM ARTICLE_CATEGORY ");
        getArticleCategory.append("WHERE ARTICLE_CATEGORY.Bar_Code = ?");

        String barCode = (String)clientRequest.getData();

        try (PreparedStatement ps = conn.prepareStatement(getArticleCategory.toString())) {
            ps.setString(1, barCode);
            try (ResultSet result = ps.executeQuery()) {
                if(result.next()){
                    int id = result.getInt(1);
                    response = new Request("GAC", ArticleCategoryFactory.getArticleCategory(id));
                }else {
                    response = new Request("GAC", null);
                }
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
    }

    /**
     * Return Some articleCategories with the name matching the given String
     * @param conn Connexion to the database
     */
    private void rGACN(Connection conn) {
        final StringBuilder getArticleCategory = new StringBuilder();
        getArticleCategory.append("SELECT ARTICLE_CATEGORY.* FROM ARTICLE_CATEGORY ");
        getArticleCategory.append("WHERE ARTICLE_CATEGORY.Name LIKE ?");

        String name = "%" + (String)clientRequest.getData() + "%";
        ArrayList<ArticleCategory> articleCategories = new ArrayList<>();
        try (PreparedStatement ps = conn.prepareStatement(getArticleCategory.toString())) {
            ps.setString(1, name);
            try (ResultSet result = ps.executeQuery()) {
                while (result.next()) {
                    articleCategories.add(ArticleCategoryFactory.getArticleCategory(result));
                }
                response = new Request("GACN", articleCategories);
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
    }

    /**
     * Send a list of Addresses where the a given article is stored to the client
     *
     * @param conn Connexion to the database
     */
    private void rGAE(Connection conn) {
        final StringBuilder getArticleLocation = new StringBuilder();
        getArticleLocation.append("SELECT ADDRESS.id FROM POSSESSES ");
        getArticleLocation.append("INNER JOIN ADDRESS ON POSSESSES.Address_id = ADDRESS.id ");
        getArticleLocation.append("INNER JOIN ARTICLE ON POSSESSES.Article_id = ARTICLE.id ");
        getArticleLocation.append("INNER JOIN SHOP ON ADDRESS.Shop_id = SHOP.id ");
        getArticleLocation.append("INNER JOIN DEPOSIT ON SHOP.Deposit_id = DEPOSIT.id ");
        getArticleLocation.append("WHERE ARTICLE.id = ? ");
        getArticleLocation.append("AND DEPOSIT.id = ?");


        Integer articleID = (Integer) clientRequest.getData();

        try (PreparedStatement ps = conn.prepareStatement(getArticleLocation.toString())) {
            ps.setInt(1, articleID);
            ps.setInt(2, client.getDeposit().getId());
            try (ResultSet result = ps.executeQuery()) {
                ArrayList<Address> addresses = new ArrayList<>();
                while (result.next()) {
                    addresses.add(AddressFactory.getAddress(result.getInt(1)));
                }
                response = new Request("GAE", addresses);
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
    }

    /**
     * Send a list of Articles corresponding to the list of article ID sent by the client
     */
    private void rGAIL() {
        List<Integer> articlesID = (List<Integer>) clientRequest.getData();
        ArrayList<Article> articles = new ArrayList<>();
        for (Integer id : articlesID)
            articles.add(ArticleFactory.getArticle(id));

        response = new Request("GAIL", articles);
    }

    /**
     * Send back a Map with Addresses that need to be refilled and the number of a given ArticleCategory given
     * @param conn Connexion to the database
     */
    private void rGAMEL(Connection conn) {
        final StringBuilder possibleAddresses = new StringBuilder();
        possibleAddresses.append("SELECT ADDRESS.*, ARTICLE.*, ARTICLE_CATEGORY.*, POSSESSES.Articles_Number ")
                .append("FROM POSSESSES ")
                .append("INNER JOIN ADDRESS ON POSSESSES.Address_id = ADDRESS.id ")
                .append("INNER JOIN ARTICLE ON POSSESSES.Article_id = ARTICLE.id ")
                .append("INNER JOIN ARTICLE_CATEGORY ON ARTICLE.ArticleCategory_id = ARTICLE_CATEGORY.id ")
                .append("INNER JOIN SHOP ON ADDRESS.Shop_id = SHOP.id ")
                .append("INNER JOIN DEPOSIT ON SHOP.Deposit_id = DEPOSIT.id ")
                .append("WHERE DEPOSIT.id = ? ")
                .append("AND ARTICLE_CATEGORY.id = ? ")
                .append("AND ( ADDRESS.Position_Number = 0 ")
                .append("OR ADDRESS.Position_Number = 1 ) ")
                .append("GROUP BY Address_id ")
                .append("HAVING COUNT(*) < ARTICLE_CATEGORY.MaxPalette ");

        TreeMap<Address, Integer> addressArticleList = new TreeMap<>();
        Integer articleCatID = (Integer) clientRequest.getData();

        try (PreparedStatement ps = conn.prepareStatement(possibleAddresses.toString())) {
            ps.setInt(1, client.getDeposit().getId());
            ps.setInt(2, articleCatID);
            try (ResultSet result = ps.executeQuery()) {
                while (result.next()) {
                    Address address = AddressFactory.getAddress(result);
                    ArticleCategory category = ArticleCategoryFactory.getArticleCategory(result, 13);
                    Integer nbArticles = result.getInt(20);

                    Integer totalArticle = nbArticles;
                    if (addressArticleList.containsKey(address))
                        totalArticle += addressArticleList.get(address);


                    boolean full = true;
                    switch (address.getSize()) {
                        case "Palette":
                            if (category.getMaxPalette() > totalArticle)
                                full = false;
                            break;
                        case "Palette USA":
                        case "PaletteUSA":
                            if (category.getMaxPaletteUSA() > totalArticle)
                                full = false;
                            break;
                        case "Palette Europe":
                        case "PaletteEurope":
                            if (category.getMaxPaletteEurope() > totalArticle)
                                full = false;
                            break;
                        default:
                            logger.log(Level.WARNING, "Unknown Size : " + address.getSize());
                            break;
                    }
                    if (full && !addressArticleList.containsKey(address))
                        addressArticleList.remove(address);
                    else if (!full)
                        addressArticleList.put(address, totalArticle);
                }
            }

            TreeMap<Address, Integer> articles = new TreeMap<>();
            for (Map.Entry<Address, Integer> e : addressArticleList.entrySet())
                articles.put(e.getKey(), e.getValue());

            response = new Request("GAMEL", articles);
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
    }

    /**
     * Send a list of Articles that need to be moved to the client
     *
     * @param conn Connexion to the database
     */
    private void rGAML(Connection conn) {
        final StringBuilder possibleAddresses = new StringBuilder();
        possibleAddresses.append("SELECT ADDRESS.*, ARTICLE.*, ARTICLE_CATEGORY.*, POSSESSES.Articles_Number ")
                .append("FROM POSSESSES ")
                .append("INNER JOIN ADDRESS ON POSSESSES.Address_id = ADDRESS.id ")
                .append("INNER JOIN ARTICLE ON POSSESSES.Article_id = ARTICLE.id ")
                .append("INNER JOIN ARTICLE_CATEGORY ON ARTICLE.ArticleCategory_id = ARTICLE_CATEGORY.id ")
                .append("INNER JOIN SHOP ON ADDRESS.Shop_id = SHOP.id ")
                .append("INNER JOIN DEPOSIT ON SHOP.Deposit_id = DEPOSIT.id ")
                .append("WHERE DEPOSIT.id = ? ")
                .append("AND ( ADDRESS.Position_Number = 0 ")
                .append("OR ADDRESS.Position_Number = 1 ) ")
                .append("GROUP BY Address_id ")
                .append("HAVING COUNT(*) < ARTICLE_CATEGORY.MaxPalette ");

        TreeMap<Address, Pair<ArticleCategory, Integer>> addressArticleList = new TreeMap<>();

        try (PreparedStatement ps = conn.prepareStatement(possibleAddresses.toString())) {
            ps.setInt(1, client.getDeposit().getId());
            try (ResultSet result = ps.executeQuery()) {
                while (result.next()) {
                    Address address = AddressFactory.getAddress(result);
                    Article article = ArticleFactory.getArticle(result, 7);
                    ArticleCategory category = ArticleCategoryFactory.getArticleCategory(result, 13);
                    Integer nbArticles = result.getInt(20);

                    Integer totalArticle = nbArticles;
                    if (addressArticleList.containsKey(address) &&
                            addressArticleList.get(address).getKey().getId() == category.getId())
                        totalArticle += addressArticleList.get(address).getValue();


                    boolean full = true;
                    switch (address.getSize()) {
                        case "Palette":
                            if (category.getMaxPalette() > totalArticle)
                                full = false;
                            break;
                        case "PaletteUSA":
                        case "Palette USA":
                            if (category.getMaxPaletteUSA() > totalArticle)
                                full = false;
                            break;
                        case "Palette Europe":
                        case "PaletteEurope":
                            if (category.getMaxPaletteEurope() > totalArticle)
                                full = false;
                            break;
                        default:
                            logger.log(Level.WARNING, "Unknown Size : " + address.getSize());
                            break;
                    }
                    if (full && !addressArticleList.containsKey(address)) {
                        addressArticleList.remove(address);
                    } else if (!full) {
                        addressArticleList.put(address, new Pair<>(category, totalArticle));
                    }
                }
            }

            TreeSet<ArticleCategory> articles = new TreeSet<>();
            for (Map.Entry<Address, Pair<ArticleCategory, Integer>> e : addressArticleList.entrySet())
                articles.add(e.getValue().getKey());

            response = new Request("GAML", articles);
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
    }

    /**
     * Send a Map of Reserve Address to pick from with the number of ArticleCategory to pick
     * The client specify the Address to refill and the ArticleCategory
     * @param conn Connexion to the database
     */
    private void rGAREL(Connection conn) {
        Integer[] id = (Integer[]) clientRequest.getData();
        Integer idCategory = id[0];
        Integer idAddress = id[1];

        int needed = 0;
        int nbArticles = 0;

        ArticleCategory category = ArticleCategoryFactory.getArticleCategory(idCategory);
        Address address = AddressFactory.getAddress(idAddress);

        StringBuilder getArticles = new StringBuilder();
        getArticles.append("SELECT POSSESSES.Articles_Number FROM ARTICLE ");
        getArticles.append("INNER JOIN POSSESSES ON POSSESSES.Article_id = ARTICLE.id ");
        getArticles.append("INNER JOIN ADDRESS ON ADDRESS.id = POSSESSES.Address_id ");
        getArticles.append("INNER JOIN SHOP ON ADDRESS.Shop_id = SHOP.id ");
        getArticles.append("INNER JOIN DEPOSIT ON SHOP.Deposit_id = SHOP.id ");
        getArticles.append("INNER JOIN ARTICLE_CATEGORY on ARTICLE.ArticleCategory_id = ARTICLE_CATEGORY.id ");
        getArticles.append("WHERE ARTICLE_CATEGORY.id = ? ");
        getArticles.append("AND ADDRESS.id = ? AND DEPOSIT.id = ?");

        try (PreparedStatement ps = conn.prepareStatement(getArticles.toString())) {
            ps.setInt(1, idCategory);
            ps.setInt(2, idAddress);
            ps.setInt(3, client.getDeposit().getId());

            try (ResultSet result = ps.executeQuery()) {
                while (result.next()) {
                    nbArticles += result.getInt(1);
                }
            }

            switch (address.getSize()) {
                case "Palette":
                    needed = category.getMaxPalette() - nbArticles;
                    break;
                case "PaletteEurope":
                case "Palette Europe":
                    needed = category.getMaxPaletteEurope() - nbArticles;
                    break;
                case "PaletteUSA":
                case "Palette USA":
                    needed = category.getMaxPaletteUSA() - nbArticles;
                    break;
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }

        try {
            response = new Request("GAREL", ServerResponseUtilities.getArticlesAddresses(getClass().getName(), conn, client, idCategory, needed, false));
        } catch (InsufficientArticlesException e) {
            logger.log(Level.WARNING, "Not Enough Items", e);
        }
    }

    /**
     * Send a list of addresses of picking
     * @param conn Connexion to the database
     */
    private void rGAP(Connection conn) {
        final StringBuilder getAddress = new StringBuilder();
        getAddress.append("SELECT ADDRESS.* FROM ADDRESS ");
        getAddress.append("INNER JOIN SHOP ON ADDRESS.Shop_id = SHOP.id ");
        getAddress.append("INNER JOIN DEPOSIT ON SHOP.Deposit_id = DEPOSIT.id ");
        getAddress.append("AND DEPOSIT.id = ? ");
        getAddress.append("AND ( ADDRESS.Position_Number = 0 ");
        getAddress.append("OR ADDRESS.Position_Number = 1 ) ");

        try (PreparedStatement ps = conn.prepareStatement(getAddress.toString())) {
            ps.setInt(1, client.getDeposit().getId());
            try (ResultSet result = ps.executeQuery()) {
                ArrayList<Address> addressPicking = new ArrayList<>();
                while (result.next()) {
                    addressPicking.add(AddressFactory.getAddress(result));
                }
                response = new Request("GAP", addressPicking);
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
    }

    /**
     * Send a list of addresses of picking and reserve
     * @param conn Connexion to the database
     */
    private void rGAPR(Connection conn) {
        final StringBuilder getAddress = new StringBuilder();
        getAddress.append("SELECT ADDRESS.* FROM ADDRESS ");
        getAddress.append("INNER JOIN SHOP ON ADDRESS.Shop_id = SHOP.id ");
        getAddress.append("INNER JOIN DEPOSIT ON SHOP.Deposit_id = DEPOSIT.id ");
        getAddress.append("AND DEPOSIT.id = ?");

        try (PreparedStatement ps = conn.prepareStatement(getAddress.toString())) {
            ps.setInt(1, client.getDeposit().getId());
            try (ResultSet result = ps.executeQuery()) {
                ArrayList<Address> addressReserve = new ArrayList<>();
                while (result.next()) {
                    addressReserve.add(AddressFactory.getAddress(result));
                }
                response = new Request("GAPR", addressReserve);
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
    }

    /**
     * Send a list of addresses of reserve
     * @param conn Connexion to the database
     */
    private void rGAR(Connection conn) {
        final StringBuilder getAddress = new StringBuilder();
        getAddress.append("SELECT ADDRESS.* FROM ADDRESS ");
        getAddress.append("INNER JOIN SHOP ON ADDRESS.Shop_id = SHOP.id ");
        getAddress.append("INNER JOIN DEPOSIT ON SHOP.Deposit_id = DEPOSIT.id ");
        getAddress.append("AND DEPOSIT.id = ? ");
        getAddress.append("AND ( ADDRESS.Position_Number = 2 ");
        getAddress.append("OR ADDRESS.Position_Number = 3 ");
        getAddress.append("OR ADDRESS.Position_Number = 4 ) ");

        try (PreparedStatement ps = conn.prepareStatement(getAddress.toString())) {
            ps.setInt(1, client.getDeposit().getId());
            try (ResultSet result = ps.executeQuery()) {
                ArrayList<Address> addressReserve = new ArrayList<>();
                while (result.next()) {
                    addressReserve.add(AddressFactory.getAddress(result));
                }
                response = new Request("GAR", addressReserve);
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
    }

    /**
     * Send a list of Articles present in an Order to the client
     */
    private void rGCLA() {
        //TODO CHNAGE ARTICLE TO AC
        int id = (int) clientRequest.getData();
        Order order = OrderFactory.getOrder(id);

        HashMap<ArticleCategory, Integer> articles = new HashMap<>();

        for (Map.Entry<Integer, Integer> entry : order.getArticles().entrySet()) {
            ArticleCategory article = ArticleCategoryFactory.getArticleCategory(entry.getKey());
            articles.put(article, entry.getValue());
        }

        response = new Request("GCLA", articles);
    }


    /**
     * Send a list of Order to the client
     * The boolean specify if we search for the completed order the non completed ones
     *
     * @param conn Connexion to the database
     * @param bool Specify if the list if of completed or uncompleted Order
     */
    private void rGCLC(Connection conn, Boolean bool) {
        final StringBuilder getIdCommand = new StringBuilder();
        getIdCommand.append("SELECT ORDERS.id FROM ORDERS ");
        getIdCommand.append("WHERE ORDERS.Picker_id IS NULL ");
        getIdCommand.append("AND ORDERS.Completed = ? ");
        getIdCommand.append("AND Deposit_id = ?");

        try (PreparedStatement ps = conn.prepareStatement(getIdCommand.toString())) {
            ps.setBoolean(1, bool);
            ps.setInt(2, client.getDeposit().getId());
            try (ResultSet result = ps.executeQuery()) {
                ArrayList<Order> orders = new ArrayList<>();
                while (result.next()) {
                    orders.add(OrderFactory.getOrder(result.getInt(1)));
                }
                response = new Request("GCLC", orders);
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
    }

    /**
     * Send a list of Orders that are not assigned to any picker to the client
     *
     * @param conn Connexion to the database
     */
    private void rGCLU(Connection conn) {
        final StringBuilder getCommandWithoutPicker = new StringBuilder();
        getCommandWithoutPicker.append("SELECT * FROM ORDERS ");
        getCommandWithoutPicker.append("WHERE Deposit_id = ? AND Picker_id is NULL");

        try (PreparedStatement ps = conn.prepareStatement(getCommandWithoutPicker.toString())) {
            ps.setInt(1, client.getDeposit().getId());
            try (ResultSet result = ps.executeQuery()) {
                ArrayList<Order> orders = new ArrayList<>();
                while (result.next()) {
                    orders.add(OrderFactory.getOrder(result));
                }
                response = new Request("GCLU", orders);
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
    }

    /**
     * Send the client name related to an order
     * @param conn Connexion to the database
     */
    private void rGCN(Connection conn) {
        final StringBuilder clientName = new StringBuilder();
        clientName.append("SELECT CUSTOMER.Name FROM CUSTOMER ");
        clientName.append("INNER JOIN ORDERS ON CUSTOMER.id = ORDERS.Customer_id ");
        clientName.append("WHERE ORDERS.id = ?");

        int id = (int) clientRequest.getData();

        try (PreparedStatement ps = conn.prepareStatement(clientName.toString())) {
            ps.setInt(1, id);
            try (ResultSet result = ps.executeQuery()) {
                result.next();
                String name = result.getString(1);
                response = new Request("GCN", name);
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
    }

    /**
     * Send a list of Deposits to the client
     *
     * @param conn Connexion to the database
     */
    private void rGDL(Connection conn) {
        final String getArticleFromEmplacement = "SELECT * FROM DEPOSIT";

        try (PreparedStatement ps = conn.prepareStatement(getArticleFromEmplacement)) {
            try (ResultSet result = ps.executeQuery()) {
                ArrayList<Deposit> depositList = new ArrayList<>();
                while (result.next()) {
                    depositList.add(DepositFactory.getDeposit(result));
                }
                response = new Request("GDL", depositList);
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
    }

    /**
     * Send a list of Articles present at one address to the client
     *
     * @param conn Connexion to the database
     */
    private void rGE(Connection conn) {
        final StringBuilder getArticleFromEmplacement = new StringBuilder();
        getArticleFromEmplacement.append("SELECT POSSESSES.Article_id, POSSESSES.Articles_Number FROM POSSESSES ");
        getArticleFromEmplacement.append("INNER JOIN ADDRESS ON POSSESSES.Address_id = ADDRESS.id ");
        getArticleFromEmplacement.append("INNER JOIN ARTICLE ON POSSESSES.Article_id = ARTICLE.id ");
        getArticleFromEmplacement.append("INNER JOIN SHOP ON ADDRESS.Shop_id = SHOP.id ");
        getArticleFromEmplacement.append("INNER JOIN DEPOSIT ON SHOP.Deposit_id = DEPOSIT.id ");
        getArticleFromEmplacement.append("WHERE ADDRESS.id= ? ");
        getArticleFromEmplacement.append("AND DEPOSIT.id = ? ");

        Integer addressID = (Integer) clientRequest.getData();

        try (PreparedStatement ps = conn.prepareStatement(getArticleFromEmplacement.toString())) {
            ps.setInt(1, addressID);
            ps.setInt(2, client.getDeposit().getId());
            try (ResultSet result = ps.executeQuery()) {
                HashMap<Article, Integer> articlesList = new HashMap<>();
                while (result.next())
                    articlesList.put(ArticleFactory.getArticle(result.getInt(1)), result.getInt(2));
                response = new Request("GE", articlesList);
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
    }

    /**
     * Send a list of Addresses that are free to the client
     *
     * @param conn Connexion to the database
     */
    private void rGELA(Connection conn) {
        final StringBuilder freeAddresses = new StringBuilder();
        freeAddresses.append("SELECT ADDRESS.* FROM ADDRESS ");
        freeAddresses.append("INNER JOIN SHOP ON ADDRESS.Shop_id = SHOP.id ");
        freeAddresses.append("INNER JOIN DEPOSIT ON SHOP.Deposit_id = DEPOSIT.id ");
        freeAddresses.append("WHERE ADDRESS.id NOT IN (");
        freeAddresses.append("SELECT Address_id FROM POSSESSES) ");
        freeAddresses.append("AND DEPOSIT.id = ? LIMIT 100");

        try (PreparedStatement ps = conn.prepareStatement(freeAddresses.toString())) {
            ps.setInt(1, client.getDeposit().getId());
            try (ResultSet result = ps.executeQuery()) {
                ArrayList<Address> addressList = new ArrayList<>();
                while (result.next()) {
                    addressList.add(AddressFactory.getAddress(result));
                }
                response = new Request("GELA", addressList);
            }
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
    }
}
