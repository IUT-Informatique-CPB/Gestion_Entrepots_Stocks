package com.gstock.server;

import com.gstock.tools.Request;
import com.gstock.tools.Tool;
import com.gstock.tools.address.Address;
import com.gstock.tools.address.AddressFactory;
import com.gstock.tools.article.Article;
import com.gstock.tools.article.ArticleCategory;
import com.gstock.tools.order.Order;
import com.gstock.tools.order.OrderFactory;
import com.gstock.tools.user.ServerUser;
import javafx.util.Pair;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;

public class ServerResponseSet extends ServerResponse {

    /**
     * Create a new ServerResponse that will treat the request of the client.
     *
     * @param requestThread To notify it that the thread has finished its job
     * @param client        The client that issued the request
     * @param clientRequest The request issued by the client
     */
    public ServerResponseSet(ServerRequest requestThread, ServerUser client, Request clientRequest) {
        super(requestThread, client, clientRequest);
    }

    /**
     * Select the correct method to execute for this specific request of a client
     */
    @Override
    public void execRequest(Connection conn) {
        switch (clientRequest.getParameters()) {
            case "SA":
                rSA(conn);
                break;
            case "SCA":
                rSCA(conn);
                break;
            case "SAMB":
                rSAM(conn, true);
                break;
            case "SAME":
                rSAM(conn, false);
                break;
            case "SASB":
                rSAS(conn, true);
                break;
            case "SASE":
                rSAS(conn, false);
                break;
            case "SCC":
                rSCC(conn);
                break;
            case "SMO":
                rSMO(conn);
                break;
            case "SCMB":
                rSCM(true);
                break;
            case "SCME":
                rSCM(false);
                break;
            case "SCMS":
                rSCMS(conn);
                break;
            case "SCPB":
                rSCP(conn, true);
                break;
            case "SCPE":
                rSCP(conn, false);
                break;
            default:
                break;
        }
    }

    private void rSMO(Connection conn) {

        Boolean isOk = false;
        StringBuilder updateQuantity = new StringBuilder();
        updateQuantity.append("UPDATE CONTAINS ");
        updateQuantity.append("SET Articles_Number = ? ");
        updateQuantity.append("WHERE Article_id = ? ");
        updateQuantity.append("AND Order_id = ?");

        StringBuilder addArticle = new StringBuilder();
        addArticle.append("INSERT INTO CONTAINS(Order_id,Article_id,Articles_Number) ");
        addArticle.append("VALUES(?,?,?)");

        StringBuilder removeArticle = new StringBuilder();
        removeArticle.append("DELETE FROM CONTAINS ");
        removeArticle.append("WHERE Article_id = ? ");
        removeArticle.append("AND Order_id = ?");

        Order newOrder = (Order) clientRequest.getData();
        Order oldOrder = OrderFactory.getOrder(newOrder.getId());
        if (oldOrder != null) { //If Null it's a new Order to create from scratch
            for (Map.Entry<Integer, Integer> e : newOrder.getArticles().entrySet()) {
                if (oldOrder.getArticles().containsKey(e.getKey())) {
                    if (oldOrder.getArticles().get(e.getKey()) != e.getValue()) {
                        //TODO MAJ la quantite pour cette Article dans l'order
                        try (PreparedStatement ps1 = conn.prepareStatement(updateQuantity.toString())) {
                            ps1.setInt(1, e.getValue());
                            ps1.setInt(2, e.getKey());
                            ps1.setInt(3, newOrder.getId());
                            if (ps1.executeUpdate() == 1)
                                isOk = true;
                        } catch (SQLException error) {
                            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", error);
                        }
                    }
                } else {
                    //TODO Rajouter l'article dans la commande
                    try (PreparedStatement ps1 = conn.prepareStatement(addArticle.toString())) {
                        ps1.setInt(1, newOrder.getId());
                        ps1.setInt(2, e.getKey());
                        ps1.setInt(3, e.getValue());
                        if (ps1.executeUpdate() == 1)
                            isOk = true;
                    } catch (SQLException error) {
                        logger.log(Level.SEVERE, "Couldn't Execute the SQL request", error);
                    }
                }
            }
            for (Map.Entry<Integer, Integer> e : oldOrder.getArticles().entrySet()) {
                if (!newOrder.getArticles().containsKey(e.getKey())) {
                    //TODO Remove The Article from the Order
                    try (PreparedStatement ps1 = conn.prepareStatement(removeArticle.toString())) {
                        ps1.setInt(1, e.getKey());
                        ps1.setInt(2, newOrder.getId());
                        if (ps1.executeUpdate() == 1)
                            isOk = true;
                    } catch (SQLException error) {
                        logger.log(Level.SEVERE, "Couldn't Execute the SQL request", error);
                    }
                }
            }
        }
        response = new Request("SMO", isOk);
    }

    /**
     * Request to clear the address of an order
     *
     * @param conn Connexion to the database
     */
    private void rSCA(Connection conn) {
        Boolean isOk = false;
        StringBuilder getArticlesNb = new StringBuilder();
        getArticlesNb.append("SELECT ARTICLE.id, POSSESSES.Articles_Number ")
                .append("FROM ARTICLE_CATEGORY ")
                .append("INNER JOIN ARTICLE ON ARTICLE.ArticleCategory_id = ARTICLE_CATEGORY.id ")
                .append("INNER JOIN POSSESSES ON ARTICLE.id = POSSESSES.Article_id ")
                .append("INNER JOIN ADDRESS ON ADDRESS.id = POSSESSES.Address_id ")
                .append("WHERE ADDRESS.id = ? ")
                .append("AND ARTICLE_CATEGORY.id = ? ");

        StringBuilder deletePossesses = new StringBuilder();
        deletePossesses.append("DELETE FROM POSSESSES ");
        deletePossesses.append("WHERE Address_id = ? ");
        deletePossesses.append("AND Article_id = ?");

        StringBuilder updatePossesses = new StringBuilder();
        updatePossesses.append("UPDATE POSSESSES ");
        updatePossesses.append("SET Articles_Number = ? ");
        updatePossesses.append("WHERE Address_id = ? ");
        updatePossesses.append("AND Article_id = ?");

        HashMap<Integer, Pair<ArticleCategory, Integer>> order = (HashMap) clientRequest.getData();

        for (Map.Entry<Integer, Pair<ArticleCategory, Integer>> e : order.entrySet()) {
            int nbToDelete = e.getValue().getValue();
            TreeMap<Integer, Integer> articlesInAddress = new TreeMap<>();
            try (PreparedStatement ps = conn.prepareStatement(getArticlesNb.toString())) {
                ps.setInt(1, e.getKey());
                ps.setInt(2, e.getValue().getKey().getId());

                try (ResultSet result = ps.executeQuery()) {
                    while (result.next()) {
                        articlesInAddress.put(result.getInt(1), result.getInt(2));
                    }
                }

                for (Map.Entry<Integer, Integer> article : articlesInAddress.entrySet()) {
                    int tmp = Math.min(nbToDelete, article.getValue());
                    nbToDelete -= tmp;

                    Integer addressID = e.getKey();
                    Integer articleID = article.getKey();

                    if (tmp == article.getKey()) {
                        //TODO Supprimer la relation POSSESSES entre addressID et articleID
                        try (PreparedStatement ps1 = conn.prepareStatement(deletePossesses.toString())) {
                            ps1.setInt(1, addressID);
                            ps1.setInt(2, articleID);
                            if (ps1.executeUpdate() == 1)
                                isOk = true;
                        }
                    } else {
                        //TODO Enlever "tmp" article dans la relation POSSESSES entre addressID et articleID
                        try (PreparedStatement ps1 = conn.prepareStatement(updatePossesses.toString())) {
                            ps1.setInt(1,article.getValue()-tmp);
                            ps1.setInt(2, addressID);
                            ps1.setInt(3, articleID);
                            if (ps1.executeUpdate() == 1)
                                isOk = true;
                        }
                    }
                }
            } catch (SQLException error) {
                logger.log(Level.SEVERE, "Couldn't Execute the SQL request", error);
            }
        }
        response = new Request("SCA", isOk);
    }

    /**
     * Request to add an article to the database
     *
     * @param conn Connexion to the database
     */
    private void rSA(Connection conn) {
        Integer articleId = -1;
        final StringBuilder addArticleCategory = new StringBuilder();
        addArticleCategory.append("INSERT INTO ARTICLE(Customs_Number,Serial_Number,Expiration_Date, Receipt_Date,ArticleCategory_id) ");
        addArticleCategory.append("VALUES (?,?,?,?,?)");

        final StringBuilder getArticleId = new StringBuilder();
        getArticleId.append("SELECT id FROM ARTICLE ");
        getArticleId.append("WHERE Customs_Number = ? ");
        getArticleId.append("AND Serial_Number = ? ");
        getArticleId.append("AND Expiration_Date = ? ");
        getArticleId.append("AND Receipt_Date = ? ");
        getArticleId.append("AND ArticleCategory_id = ?");

        Article article = (Article) clientRequest.getData();

        try (PreparedStatement ps = conn.prepareStatement(addArticleCategory.toString(), Statement.RETURN_GENERATED_KEYS)) {
            ps.setObject(1, article.getCustomsNumber());
            ps.setObject(2, article.getSerialNumber());
            ps.setString(3, article.getExpirationDate());
            ps.setString(4, article.getReceiptDate());
            ps.setObject(5, article.getArticleCategory().getId());

            ps.executeUpdate();

            try (ResultSet rs = ps.getGeneratedKeys()) {
                if (rs.next())
                    articleId = rs.getInt(1);
            }

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
        response = new Request("SA", articleId);
    }

    /**
     * Request to add an articleCategory to the database
     *
     * @param conn Connexion to the database
     */
    private void rSCC(Connection conn) {
        boolean isSuccessful = false;
        final StringBuilder addArticleCategory = new StringBuilder();
        addArticleCategory.append("INSERT INTO ARTICLE_CATEGORY(Bar_Code, Name,MaxPalette,MaxPaletteUSA,MaxPaletteEurope) ");
        addArticleCategory.append("VALUES (?,?,?,?,?)");

        final StringBuilder getCategoryId = new StringBuilder();
        getCategoryId.append("SELECT id FROM ARTICLE_CATEGORY ");
        getCategoryId.append("WHERE Bar_Code = ?");

        ArticleCategory category = (ArticleCategory) clientRequest.getData();

        try (PreparedStatement ps = conn.prepareStatement(addArticleCategory.toString())) {
            ps.setString(1, category.getBarCode());
            ps.setString(2, category.getName());
            ps.setInt(3, category.getMaxPalette());
            ps.setInt(4, category.getMaxPaletteUSA());
            ps.setInt(5, category.getMaxPaletteEurope());
            isSuccessful = ps.executeUpdate() == 1;
        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
        if (isSuccessful) {
            try (PreparedStatement ps = conn.prepareStatement(getCategoryId.toString())) {
                ps.setString(1, category.getBarCode());

                try (ResultSet result = ps.executeQuery()) {
                    if (result.next())
                        response = new Request("SCC", true);
                }
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
            }

        }
    }

    /**
     * State of adding an article
     *
     * @param conn  Connexion to the database
     * @param state if (true) report that an article in being stocked
     *              (false) report that an article has been stocked
     */
    private void rSAS(Connection conn, boolean state) { // true = SASB, false = SASE
        Boolean isSuccessful = false;
        Boolean exist = false;
        Integer articleId;
        Integer nbArticlesPresent = 0;
        Integer addressId;
        Integer amount;
        ArrayList<Integer> data;

        if (state) {
            articleId = (Integer) clientRequest.getData();
            if (!Tool.getArticlesBeingStocked().contains(articleId)) {
                Tool.getArticlesBeingStocked().add(articleId);
                isSuccessful = true;
            }
            response = new Request("SASB", isSuccessful);
        } else {
            data = (ArrayList) clientRequest.getData();
            articleId = data.get(0);
            addressId = data.get(1);
            amount = data.get(2);

            //if (Tool.getArticlesBeingStocked().contains(articleId)) {
            //Tool.getArticlesBeingStocked().remove(articleId);

            final StringBuilder getIfExist = new StringBuilder();
            getIfExist.append("SELECT Articles_Number FROM POSSESSES ");
            getIfExist.append("WHERE Article_id = ? ");
            getIfExist.append("AND Address_id = ?");

            try (PreparedStatement ps = conn.prepareStatement(getIfExist.toString())) {
                ps.setObject(1, articleId);
                ps.setObject(2, addressId);

                try (ResultSet result = ps.executeQuery()) {
                    if (result.next()) {
                        exist = true;
                        nbArticlesPresent = result.getInt(1);
                    } else {
                        exist = false;
                    }
                }
            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
            }

            if (exist) {
                final StringBuilder updatePossesses = new StringBuilder();
                updatePossesses.append("UPDATE POSSESSES ");
                updatePossesses.append("SET Articles_Number = ? ");
                updatePossesses.append("WHERE Article_id = ? ");
                updatePossesses.append("AND Address_id = ?");

                try (PreparedStatement ps = conn.prepareStatement(updatePossesses.toString())) {
                    ps.setInt(1, nbArticlesPresent + amount);
                    ps.setInt(2, articleId);
                    ps.setInt(3, addressId);

                    isSuccessful = ps.executeUpdate() == 1;
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
                }
            } else {
                final StringBuilder addArticleAddress = new StringBuilder();
                addArticleAddress.append("INSERT INTO POSSESSES(Address_id, Article_id, Articles_Number) ");
                addArticleAddress.append("VALUES (?,?,?)");

                try (PreparedStatement ps = conn.prepareStatement(addArticleAddress.toString())) {
                    ps.setObject(1, addressId);
                    ps.setObject(2, articleId);
                    ps.setObject(3, amount);

                    isSuccessful = ps.executeUpdate() == 1;

                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
                }
            }


            // }
            response = new Request("SASE", isSuccessful);
        }
    }

    /**
     * Change the moving state of an article
     *
     * @param conn  Connexion to the database
     * @param state if (true) report that an article in being moved
     *              (false) report that an article has been moved
     */
    private void rSAM(Connection conn, boolean state) { // true = SAMB, false = SAME
        Boolean isSuccessful = false;
        Boolean exist = false;
        Integer articleId;
        Integer oldAddressId;
        Integer newAddressId;
        Integer quantityToMove;
        Integer nbArticlesPresent = 0;
        Integer oldQuantity = 0;
        ArrayList<Integer> data;

        if (state) {
            articleId = (Integer) clientRequest.getData();
            if (!Tool.getArticlesBeingMoved().contains(articleId)) {
                Tool.getArticlesBeingMoved().add(articleId);
                isSuccessful = true;
            }
            response = new Request("SAMB", isSuccessful);
        } else {
            data = (ArrayList) clientRequest.getData();
            articleId = data.get(0);
            newAddressId = data.get(1);
            oldAddressId = data.get(2);
            quantityToMove = data.get(3);

            //if (Tool.getArticlesBeingMoved().contains(articleId)) {
                //Tool.getArticlesBeingMoved().remove(articleId);

                    final StringBuilder getOldQuantity = new StringBuilder();
                getOldQuantity.append("SELECT Articles_Number FROM POSSESSES ");
                getOldQuantity.append("WHERE Article_id = ? ");
                getOldQuantity.append("AND Address_id = ?");

                try (PreparedStatement ps = conn.prepareStatement(getOldQuantity.toString())) {
                    ps.setObject(1, articleId);
                    ps.setObject(2, oldAddressId);

                    try (ResultSet result = ps.executeQuery()) {
                        if (result.next()) {
                            oldQuantity = result.getInt(1);
                        }
                    }

                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
                }


                final StringBuilder getIfExist = new StringBuilder();
                getIfExist.append("SELECT Articles_Number FROM POSSESSES ");
                getIfExist.append("WHERE Article_id = ? ");
                getIfExist.append("AND Address_id = ?");

                try (PreparedStatement ps = conn.prepareStatement(getIfExist.toString())) {
                    ps.setObject(1, articleId);
                    ps.setObject(2, newAddressId);

                    try (ResultSet result = ps.executeQuery()) {
                        if (result.next()) {
                            exist = true;
                            nbArticlesPresent = result.getInt(1);
                        } else {
                            exist = false;
                        }
                    }
                } catch (SQLException e) {
                    logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
                }

                if (exist) {
                    final StringBuilder updatePossesses = new StringBuilder();
                    updatePossesses.append("UPDATE POSSESSES ");
                    updatePossesses.append("SET Articles_Number = ? ");
                    updatePossesses.append("WHERE Article_id = ? ");
                    updatePossesses.append("AND Address_id = ?");

                    try (PreparedStatement ps = conn.prepareStatement(updatePossesses.toString())) {
                        ps.setInt(1, nbArticlesPresent + quantityToMove);
                        ps.setInt(2, articleId);
                        ps.setInt(3, newAddressId);

                        isSuccessful = ps.executeUpdate() == 1;
                    } catch (SQLException e) {
                        logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
                    }
                } else {
                    final StringBuilder addArticleAddress = new StringBuilder();
                    addArticleAddress.append("INSERT INTO POSSESSES(Address_id,Article_id,Articles_Number) ");
                    addArticleAddress.append("VALUES(?,?,?)");

                    try (PreparedStatement ps = conn.prepareStatement(addArticleAddress.toString())) {
                        ps.setInt(1, newAddressId);
                        ps.setInt(2, articleId);
                        ps.setInt(3, quantityToMove);

                        isSuccessful = ps.executeUpdate() == 1;
                    } catch (SQLException e) {
                        logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
                    }
                }

                if (isSuccessful) {

                    final StringBuilder updateTheOldAdress = new StringBuilder();
                    updateTheOldAdress.append("UPDATE POSSESSES ");
                    updateTheOldAdress.append("SET Articles_Number = ? ");
                    updateTheOldAdress.append("WHERE Address_id = ? AND Article_id = ?");

                    try (PreparedStatement ps = conn.prepareStatement(updateTheOldAdress.toString())) {
                        ps.setInt(1, oldQuantity - quantityToMove);
                        ps.setInt(2, oldAddressId);
                        ps.setInt(3, articleId);

                        isSuccessful = ps.executeUpdate() == 1;

                    } catch (SQLException e) {
                        logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
                    }
                }
            //}
            response = new Request("SAME", isSuccessful);
        }
    }

    /**
     * Change the prepared state of an order
     *
     * @param state if (true) report that an order in being prepared
     *              (false) report that an order has been prepared
     */
    private void rSCP(Connection conn, boolean state) { // true = SCPB, false = SCPE
        Boolean isSuccessful = false;
        Order order = (Order) clientRequest.getData();

        if (state) {
            final StringBuilder setOrderPicker = new StringBuilder();
            setOrderPicker.append("UPDATE ORDERS");
            setOrderPicker.append(" SET ORDERS.Picker_id = ?");
            setOrderPicker.append(" WHERE ORDERS.id = ?");

            try (PreparedStatement ps = conn.prepareStatement(setOrderPicker.toString())) {
                ps.setInt(1, client.getId());
                ps.setInt(2, order.getId());

                isSuccessful = ps.executeUpdate() == 1;

            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
            }

            response = new Request("SCPB", isSuccessful);
        } else {
            final StringBuilder setOrderCompleted = new StringBuilder();
            setOrderCompleted.append("UPDATE ORDERS");
            setOrderCompleted.append(" SET ORDERS.Completed = ?");
            setOrderCompleted.append(" WHERE ORDERS.id = ?");

            try (PreparedStatement ps = conn.prepareStatement(setOrderCompleted.toString())) {
                ps.setBoolean(1, true);
                ps.setInt(2, order.getId());

                isSuccessful = ps.executeUpdate() == 1;

            } catch (SQLException e) {
                logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
            }

            response = new Request("SCPE", isSuccessful);
        }
    }

    /**
     * Change the modified state of an order
     *
     * @param state if (true) report that an order in being modified
     *              (false) report that an order has been modified
     */
    private void rSCM(boolean state) { // true = SCMB, false = SCME

        Boolean isSuccessful = false;
        Order order = (Order) clientRequest.getData();

        if (state) {
            if (!Tool.getOrderBeingEdited().contains(order.getId())) {
                Tool.getOrderBeingEdited().add(order.getId());
                isSuccessful = true;
            }
            response = new Request("rSCM", isSuccessful);
        } else {
            if (Tool.getOrderBeingEdited().contains(order.getId())) {
                Tool.getOrderBeingEdited().remove(order.getId());
                isSuccessful = true;
            }
            response = new Request("SCME", isSuccessful);
        }
    }

    /**
     * Update the Order Sent by the client in the DataBase
     *
     * @param conn Connection to the DataBase
     */
    private void rSCMS(Connection conn) { //SCMS

        final StringBuilder updateOrder = new StringBuilder();
        updateOrder.append("UPDATE ORDERS");
        updateOrder.append(" SET ORDERS.Bar_Code = ?,");
        updateOrder.append(" ORDERS.Date = ?,");
        updateOrder.append(" ORDERS.Picker_id = ?,");
        updateOrder.append(" ORDERS.Operator_id = ?,");
        updateOrder.append(" ORDERS.Provider_id = ?,");
        updateOrder.append(" ORDERS.Customer_id = ?,");
        updateOrder.append(" ORDERS.Completed = ?");
        updateOrder.append(" WHERE ORDERS.id = ?");

        Order order = (Order) clientRequest.getData();

        try (PreparedStatement ps = conn.prepareStatement(updateOrder.toString())) {
            ps.setString(1, order.getBarCode());
            ps.setString(2, order.getDate());
            ps.setObject(3, order.getPickerId());
            ps.setObject(4, order.getOperatorId());
            ps.setObject(5, order.getProviderId());
            ps.setObject(6, order.getCustomerId());
            ps.setBoolean(7, order.getCompleted());
            ps.setInt(8, order.getId());

            Boolean isSuccessful = ps.executeUpdate() == 1;

            response = new Request("SCMS", isSuccessful);

        } catch (SQLException e) {
            logger.log(Level.SEVERE, "Couldn't Execute the SQL request", e);
        }
    }
}
