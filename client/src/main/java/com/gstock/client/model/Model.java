package com.gstock.client.model;

import com.gstock.tools.Request;
import com.gstock.tools.Tool;
import com.gstock.tools.user.User;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Model {

    private static Socket socket;

    private static User currentUser;

    private static Logger LOGGER = Logger.getLogger( Model.class.getName() );

    /**
     * Disconnect the user from the client
     */
    public static void disconnectUser(){
        Request t;
        t = new Request("ULO",null);
        Socket socket = Model.getSocket();
        try {
            // request to the server to disconnect
            ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
            outputStream.writeObject(t);
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, Tool.getStackTrace(e));
        }
    }

    /**
     * getSocket()
     * @return The socket
     */
    public static Socket getSocket() {
        return socket;
    }

    /**
     * getCurrentUser
     * @return the current user
     */
    public static User getCurrentUser() {
        return currentUser;
    }

    /**
     * Set the socket
     * @param socket
     */
    public static void setSocket(Socket socket) {
        Model.socket = socket;
    }

    /**
     * Set the current user
     * @param currentUser
     */
    public static void setCurrentUser(User currentUser) {
        Model.currentUser = currentUser;
    }
}
