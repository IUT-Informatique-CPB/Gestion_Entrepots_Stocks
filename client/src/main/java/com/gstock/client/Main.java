package com.gstock.client;

import com.gstock.client.controller.ConnectionController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

/**
 * Main is the class that initialize the window and load the default menu.
 * It extends Application in order to use the JavaFX library
 */
public class Main extends Application {

    /**
     * scene is a JavaFX Scene component
     */
    public static Scene scene;

    /**
     * Method used to load the first menu of the application : Connection
     *
     * @see ConnectionController
     */
    void initMenus() {
        java.net.URL a = getClass().getResource(".");
        FXMLLoader fxmlLoader1 = new FXMLLoader(getClass().getResource("./controller/interfaces/Connexion.fxml"));
        fxmlLoader1.setController( new ConnectionController() );
        try {
            scene.setRoot(fxmlLoader1.load());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

	/**
     * Overrive method of the Application class used to create the window
     *
     * @param primaryStage Main stage of the application
     */
    @Override
    public void start(Stage primaryStage) {
        //stage is a JavaFX Stage component
        // group is a JavaFX Group component
        Group group = new Group();
        scene = new Scene(group, 1280, 720);
        initMenus();
        primaryStage.setTitle("GStock");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

	 /**
     * Main method that launch the application
     *
     * @param args User's arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
}
