package com.gstock.client.controller;

import com.gstock.client.Main;
import com.gstock.client.model.Model;
import com.gstock.tools.Request;
import com.gstock.tools.Tool;
import com.gstock.tools.address.Address;
import com.gstock.tools.address.AddressWithQuantity;
import com.gstock.tools.article.Article;
import com.gstock.tools.article.ArticleCategory;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * OperatorMovement2Controller is the class that handles all events in the second movement menu.
 * It implements Initializable in order to use FXML components.
 */
public class OperatorMovement2Controller implements Initializable {

    /**
     * confirmationPane is a Java FX Pane component.
     * It is used to notice the user he can't leave the current menu until he finished.
     */
    @FXML
    private Pane confirmationPane;

    /**
     * confirmationPane2 is a Java FX Pane component.
     * It is used to get the confirmation from the user that he moved the articles in the selected addresses.
     */
    @FXML
    private Pane confirmationPane2;

    /**
     * shadowPane is a Java FX Pane component.
     * It is used to add a "shadow" effect behind the confirmation pane.
     */
    @FXML
    private Pane shadowPane;


    /**
     * confirmButton is a Java FX Button component.
     * It is used by the user to commit confirm what he did on this menu.
     */
    @FXML
    private Button confirmButton;

    /**
     * operatorMenuButton is a Java FX Button component.
     * The operatorMenuButton is here only a part of the interface that won't do anything.
     * It's only here so that the user isn't lost when he uses the application.
     */
    @FXML
    private Button operatorMenuButton;

    /**
     * disconnectButton is a Java FX Button component.
     * Same as operatorMenuButton.
     */
    @FXML
    private Button disconnectButton;

    /**
     * okButton is a Java FX Button component.
     * It is used by the user to confirm he understood what the confirmationPane said.
     */
    @FXML
    private Button okButton;

    /**
     * acceptButton is a Java FX Button component.
     * It is used by the user to confirm that he moved the articles.
     */
    @FXML
    private Button acceptButton;

    /**
     * refuseButton is a Java FX Button component.
     * It is used by the user to cancel the confirmation.
     */
    @FXML
    private Button refuseButton;


    /**
     * warningLabel is a Java FX Label component.
     * It is used in the two confirmations panes to print a different message.
     */
    @FXML
    private Label warningLabel;

    /**
     * loginLabel is a Java FX Button component.
     * It is used to print the current user's login.
     */
    @FXML
    private Label loginLabel;

    /**
     * warehouseLabel is a Java FX Button component.
     * It is used to print the current warehouse.
     */
    @FXML
    private Label warehouseLabel;

    /**
     * articleNameLabel is a Java FX Button component.
     * It is used to print the current article name.
     */
    @FXML
    private Label articleNameLabel;

    /**
     * addressLabel is JavaFX Label component.
     * addressLabel is used to print the selected address from the previous menu.
     */
    @FXML
    private Label addressLabel;


    /**
     * article is a Article.
     * article is the article we chose to move on the previous menu.
     */
    private ArticleCategory articleCategory;

    /**
     * article is a Article.
     * article is the article we chose to move on the previous menu.
     */
    private Article selectedArticle;


    /**
     * addressLabel is JavaFX Label component.
     * selectedAddress is the address we chose where we move the article on the previous menu.
     */
    private Address selectedAddress;


    /**
     * availableAdressesListView is a Java FX ListView Component.
     * It is used to show all the available addresses where we can move the articles.
     */
    @FXML
    private ListView<AddressWithQuantity> availableAdressesListView;

    private TreeMap<Article, Integer> articleQuantityToStore = new TreeMap<>();


    /**
     * scene is a JavaFX Scene component.
     * It is used to add components to the current scene.
     */
    private final Scene scene;

    private Logger LOGGER = Logger.getLogger(this.getClass().getName());

    /**
     * Default constructor of the OperatorMovement2Controller class.
     *
     * @see OperatorMovement2Controller
     */
    OperatorMovement2Controller(ArticleCategory articleCategory, Address selectedAddress) {
        this.scene = Main.scene;
        this.articleCategory = articleCategory;
        this.selectedAddress = selectedAddress;
    }

    /**
     * Method used to initialize all actions and events that can be triggered
     * when the user do an action.
     *
     * @param location  Unused parameter
     * @param resources Unused parameter
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        loginLabel.setText(Model.getCurrentUser().getUsername());
        warehouseLabel.setText(Model.getCurrentUser().getDeposit().getName());

        Platform.runLater(() -> {
            articleNameLabel.setText("Nom Article : " + articleCategory.getName());
            addressLabel.setText("Adresse à remplir : " + selectedAddress);
        });

        /*
          This thread is used to fill the availableAdressesListView.
          We ask the "GAREL" request to get all the address that correspond to the selected article category
          @see Request
         */
        Thread thread = new Thread(() -> {

            Integer[] idPair = {articleCategory.getId(), selectedAddress.getId()};
            Request t = new Request("GAREL", idPair);
            Socket socket = Model.getSocket();

            try {
                ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                outputStream.writeObject(t);

                ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                Request response = (Request) inputStream.readObject();

                TreeMap<Address, Integer> addresses = ((TreeMap<Address, Integer>) response.getData());

                if (addresses != null) {
                    Platform.runLater(() -> {
                        availableAdressesListView.getItems().clear();
                        for (Map.Entry<Address, Integer> a : addresses.entrySet()) {
                            AddressWithQuantity awq = new AddressWithQuantity(a.getKey(),a.getValue());
                            availableAdressesListView.getItems().add(awq);
                        }
                    });
                }
            } catch (IOException | ClassNotFoundException e) {
                LOGGER.log(Level.SEVERE, "Error using the request \"GAREL\" request in the operator movement 2 menu", e);
            }
        });
        thread.start();

        /*
          We put a trigger event when the user click on the button
          to hide the confirmation and shadow panes.
         */
        confirmButton.setOnMouseClicked(event -> {
            if (availableAdressesListView.getSelectionModel().getSelectedItem() == null) {
                warningLabel.setText("Vous devez sélectionner une adresse avant de continuer");
                shadowPane.setVisible(true);
                confirmationPane.setVisible(true);
            } else {
                shadowPane.setVisible(true);
                confirmationPane2.setVisible(true);
            }
        });

        /*
          We put a trigger event when the user click on the button
          to set a text on the label and show the confirmation and shadow panes.
         */
        operatorMenuButton.setOnMouseClicked(event -> {
            warningLabel.setText("Vous ne pouvez pas revenir au menu cariste avant d'avoir fini");
            shadowPane.setVisible(true);
            confirmationPane.setVisible(true);
        });

        /*
          We put a trigger event when the user click on the button
          to set the confirmation and shadow Panes visible.
         */
        disconnectButton.setOnMouseClicked(event -> {
            warningLabel.setText("Vous ne pouvez pas vous déconnecter avant d'avoir fini");
            shadowPane.setVisible(true);
            confirmationPane.setVisible(true);
        });

        /*
          We put a trigger event when the user click on the button
          to hide the shadow and confirmation panes.
         */
        okButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane.setVisible(false);
        });

        /*
          We put a trigger event when the user click on the button
          to move to the operator menu.
         */
        acceptButton.setOnMouseClicked(event -> {

            /*
                We are using this request to get the article and quantity
            */
            Request t = new Request("GE", selectedAddress.getId());
            Socket socket = Model.getSocket();

            try {
                ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                outputStream.writeObject(t);

                ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                Request response = (Request) inputStream.readObject();

                Map<Article, Integer> articleIntegerMap = (Map<Article, Integer>) response.getData();

                    for (Map.Entry<Article, Integer> article : articleIntegerMap.entrySet()) {
                        if (article.getKey().getArticleCategory().getId() == articleCategory.getId()) {
                            selectedArticle = article.getKey();
                            articleQuantityToStore.put(article.getKey(), article.getValue());
                        }
                    }
            } catch (IOException | ClassNotFoundException e) {
                LOGGER.log(Level.SEVERE, "Error using the request \"GE\" request in the operator movement 1 menu", e);
            }

            // We prepare the parameters for the "SAME" request
            ArrayList<Integer> idResponse = new ArrayList<>();
                        idResponse.add(selectedArticle.getId());
                        idResponse.add(selectedAddress.getId());
                        idResponse.add(availableAdressesListView.getSelectionModel().getSelectedItem().getAddress().getId());
                        idResponse.add(availableAdressesListView.getSelectionModel().getSelectedItem().getQuantity());

            Request same = new Request("SAME", idResponse);

            try {
                ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                outputStream.writeObject(same);

                ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                inputStream.readObject();

                Platform.runLater(() -> {

                });
            } catch (IOException | ClassNotFoundException e) {
                LOGGER.log(Level.SEVERE, "Error using the request \"SAME\" request in the operator movement 1 menu", e);
            }

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interfaces/Cariste/Cariste.fxml"));
            fxmlLoader.setController(new OperatorController());
            try {
                scene.setRoot(fxmlLoader.load());
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "Error while moving from the operator movement 2 menu to the operator menu", e);
            }
        });

        /*
          We put a trigger event when the user click on the button
          to hide the shadow and second confirmation panes.
         */
        refuseButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane2.setVisible(false);
        });

        /*
          We put a trigger event when the user click outside the confirmationPane
          to hide the shadow and confirmation panes.
         */
        shadowPane.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane.setVisible(false);
            confirmationPane2.setVisible(false);
        });
    }
}
