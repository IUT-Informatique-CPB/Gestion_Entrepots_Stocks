package com.gstock.client.controller;

import com.gstock.client.Main;
import com.gstock.client.model.Model;
import com.gstock.tools.Request;
import com.gstock.tools.Tool;
import com.gstock.tools.address.Address;
import com.gstock.tools.article.Article;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * OperatorStorage2Controller is the class that handles all events in the second storage menu.
 * It implements Initializable in order to use FXML components.
 */
public class OperatorStorage2Controller implements Initializable {

    /**
     * confirmationPane is a Java FX Pane component.
     * It is used to get a confirmation from the user that he physically stored the article.
     */
    @FXML
    private Pane confirmationPane;

    /**
     * confirmationPane2 is a Java FX Pane component.
     * It is used to notice the user he can't leave the current menu until he finished.
     */
    @FXML
    private Pane confirmationPane2;

    /**
     * addressChoicePane is a Java FX Pane component.
     * It is used to select an address to store the article.
     */
    @FXML
    private Pane addressChoicePane;

    /**
     * shadowPane is a Java FX Pane component.
     * It is used to add a "shadow" effect behind the confirmation pane.
     */
    @FXML
    private Pane shadowPane;



    /**
     * confirmStorageButton is a Java FX Button component.
     * It is used to confirm the storage of the articles.
     */
    @FXML
    private Button confirmStorageButton;

    /**
     * operatorMenuButton is a Java FX Button component.
     * The operatorMenuButton is here only a part of the interface that won't do anything.
     * It's only here so that the user isn't lost when he uses the application.
     */
    @FXML
    private Button operatorMenuButton;

    /**
     * disconnectButton is a Java FX Button component.
     * Same as operatorMenuButton.
     */
    @FXML
    private Button disconnectButton;

    /**
     * okButton is a Java FX Button component.
     * It is used by the user to confirm he understood what the confirmationPane said.
     */
    @FXML
    private Button okButton;

    /**
     * selectButton is a Java FX Button component.
     * It is used to select the address the user chose.
     */
    @FXML
    private Button selectButton;

    /**
     * acceptButton is a Java FX Button component.
     * It is used by the user to confirm that he stored the article in the address he chose.
     */
    @FXML
    private Button acceptButton;

    /**
     * refuseButton is a Java FX Button component.
     * It is used by the user to cancel the confirmation.
     */
    @FXML
    private Button refuseButton;

    /**
     * tempAddress is a Java FX Button component.
     * It is used to store the button so we can change the text displayed on it.
     */
    private Button tempButton;



    /**
     * loginLabel is a Java FX Button component.
     * It is used to print the current user's login.
     */
    @FXML
    private Label loginLabel;

    /**
     * warehouseLabel is a Java FX Button component.
     * It is used to print the current warehouse.
     */
    @FXML
    private Label warehouseLabel;

    /**
     * warningLabel is a Java FX Label component.
     * It is used to set a different message depending on the confirmation.
     */
    @FXML
    private Label warningLabel;

    /**
     * articleNameLabel is a Java FX Label component
     * It is used to print the name of the article we want to store
     */
    @FXML
    private Label articleNameLabel;

    /**
     * quantityLabel is a JavaFX Label component
     * It is used to print the quantity of the current article that we need to store
     */
    @FXML
    private Label quantityLabel;


    /**
     * addBox is a Java FX VBox component.
     * It is used by the user to add or remove a line of address where the user want to store.
     */
    @FXML
    private VBox addBox;

    /**
     * quantityBox is a Java FX VBox component.
     * It is used by the user to set a quantity of article to store.
     */
    @FXML
    private VBox quantityBox;

    /**
     * quantityBox is a Java FX VBox component.
     * It is used by the user to set an address of storage.
     */
    @FXML
    private VBox addressBox;



    /**
     * typeComboBox is a Java FX MenuButton component.
     * It is used to select the type of address we want.
     */
    @FXML
    private MenuButton typeComboBox;



    /**
     * pickingType is a Java FX MenuItem component.
     * It correspond to the picking address selection.
     */
    @FXML
    private MenuItem pickingType;

    /**
     * reserveType is a Java FX MenuItem component.
     * It correspond to the storage address selection.
     */
    @FXML
    private MenuItem reserveType;


    /**
     * addButtonTab is a vector of Hbox.
     * It contains all the Hbox that contains add and remove buttons.
     */
    private Vector<HBox> addButtonTab = new Vector<>();

    /**
     * quantityFieldTab is a vector of TextField.
     * It contains all the text fields of article we want to store.
     */
    private Vector<TextField> quantityFieldTab = new Vector<>();

    /**
     * addressTab is a vector of Button.
     * It contains all the Buttons used to choose an address.
     */
    private Vector<Button> addressTab = new Vector<>();



    /**
     * article is a Article.
     * article is the article we chose on the previous menu.
     */
    private Article article;



    /**
     * addressListView is a Java FX ListView component.
     * It is used to show all the addresses corresponding to the current type of address selected.
     */
    @FXML private ListView<Address> addressListView;



    /**
     * tempAddress is an Address.
     * It is used to store the address the user selected so we can put it on the button.
     */
    private Address tempAddress;



    /**
     * articleQuantity is a String.
     * It is used to store the current article quantity to store (maximum).
     */
    private String articleQuantity;



    /**
     * requestDone is a boolean.
     * It is used to mark when all the requests of
     * the page are done
     */
    private boolean requestDone = true;



    /**
     * scene is a JavaFX Scene component.
     * It is used to add components to the current scene.
     */
    private final Scene scene;

    private HashMap<Integer,Address> currentAddresses = new HashMap<>();

    private Logger LOGGER = Logger.getLogger(this.getClass().getName());
    private boolean articleIsNew;

    /**
     * Default constructor of the OperatorStorage2Controller class.
     *
     * @see OperatorStorage2Controller
     */
    OperatorStorage2Controller(Article article, String articleQuantity, boolean articleIsNew) {
        this.articleIsNew = articleIsNew;
        this.scene = Main.scene;
        this.article = article;
        this.articleQuantity = articleQuantity;
    }

    /**
     * Method used to initialize all actions and events that can be triggered
     * when the user do an action.
     *
     * @param location  Unused parameter
     * @param resources Unused parameter
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        loginLabel.setText(Model.getCurrentUser().getUsername());
        warehouseLabel.setText(Model.getCurrentUser().getDeposit().getName());

        addLine();
        // We set the typeComboBox to the default value : picking address
        typeComboBox.setText(pickingType.getText());

        Platform.runLater(() -> articleNameLabel.setText("Nom Article : " + article.getArticleCategory().getName()));
        Platform.runLater(() -> quantityLabel.setText("Quantité : " + articleQuantity));

        /*
          We put a trigger event when the user click on the button
          to show the confirmation and shadow panes.
         */
        confirmStorageButton.setOnMouseClicked(event -> {
            int total = 0;
            for(TextField textField : quantityFieldTab) {
                if(textField.getText().equals(""))
                    textField.setText("0");
                total += Integer.parseInt(textField.getText());
            }
            shadowPane.setVisible(true);
            if(String.valueOf(total).equals(quantityLabel.getText().substring(11))) {
                boolean allSelected = true;
                for(Button button : addressTab) {
                    if(button.getText().startsWith("S"))
                        allSelected = false;
                }
                if(allSelected)
                    confirmationPane.setVisible(true);
                else {
                    warningLabel.setText("Vous devez sélectionner un adresse pour chaque entrée");
                    confirmationPane2.setVisible(true);
                }
            }
            else {
                warningLabel.setText("La somme des quantités entrées doit être égale à la quantité actuelle");
                confirmationPane2.setVisible(true);
            }
        });

        /*
          We put a trigger event when the user click on the button
          to show the shadow and second confirmation panes.
         */
        operatorMenuButton.setOnMouseClicked(event -> {
            warningLabel.setText("Vous ne pouvez pas revenir au menu cariste avant d'avoir fini");
            shadowPane.setVisible(true);
            confirmationPane2.setVisible(true);
        });

        /*
          We put a trigger event when the user click on the button
          to set the confirmation and shadow Panes visible and we also set the text to the correct confirmation.
         */
        disconnectButton.setOnMouseClicked(event -> {
            warningLabel.setText("Vous ne pouvez pas vous déconnecter avant d'avoir fini");
            shadowPane.setVisible(true);
            confirmationPane2.setVisible(true);
        });

        /*
          We put a trigger event when the user click on the button
          to hide the shadow and second confirmation panes.
         */
        okButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane2.setVisible(false);
        });

        /*
          We put a trigger event when the user click on the button
          to hide the shadow and adressChoice panes.
         */
        selectButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            addressChoicePane.setVisible(false);
            if (addressListView.getSelectionModel().getSelectedItem() != null) {
                tempAddress = addressListView.getSelectionModel().getSelectedItem();
                tempButton.setText(tempAddress.toString());
                currentAddresses.put(addressTab.indexOf(tempButton),tempAddress);
            }
        });

        /*
          We put a trigger event when the user click on the button
          to move to the operator menu.
         */
        acceptButton.setOnMouseClicked(event -> {
            if(articleIsNew) {
                Request t = new Request("SCC", article.getArticleCategory());
                Socket socket = Model.getSocket();

                try {
                    ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                    outputStream.writeObject(t);

                    ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                    Request response = (Request) inputStream.readObject();

                    boolean success = (boolean) response.getData();

                    if(!success)
                        return;
                } catch (IOException | ClassNotFoundException e) {
                    LOGGER.log(Level.SEVERE, "Error using the request \"SCC\" request in the operator storage 2 menu", e);
                }
            }

            Integer articleId = -2;

            Request t = new Request("SA", article);
            Socket socket = Model.getSocket();

            try {
                ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                outputStream.writeObject(t);

                ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                Request response = (Request) inputStream.readObject();

                articleId = (Integer) response.getData();

                if (articleId==null)
                    return;
            } catch (IOException | ClassNotFoundException e) {
                LOGGER.log(Level.SEVERE, "Error using the request \"SA\" request in the operator storage 2 menu", e);
            }

            for(int i = 0; i < addressTab.size() ; ++i) {
                ArrayList<Integer> toSend = new ArrayList<>();
                toSend.add(articleId);
                toSend.add(currentAddresses.get(i).getId());
                toSend.add(Integer.parseInt(quantityFieldTab.get(i).getText()));

                Request r1 = new Request("SASE", toSend);
                Socket socket1 = Model.getSocket();

                try {
                    ObjectOutputStream outputStream = new ObjectOutputStream(socket1.getOutputStream());
                    outputStream.writeObject(r1);

                    ObjectInputStream inputStream = new ObjectInputStream(socket1.getInputStream());
                    Request response = (Request) inputStream.readObject();

                    boolean success = (boolean) response.getData();

                    if (!success)
                        return;
                } catch (IOException | ClassNotFoundException e) {
                    LOGGER.log(Level.SEVERE, "Error using the request \"SASE\" request in the operator storage 2 menu", e);
                }
            }
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interfaces/Cariste/Cariste.fxml"));
            fxmlLoader.setController(new OperatorController());
            try {
                scene.setRoot(fxmlLoader.load());
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "Error while moving from the operator storage 2 menu to the operator menu", e);
            }


        });

        /*
          We put a trigger event when the user click on the button
          to hide the shadow and confirmation panes.
         */
        refuseButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane.setVisible(false);
        });

        /*
          We put a trigger event when the user click outside the confirmationPane
          to hide the shadow and confirmation panes and we also put the default value on the comboBox.
         */
        shadowPane.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane2.setVisible(false);
            confirmationPane.setVisible(false);
            addressChoicePane.setVisible(false);
            typeComboBox.setText(pickingType.getText());
        });

        /*
          We put a trigger event when the user click on the picking type
          to refresh the address ListView with the right addresses
         */
        pickingType.setOnAction(event -> {
            if(requestDone && (!typeComboBox.getText().equals(pickingType.getText()) || addressListView.getItems().size() == 0)) {
                typeComboBox.setText(pickingType.getText());
                Thread subThread = new Thread(() -> {
                    requestDone = false;
                    Request t = new Request("GAP", article.getArticleCategory());
                    Socket socket = Model.getSocket();

                    try {
                        ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                        outputStream.writeObject(t);

                        ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                        Request response = (Request) inputStream.readObject();

                        ArrayList<Address> addresses = (ArrayList<Address>) response.getData();

                        if (addresses != null) {
                            Platform.runLater(() -> {
                                addressListView.getItems().clear();
                                for (Address a : addresses) {
                                    addressListView.getItems().add(a);
                                }
                            });
                        }
                    } catch (IOException | ClassNotFoundException e) {
                        LOGGER.log(Level.SEVERE, "Error using the request \"GAP\" request in the operator storage 2 menu", e);
                    }
                    requestDone = true;
                });
                subThread.start();
            }
        });

        /*
          We put a trigger event when the user click on the reserve type
          to refresh the address ListView with the right addresses
         */
        reserveType.setOnAction(event -> {
            if(requestDone && (!typeComboBox.getText().equals(reserveType.getText()) || addressListView.getItems().size() == 0)) {
                typeComboBox.setText(reserveType.getText());
                Thread subThread = new Thread(() -> {
                    requestDone = false;
                    Request t = new Request("GAR", article.getArticleCategory());
                    Socket socket = Model.getSocket();

                    try {
                        ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                        outputStream.writeObject(t);

                        ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                        Request response = (Request) inputStream.readObject();

                        ArrayList<Address> addresses = (ArrayList<Address>) response.getData();

                        if (addresses != null) {
                            Platform.runLater(() -> {
                                addressListView.getItems().clear();
                                for (Address a : addresses) {
                                    addressListView.getItems().add(a);
                                }
                            });
                        }
                    } catch (IOException | ClassNotFoundException e) {
                        LOGGER.log(Level.SEVERE, "Error using the request \"GAR\" request in the operator storage 2 menu", e);
                    }
                    requestDone = true;
                });
                subThread.start();
            }
        });
    }

    /**
     * This function is used to add a new line of address where we can store some of the article selected
     * This is mainly setters of properties on textfields, hbox and buttons.
     */
    private void addLine() {
        Button add = new Button("+");
        add.getStyleClass().add("info");
        add.setOnMouseClicked(event -> addLine());
        add.setTextAlignment(TextAlignment.CENTER);
        Button minus = new Button("-");
        minus.getStyleClass().add("info");
        HBox hBox = new HBox(minus, add);
        hBox.setAlignment(Pos.CENTER);
        hBox.setPrefWidth(113);
        HBox.setMargin(add, new Insets(0, 0, 0, 10));
        VBox.setMargin(hBox, new Insets((addButtonTab.size() == 0) ? 30 : 0, 0, 7, 0));
        addBox.getChildren().add(hBox);
        addButtonTab.add(hBox);
        minus.setOnMouseClicked(event -> removeLine(addButtonTab.indexOf(hBox)));


        TextField textField = new TextField();
        textField.setPromptText("Nombre");
        if (quantityFieldTab.isEmpty())
            textField.setText(articleQuantity);
        else
            textField.setText("0");
        textField.setMinHeight(32);
        textField.setMaxWidth(75);
        Tool.setAsNumericField(textField);
        VBox.setMargin(textField, new Insets((quantityFieldTab.size() == 0) ? 30 : 0, 0, 7, 0));
        quantityBox.getChildren().add(textField);
        quantityFieldTab.add(textField);

        Button address = new Button("Sélectionnez une adresse");
        address.getStyleClass().add("success");
        address.setPrefWidth(330);
        address.setOnMouseClicked(event -> {
            shadowPane.setVisible(true);
            addressChoicePane.setVisible(true);
            pickingType.fire();
            tempButton = address;
        });
        VBox.setMargin(address, new Insets((addressTab.size() == 0) ? 30 : 0, 0, 7, 0));
        addressBox.getChildren().add(address);
        addressTab.add(address);

        if (addButtonTab.size() == 2)
            addButtonTab.get(0).getChildren().get(0).setDisable(false);
        else if (addButtonTab.size() == 1)
            addButtonTab.get(0).getChildren().get(0).setDisable(true);
    }

    /**
     * This function is used to remove a new line of address where we can store some of the article selected
     */
    private void removeLine(int position) {
        if (addButtonTab.size() >= 1) {
            addButtonTab.remove(position);
            quantityFieldTab.remove(position);
            addressTab.remove(position);

            addBox.getChildren().remove(position);
            quantityBox.getChildren().remove(position);
            addressBox.getChildren().remove(position);

            if (position == 0) {
                VBox.setMargin(addButtonTab.get(0), new Insets(30, 0, 7, 0));
                VBox.setMargin(quantityFieldTab.get(0), new Insets(30, 0, 7, 0));
                VBox.setMargin(addressTab.get(0), new Insets(30, 0, 7, 0));
            }
            if (addButtonTab.size() == 1)
                addButtonTab.get(0).getChildren().get(0).setDisable(true);
        }

    }
}

