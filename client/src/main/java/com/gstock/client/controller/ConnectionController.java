package com.gstock.client.controller;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.gstock.client.model.Model;
import com.gstock.tools.Request;
import com.gstock.tools.Tool;
import com.gstock.tools.user.User;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import com.gstock.client.Main;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

/**
 * ConnectionController is the class that handles all events in the connection menu.
 * It implements Initializable in order to use FXML components.
 */
public class ConnectionController implements Initializable {

    /**
     * GridPane is a JavaFX component
     * It is used to ensure so that the user can press enter to connect
     */
    @FXML private GridPane gridPane;



    /**
     * connectionButton is a FXML Button component of the connection interface.
     * It is used to connect the user to the server.
     */
    @FXML private Button connectionButton;
	
	
	
	/**
     * idField is a FXML TextField component of the connection interface.
     * It is used to get the user login
     */
    @FXML private TextField idField;
	
	
	
	/**
     * passwordField is a FXML PasswordField component of the connection interface.
     * It is used to get the user password
     */
    @FXML private PasswordField passwordField;

	
	
    /**
     * scene is a JavaFX Scene component.
     * It is used to add components to the current scene.
     */
    private final Scene scene;



	/**
     * we create a logging allowing for example to keep a record of the exceptions that are raised in the application
     * and the various abnormal or normal events related to the execution of the application.
     */
    private Logger LOGGER = Logger.getLogger( this.getClass().getName() );

	
	/**
     * Default constructor of the ConnectionController class.
     *
     * @see ConnectionController
     */
    public ConnectionController() {
        this.scene = Main.scene ;
    }

    private void connection()  {
        if(!idField.getText().equals("") && !passwordField.getText().equals("")) {

            Thread thread = new Thread(() -> {
                Request t;
                // We get the two parameters for the request
                String[] str = new String[2];
                str[0] = idField.getText();
                str[1] = passwordField.getText();

                t = new Request("ULI", str);
                Socket socket;

                try {
                    socket = new Socket("localHost", 2048);

                    ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                    outputStream.writeObject(t);

                    ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                    Request response = (Request)inputStream.readObject();

                    Socket finalSocket = socket;
                    Platform.runLater(() -> {
                        if(response.getData() != null){
                            Model.setSocket(finalSocket);
                            Model.setCurrentUser((User)response.getData());
                            // We load the main menu
                            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interfaces/MenuPrincipal.fxml"));
                            fxmlLoader.setController(new MainMenuController());
                            try {
                                scene.setRoot(fxmlLoader.load());
                            } catch (IOException e) {
                                LOGGER.log(Level.SEVERE, "Error while connecting to the server", e);
                            }
                        }else{
                            // If the login/password aren't correct we add a red glow to notify the user
                            idField.getStyleClass().add("red");
                            passwordField.getStyleClass().add("red");
                        }
                    });

                } catch (IOException | ClassNotFoundException e) {
                    LOGGER.log(Level.SEVERE, Tool.getStackTrace(e));
                    LOGGER.log(Level.SEVERE, "Error while connecting to the server", e);
                }
            });
            thread.start();
        }
    }

	/**
     * Method used to initialize all actions and events that can be triggered
     * when the user do an action.
     *
     * @param location Unused parameter
     * @param resources Unused parameter
     */
	@Override
    public void initialize(URL location, ResourceBundle resources) {

        idField.getStylesheets().add("com/gstock/client/controller/interfaces/WrongFields.css");
        passwordField.getStylesheets().add("com/gstock/client/controller/interfaces/WrongFields.css");

        /*
          We put a trigger event when the user click on the button to connect.
          to move to the picker menu.
          The we try to connect to the server with the data returned by the user.
         */
        connectionButton.setOnMouseClicked(event -> connection());

        /*
          We put a trigger event when the user press a button.
          to do the same as the connection button.
         */
        gridPane.setOnKeyPressed(event -> connection());


    }
}
