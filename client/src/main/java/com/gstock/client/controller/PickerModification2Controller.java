package com.gstock.client.controller;

import com.gstock.client.Main;
import com.gstock.client.model.Model;
import com.gstock.tools.Request;
import com.gstock.tools.Tool;
import com.gstock.tools.article.ArticleCategory;
import com.gstock.tools.order.Order;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * PickerModification2Controller is the class that handles all events in the second modification menu.
 * It implements Initializable in order to use FXML components.
 */
public class PickerModification2Controller implements Initializable {

    /**
     * confirmationPane is a Java FX Pane component.
     * It is used to get a confirmation from the user that he wants to modify the current order.
     */
    @FXML private Pane confirmationPane;

    /**
     * addArticlePane is a Java FX Pane component.
     * It is used to add an article to the current command.
     */
    @FXML private Pane addArticlePane;

    /**
     * confirmationPane2 is a Java FX Pane component.
     * It is used to notice the user he can't leave the current menu until he finished.
     */
    @FXML private Pane confirmationPane2;

    /**
     * shadowPane is a Java FX Pane component.
     * It is used to add a "shadow" effect behind the confirmation pane.
     */
    @FXML private Pane shadowPane;



    /**
     * saveOrderButton is a Java FX Button component.
     * It is use by the user to confirm he wants to save all modifications on the order.
     */
    @FXML private Button saveOrderButton;

    /**
     * cancelOrderModificationButton is a Java FX Button component.
     * It is use by the user to confirm he wants to cancel all modifications on the order.
     */
    @FXML private Button cancelOrderModificationButton;

    /**
     * pickerMenuButton is a Java FX Button component.
     * The pickerMenuButton is here only a part of the interface that won't do anything.
     * It's only here so that the user isn't lost when he uses the application.
     */
    @FXML private Button pickerMenuButton;

    /**
     * disconnectButton is a Java FX Button component.
     * Same as pickerMenuButton.
     */
    @FXML private Button disconnectButton;

    /**
     * okButton is a Java FX Button component.
     * It is used by the user to confirm he understood what the confirmationPane2 said.
     */
    @FXML private Button okButton;

    /**
     * acceptButton is a Java FX Button component.
     * It is used by the user to confirm that he wants to modify the order (final confirmation).
     */
    @FXML private Button acceptButton;

    /**
     * refuseButton is a Java FX Button component.
     * It is used by the user to cancel the confirmation.
     */
    @FXML private Button refuseButton;

    /**
     * addArticleButton is a Java FX Button component.
     * It is used by the user to add an Article in the command.
     */
    @FXML private Button addArticleButton;

    /**
     * deleteArticleButton is a Java FX Button component.
     * It is used by the user to delete an article in the command.
     */
    @FXML private Button deleteArticleButton;

    /**
     * cancelAddingArticleButton is a Java FX Button component.
     * It is used by the user to cancel the addition.
     */
    @FXML private Button cancelAddingArticleButton;

    /**
     * confirmAddingArticleButton is a Java FX Button component.
     * It is used by the user to confirm the addition.
     */
    @FXML private Button confirmAddingArticleButton;

    /**
     * confirmAddingArticleButton is a Java FX Button component.
     * It is used by the user to confirm the addition.
     */
    @FXML private Button searchButton;



    /**
     * warningLabel is a Java FX Label component.
     * It is used to set a different message depending on the confirmation.
     */
    @FXML private Label warningLabel;

    /**
     * warningLabel2 is a Java FX Label component.
     * It is used to set a different message depending on the confirmation.
     */
    @FXML private Label warningLabel2;

    /**
     * loginLabel is a Java FX Label component.
     * It is used to print the current user's login.
     */
    @FXML private Label loginLabel;

    /**
     * warehouseLabel is a Java FX Label component.
     * It is used to print the current warehouse.
     */
    @FXML private Label warehouseLabel;



    /**
     * articles is a variable used to store the list
     * of articles gathered from the server
     * to avoid making another request
     */
     private Set<ArticleCategory> articles;



    /**
     * order is the current selected order to process.
     */
    private Order order;



    /**
     * articleListView is a JavaFX Scene component.
     * It is used to show the articles of the selected order.
     */
    @FXML private ListView<ArticleCategory> articleListView;

    /**
     * articlesToAddListView is a Java FX ListView component.
     * It is used by the user to show the searched articles.
     */
    @FXML private ListView articlesToAddListView;



    /**
     * quantityTextField is a JavaFX TextField component.
     * It is used to show the articles of the selected order.
     */
    @FXML private TextField quantityTextField;

    /**
     * articleNameTextField is a JavaFX TextField component.
     * It is used to show the articles of the selected order.
     */
    @FXML private TextField articleNameTextField;

    /**
     * searchTextField is a Java FX TextField component.
     * It is used by the user to search for an article in the database.
     */
    @FXML private TextField searchTextField;

    /**
     * quantityArticleToAddTextField is a Java FX TextField component.
     * It is used by the user to enter the quantity of the needed article.
     */
    @FXML private TextField quantityArticleToAddTextField;

    /**
     * maxNumberPaletteTextField is a Java FX TextField component.
     * It is used to display the maximum number of the selected article we can put in a palette.
     */
    @FXML private TextField maxNumberPaletteTextField;

    /**
     * maxNumberPaletteEuropeTextField is a Java FX TextField component.
     * It is used to display the maximum number of the selected article we can put in a Europe palette.
     */
    @FXML private TextField maxNumberPaletteEuropeTextField;

    /**
     * maxNumberPaletteUSATextField is a Java FX TextField component.
     * It is used to display the maximum number of the selected article we can put in a USA palette.
     */
    @FXML private TextField maxNumberPaletteUSATextField;



    /**
     * requestDone is a boolean.
     * It is used to mark when all the requests of
     * the page are done
     */
    private boolean requestDone = true;

    private boolean cancelling = false;

    /**
     * scene is a JavaFX Scene component.
     * It is used to add components to the current scene.
     */
    private final Scene scene;

    private Logger LOGGER = Logger.getLogger( this.getClass().getName() );

    /**
     * Default constructor of the PickerModification2Controller class.
     *
     * @see PickerModification2Controller
     */
    PickerModification2Controller(Order order) {
        this.order = order;
        this.scene = Main.scene;
    }

    /**
     * Method used to initialize all actions and events that can be triggered
     * when the user do an action.
     *
     * @param location Unused parameter
     * @param resources Unused parameter
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        loginLabel.setText(Model.getCurrentUser().getUsername());
        warehouseLabel.setText(Model.getCurrentUser().getDeposit().getName());

        /*
          This thread is used to fill the articleListView.
          We ask the "GCLA" request to get all the articles of the selected order.
          @see Request
         */
        Thread subThread = new Thread(() -> {

            Request t = new Request("GCLA", order.getId());
            Socket socket = Model.getSocket();

            try {
                ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                outputStream.writeObject(t);

                ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                Request response = (Request) inputStream.readObject();

                articles = ((HashMap<ArticleCategory,Integer>) response.getData()).keySet();

                Platform.runLater(() -> articleListView.getItems().addAll(articles));

            } catch (IOException | ClassNotFoundException e) {
                LOGGER.log(Level.SEVERE, "Error using the request \"GCLA\" request in the picker modification 2 menu", e);
            }
        });
        subThread.start();

        Tool.setAsNumericField(quantityTextField);
        Tool.setAsNumericField(quantityArticleToAddTextField);
        articleNameTextField.setDisable(true);
        maxNumberPaletteTextField.setDisable(true);
        maxNumberPaletteEuropeTextField.setDisable(true);
        maxNumberPaletteUSATextField.setDisable(true);

        quantityTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!newValue.equals("")) {
                order.getArticles().put(articleListView.getSelectionModel().getSelectedItem().getId(), Integer.parseInt(newValue));
            }
        });

        /*
          We put a trigger event when the user click on the button
          to cancel the article adding and back to the previous page.
         */
        searchButton.setOnMouseClicked(event -> {
            String searchValue = searchTextField.getText().trim();
            if(requestDone && !searchValue.equals("")) {
                /*
                  We use a thread do not interfere with the graphic fluidity
                 */
                Thread thread = new Thread(() -> {
                    requestDone = false;
                    Request t = new Request("GACN", searchValue);
                    Socket socket = Model.getSocket();

                    try {
                        ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                        outputStream.writeObject(t);

                        ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                        Request response = (Request) inputStream.readObject();

                        ArrayList<ArticleCategory> articleCategories = (ArrayList<ArticleCategory>) response.getData();

                        if (articleCategories != null) {
                            Platform.runLater(() -> {
                                articlesToAddListView.getItems().clear();
                                articlesToAddListView.getItems().addAll(articleCategories);
                            });
                        }
                    } catch (IOException | ClassNotFoundException e) {
                        LOGGER.log(Level.SEVERE, "Error using the request \"GACN\" request in the picker modification 2 menu", e);
                    }
                    requestDone = true;
                });
                thread.start();
            }
        });

        /*
          We put a trigger event when the user click on the button
          to cancel the article adding and back to the previous page.
         */
        cancelAddingArticleButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            addArticlePane.setVisible(false);
        });

        /*
          We put a trigger event when the user click on the button
          to confirm the article adding and back to the previous page.
         */
         confirmAddingArticleButton.setOnMouseClicked(event -> {
             shadowPane.setVisible(false);
             addArticlePane.setVisible(false);
             ArticleCategory articleCategory = (ArticleCategory) articlesToAddListView.getSelectionModel().getSelectedItem();
             if(articleCategory != null) {
                 articleListView.getItems().add(articleCategory);
                 order.getArticles().put(articleCategory.getId(),Integer.parseInt(quantityArticleToAddTextField.getText()));
             }
         });

        /*
          We put a trigger event when the user click on the button
          to update the right side of the page with the currently
          selected Article.
         */
        articleListView.setOnMouseClicked(event -> {
            if(articleListView.getSelectionModel().getSelectedItem() != null) {
                ArticleCategory currentArticle = articleListView.getSelectionModel().getSelectedItem();
                int currentID = currentArticle.getId();
                quantityTextField.setText(""+order.getArticles().get(currentID));
                articleNameTextField.setText(""+currentArticle.getName());
                maxNumberPaletteTextField.setText(""+currentArticle.getMaxPalette());
                maxNumberPaletteEuropeTextField.setText(""+currentArticle.getMaxPaletteEurope());
                maxNumberPaletteUSATextField.setText(""+currentArticle.getMaxPaletteUSA());
            }
        });

        /*
          We put a trigger event when the user click on the button
          to delete the currently selected item in the article list
         */
        deleteArticleButton.setOnMouseClicked(event -> {
            ArticleCategory currentArticle = articleListView.getSelectionModel().getSelectedItem();
            if(currentArticle != null) {
                articleListView.getItems().remove(currentArticle);
                quantityTextField.setText("");
                articleNameTextField.setText("");
                maxNumberPaletteTextField.setText("");
                maxNumberPaletteEuropeTextField.setText("");
                maxNumberPaletteUSATextField.setText("");
                order.getArticles().remove(currentArticle.getId());
            }
        });

        /*
          We put a trigger event when the user click on the button
          to set the confirmation and shadow Panes visible and the same for the addArticlePane
         */
        addArticleButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(true);
            addArticlePane.setVisible(true);
            quantityTextField.setText("");
        });

        /*
          We put a trigger event when the user click on the button
          to set the confirmation and shadow Panes visible and we also set the text to the correct confirmation.
         */
        saveOrderButton.setOnMouseClicked(event -> {
            warningLabel2.setText("Avez-vous effectué toutes les modifications que vous souhaitiez ?");
            shadowPane.setVisible(true);
            confirmationPane.setVisible(true);
        });


        /*
          We put a trigger event when the user click on the button
          to set the confirmation and shadow Panes visible and we also set the text to the correct confirmation.
         */
        cancelOrderModificationButton.setOnMouseClicked(event -> {
            warningLabel2.setText("Voulez-vous vraiment annuler toutes les modifications effectuées ?");
            shadowPane.setVisible(true);
            confirmationPane.setVisible(true);
            cancelling = true;
        });

        /*
          We put a trigger event when the user click on the button
          to set the second confirmation and shadow Panes visible and we also set the text to the correct confirmation.
         */
        pickerMenuButton.setOnMouseClicked(event -> {
            warningLabel.setText("Vous ne pouvez pas revenir au menu picker avant d'avoir fini");
            shadowPane.setVisible(true);
            confirmationPane2.setVisible(true);
        });

        /*
          We put a trigger event when the user click on the button
          to set the second confirmation and shadow Panes visible and we also set the text to the correct confirmation.
         */
        disconnectButton.setOnMouseClicked(event -> {
            warningLabel.setText("Vous ne pouvez pas vous déconnecter avant d'avoir fini");
            shadowPane.setVisible(true);
            confirmationPane2.setVisible(true);
        });

        /*
          We put a trigger event when the user click on the button
          to hide the shadow and second confirmation panes.
         */
        okButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane2.setVisible(false);
        });

        /*
          We put a trigger event when the user click on the button
          to move to the picker menu.
         */
        acceptButton.setOnMouseClicked(event -> {

            if(!cancelling) {
                Request t = new Request("SMO", order);
                Socket socket = Model.getSocket();

                try {
                    ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                    outputStream.writeObject(t);

                    ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                    Request response = (Request) inputStream.readObject();

                    boolean success = (boolean) response.getData();

                    if (!success)
                        return;
                } catch (IOException | ClassNotFoundException e) {
                    LOGGER.log(Level.SEVERE, "Error using the request \"SMO\" request in the picker modification 2 menu", e);
                }
            }
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interfaces/Picker/Picker.fxml"));
            fxmlLoader.setController(new PickerController());
            try {
                scene.setRoot(fxmlLoader.load());
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "Error while moving from the picker modification 2 menu to the picker menu", e);
            }
        });

        /*
          We put a trigger event when the user click on the button
          to hide the shadow and confirmation panes.
         */
        refuseButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane.setVisible(false);
            cancelling = false;
        });

        /*
          We put a trigger event when the user click outside the confirmationPane
          to hide the shadow and confirmation panes.
         */
        shadowPane.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane2.setVisible(false);
            confirmationPane.setVisible(false);
            addArticlePane.setVisible(false);
            cancelling = false;
        });

    }
}