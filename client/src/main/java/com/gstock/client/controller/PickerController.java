package com.gstock.client.controller;

import com.gstock.client.Main;
import com.gstock.client.model.Model;
import com.gstock.tools.Tool;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * PickerController is the class that handles all events in the picker main menu.
 * It implements Initializable in order to use FXML components.
 */
public class PickerController implements Initializable {

/**
     * shadowPane is a Java FX Pane component.
     * It is used to add a "shadow" effect behind the confirmation pane.
     */
    @FXML private Pane shadowPane;

    /**
     * confirmationPane is a Java FX Pane component.
     * It is used to get a confirmation to leave the current menu.
     */
    @FXML private Pane confirmationPane;



    /**
     * modifyOrderButton is a Java FX Button component.
     * It is used to move to the first storage menu.
     */
    @FXML private Button modifyOrderButton;

    /**
     * prepareOrderButton is a Java FX Button component.
     * It is used to move to the first preparation menu.
     */
    @FXML private Button prepareOrderButton;

    /**
     * mainMenuButton is a Java FX Button component.
     * It is used to move to the main menu.
     */
    @FXML private Button mainMenuButton;

    /**
     * disconnectButton is a Java FX Button component.
     * It is used to disconnect the user from the server.
     */
    @FXML private Button disconnectButton;

    /**
     * acceptButton is a Java FX Button component.
     * It is used to accept what is said in the confirmationPane.
     */
    @FXML private Button acceptButton;

    /**
     * refuseButton is a Java FX Button component.
     * It is used to refuse what is said in the confirmationPane.
     */
    @FXML private Button refuseButton;



    /**
     * loginLabel is a Java FX Button component.
     * It is used to print the current user's login.
     */
    @FXML private Label loginLabel;

    /**
     * warehouseLabel is a Java FX Button component.
     * It is used to print the current warehouse.
     */
    @FXML private Label warehouseLabel;



    /**
     * scene is a JavaFX Scene component.
     * It is used to add components to the current scene.
     */
    private final Scene scene;

    private Logger LOGGER = Logger.getLogger( this.getClass().getName() );

	/**
     * Default constructor of the PickerController class.
     *
     * @see PickerController
     */
    PickerController() {
        this.scene = Main.scene;
    }

    /**
     * Method used to initialize all actions and events that can be triggered
     * when the user do an action.
     *
     * @param location Unused parameter
     * @param resources Unused parameter
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        loginLabel.setText(Model.getCurrentUser().getUsername());
        warehouseLabel.setText(Model.getCurrentUser().getDeposit().getName());

        /*
          We put a trigger event when the user click on the button
          to move to the first modification menu.
         */
        modifyOrderButton.setOnMouseClicked(event -> {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interfaces/Picker/ModificationCommande1.fxml"));
            fxmlLoader.setController( new PickerModification1Controller() );
            try {
                scene.setRoot(fxmlLoader.load());
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "Error while moving from the picker menu to the picker modification 1 menu", e);
            }
        });

        /*
          We put a trigger event when the user click on the button
          to move to the first preparation menu.
         */
        prepareOrderButton.setOnMouseClicked(event -> {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interfaces/Picker/PreparationCommande1.fxml"));
            fxmlLoader.setController( new PickerPreparation1Controller() );
            try {
                scene.setRoot(fxmlLoader.load());
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "Error while moving from the picker menu to the picker preparation 1 menu", e);
            }
        });

        /*
          We put a trigger event when the user click on the button
          to move to the main menu.
         */
        mainMenuButton.setOnMouseClicked(event -> {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interfaces/MenuPrincipal.fxml"));
            fxmlLoader.setController( new MainMenuController() );
            try {
                scene.setRoot(fxmlLoader.load());
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "Error while moving from the picker menu to the main menu", e);
            }
        });

        /*
          We put a trigger event when the user click on the button
          to set the confirmation and shadow Panes visible.
         */
        disconnectButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(true);
            confirmationPane.setVisible(true);
        });

        /*
          We put a trigger event when the user click on the button
          to disconnect : request to the server to disconnect,
          and we move to the connection menu.
         */
        acceptButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane.setVisible(false);
            try {
                // request to the server to disconnect
                Model.disconnectUser();
                // go to the main menu
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interfaces/Connexion.fxml"));
                fxmlLoader.setController( new ConnectionController() );
                scene.setRoot(fxmlLoader.load());
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "Error while disconnecting the client", e);
            }
        });

		/*
          We put a trigger event when the user click on the button
          to remove the confirmation and shadow Panes.
         */
        refuseButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane.setVisible(false);
        });

        /*
          We put a trigger event when the user click outside the confirmationPane
          to do the same as the refuseButton.
         */
        shadowPane.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane.setVisible(false);
        });

    }
}
