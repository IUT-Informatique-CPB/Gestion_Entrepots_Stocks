package com.gstock.client.controller;

import com.gstock.client.Main;
import com.gstock.client.model.Model;
import com.gstock.tools.Request;
import com.gstock.tools.Tool;
import com.gstock.tools.article.Article;
import com.gstock.tools.article.ArticleCategory;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * OperatorStorage1Controller is the class that handles all events in the first storage menu.
 * It implements Initializable in order to use FXML components.
 */
public class OperatorStorage1Controller implements Initializable {

    /**
     * shadowPane is a Java FX Pane component.
     * It is used to make a shadow affect to make
     * the user focus on the Pane we want
     */
    @FXML private Pane shadowPane;

    /**
     * confirmationPane is a Java FX Pane component.
     * It is used to get a confirmation from the user to
     * either leave the current menu or to confirm the creation
     * of the new product in the database.
     */
    @FXML private Pane confirmationPane;

    /**
     * confirmationPane2 is a Java FX Pane component.
     * It is used to address the user a message on what
     * he need to do or what he can't do
     */
    @FXML private Pane confirmationPane2;

    /**
     * articleSelectPane is a Java FX Pane component.
     * It is used to add or edit the current article
     * values
     */
    @FXML private Pane articleSelectPane;



    /**
     * okButton is a Java FX Button component.
     * It is used by the user to confirm he understood what the confirmationPane said.
     */
    @FXML private Button okButton;

    /**
     * acceptButton is a Java FX Button component.
     * It is used by the user to confirm that he stored the article in the address he chose.
     */
    @FXML private Button acceptButton;

    /**
     * refuseButton is a Java FX Button component.
     * It is used by the user to cancel the confirmation.
     */
    @FXML private Button refuseButton;

    /**
     * operatorMenuButton is a Java FX Button component.
     * The operatorMenuButton is here only a part of the interface that won't do anything.
     * It's only here so that the user isn't lost when he uses the application.
     */
    @FXML private Button operatorMenuButton;

    /**
     * disconnectButton is a Java FX Button component.
     * Same as operatorMenuButton.
     */
    @FXML private Button disconnectButton;

    /**
     * validateArticle is a Java FX Button component.
     * It is used to validate the article modification
     * and return to the previous menu
     */
    @FXML private Button validateArticle;

    /**
     * searchButton is a Java FX Button component.
     * It is used to send a request to the server
     * about the barcode entered
     */
    @FXML private Button searchButton;

    /**
     * modifyButton is a Java FX Button component.
     * It is used to add or edit the article values
     */
    @FXML private Button modifyButton;

    /**
     * storeButton is a Java FX Button component.
     * It is used to valid your the current store try
     */
    @FXML private Button storeButton;



    /**
     * confirmationLabel is a Java FX Label component.
     * It is used to show a message indicating which choice
     * the user is asked.
     */
    @FXML private Label confirmationLabel;

    /**
     * confirmationLabel is a Java FX Label component.
     * It is used to show a message indicating which choice
     * the user is asked.
     */
    @FXML private Label confirmationLabel2;

    /**
     * loginLabel is a Java FX Button component.
     * It is used to print the current user's login.
     */
    @FXML private Label loginLabel;

    /**
     * warehouseLabel is a Java FX Button component.
     * It is used to print the current warehouse.
     */
    @FXML private Label warehouseLabel;

    /**
     * barCodeLabel is a Java FX Label component.
     * It is used to show the article bar code
     */
    @FXML private Label barCodeLabel;

    /**
     * nameLabel is a Java FX Label component.
     * It is used to show the article name
     */
    @FXML private Label nameLabel;

    /**
     * maxNumberPaletteLabel is a Java FX Label component.
     * It is used to show the maximum article amount
     * you can store on a standard palette
     */
    @FXML private Label maxNumberPaletteLabel;

    /**
     * maxNumberEuropePaletteLabel is a Java FX Label component.
     * It is used to show the maximum article amount
     * you can store on a Europe palette
     */
    @FXML private Label maxNumberEuropePaletteLabel;

    /**
     * maxNumberUSAPaletteLabel is a Java FX Label component.
     * It is used to show the maximum article amount
     * you can store on a USA palette
     */
    @FXML private Label maxNumberUSAPaletteLabel;

    /**
     * quantityLabel is a Java FX Label component.
     * It is used to show the article quantity
     */
    @FXML private Label quantityLabel;

    /**
     * customsNumberLabel is a Java FX Label component.
     * It is used to show the customs number of the article
     */
    @FXML private Label customsNumberLabel;

    /**
     * serialNumberLabel is a Java FX Label component.
     * It is used to show the serial number of the article
     */
    @FXML private Label serialNumberLabel;

    /**
     * expirationDateLabel is a Java FX Label component.
     * It is used to show the expiration date of the article
     */
    @FXML private Label expirationDateLabel;

    /**
     * receptionDateLabel is a Java FX Label component.
     * It is used to show the reception date of the article
     */
    @FXML private Label receptionDateLabel;



    /**
     * barCodeTextField is a Java FX TextField component.
     * It is used to edit or enter the article bar code
     */
    @FXML private TextField barCodeTextField;

    /**
     * nameTextField is a Java FX TextField component.
     * It is used to edit or enter the article name
     */
    @FXML private TextField nameTextField;

    /**
     * maxNumberPaletteTextField is a Java FX TextField component.
     * It is used to edit or enter the maximum article amount
     * you can store on a standard palette
     */
    @FXML private TextField maxNumberPaletteTextField;

    /**
     * maxNumberEuropePaletteTextField is a Java FX TextField component.
     * It is used to edit or enter the maximum article amount
     * you can store on a Europe palette
     */
    @FXML private TextField  maxNumberEuropePaletteTextField;

    /**
     * maxNumberUSAPaletteTextField is a Java FX TextField component.
     * It is used to edit or enter the maximum article amount
     * you can store on a USA palette
     */
    @FXML private TextField maxNumberUSAPaletteTextField;

    /**
     * quantityTextField is a Java FX TextField component.
     * It is used to edit or enter the article quantity
     */
    @FXML private TextField quantityTextField;

    /**
     * customsNumberTextField is a Java FX TextField component.
     * It is used to edit or enter the article customs number
     */
    @FXML private TextField customsNumberTextField;

    /**
     * serialNumberTextField is a Java FX TextField component.
     * It is used to edit or enter the article serial number
     */
    @FXML private TextField serialNumberTextField;

    /**
     * expirationDateTextField is a Java FX TextField component.
     * It is used to edit or enter the article expiration date
     */
    @FXML private TextField expirationDateTextField;

    /**
     * receptionDateTextField is a Java FX TextField component.
     * It is used to edit or enter the article reception date
     */
    @FXML private TextField receptionDateTextField;



    /**
     * disconnecting is a boolean.
     * It is used to know if the user is trying to disconnect
     * false = not trying
     * true = trying
     */
    private boolean disconnecting = false;

    /**
     * backToMenu is a boolean.
     * It is used to know if the user is trying to get to the operator menus
     * false = not trying
     * true = trying
     */
    private boolean backToMenu = false;

    /**
     * storing is a boolean.
     * It is used to know if the user is trying to store the article
     * false = not trying
     * true = trying
     */
    private boolean storing = false;

    /**
     * articleIsNew is a boolean.
     * It is used to know if the article scanned is new or notv
     * false = not new
     * true = new
     */
    private boolean articleIsNew = false;

    /**
     * validated is a boolean.
     * It is used to know if the article is validated or not
     * false = not validated
     * true = validated
     */
    private boolean validated = false;

    /**
     * requestDone is a boolean.
     * It is used to mark when all the requests of
     * the page are done
     */
    private boolean requestDone = true;



    /**
     * article is an Article ;).
     * It is used to store the current article values
     */
    private Article article = new Article();



    /**
     * scene is a JavaFX Scene component.
     * It is used to add components to the current scene.
     */
    private final Scene scene;

    private Logger LOGGER = Logger.getLogger( this.getClass().getName() );

    /**
     * Default constructor of the OperatorStorage1Controller class.
     *
     * @see OperatorStorage1Controller
     */
    OperatorStorage1Controller() {
        this.scene = Main.scene;
    }

    /**
     * Method used to initialize all actions and events that can be triggered
     * when the user do an action.
     *
     * @param location Unused parameter
     * @param resources Unused parameter
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        loginLabel.setText(Model.getCurrentUser().getUsername());
        warehouseLabel.setText(Model.getCurrentUser().getDeposit().getName());

        /*
          Setting some TextFields as numeric
         */
        Tool.setAsNumericField(barCodeTextField);
        Tool.setMaxDigit(barCodeTextField,13);
        Tool.setAsNumericField(maxNumberPaletteTextField);
        Tool.setAsNumericField(maxNumberUSAPaletteTextField);
        Tool.setAsNumericField(maxNumberEuropePaletteTextField);
        Tool.setAsNumericField(serialNumberTextField);
        Tool.setAsNumericField(customsNumberTextField);
        Tool.setAsNumericField(quantityTextField);

        /*
          We check if the barcodeText is correct or not
         */
        barCodeTextField.textProperty().addListener((observable, oldValue, newValue) -> {
            if(!oldValue.equals(newValue))
                validated = false;
        });

        /*
          We put a trigger event when the user click on the button
          to send a request to the server to know if the article is new or not
         */
        searchButton.setOnMouseClicked(event -> {
            if(barCodeTextField.getText().length() == 13) {
                if(requestDone) {
                    /*
                      We use a thread to make the request to avoid blocking
                      the user interface
                     */
                    Thread thread = new Thread(() -> {
                        requestDone = false;
                        Request t = new Request("GAC", barCodeTextField.getText());
                        Socket socket = Model.getSocket();

                        try {
                            ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                            outputStream.writeObject(t);

                            ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                            Request response = (Request) inputStream.readObject();

                            ArticleCategory articleCategory = (ArticleCategory) response.getData();

                            Platform.runLater(() -> {
                                barCodeLabel.setText("Code Barre : "
                                        + barCodeTextField.getText());
                                if (articleCategory != null) {
                                    article.setArticleCategory(articleCategory);
                                    articleIsNew = false;
                                    nameTextField.setDisable(true);
                                    maxNumberEuropePaletteTextField.setDisable(true);
                                    maxNumberPaletteTextField.setDisable(true);
                                    maxNumberUSAPaletteTextField.setDisable(true);

                                    nameTextField.setText(articleCategory.getName());
                                    maxNumberEuropePaletteTextField.setText(String.valueOf(articleCategory.getMaxPaletteEurope()));
                                    maxNumberPaletteTextField.setText(String.valueOf(articleCategory.getMaxPalette()));
                                    maxNumberUSAPaletteTextField.setText(String.valueOf(articleCategory.getMaxPaletteUSA()));

                                    nameLabel.setText("Nom article : "
                                            + articleCategory.getName());
                                    maxNumberEuropePaletteLabel.setText("Nombre d'article(s) maximum (Palette Europe) : "
                                            + String.valueOf(articleCategory.getMaxPaletteEurope()));
                                    maxNumberPaletteLabel.setText("Nombre d'article(s) maximum (Palette) : "
                                            + String.valueOf(articleCategory.getMaxPalette()));
                                    maxNumberUSAPaletteLabel.setText("Nombre d'article(s) maximum (Palette USA) : "
                                            + String.valueOf(articleCategory.getMaxPaletteUSA()));
                                } else {
                                    articleIsNew = true;
                                    nameTextField.setDisable(false);
                                    maxNumberEuropePaletteTextField.setDisable(false);
                                    maxNumberPaletteTextField.setDisable(false);
                                    maxNumberUSAPaletteTextField.setDisable(false);

                                    nameTextField.setText("");
                                    maxNumberEuropePaletteTextField.setText("");
                                    maxNumberPaletteTextField.setText("");
                                    maxNumberUSAPaletteTextField.setText("");

                                    nameLabel.setText("Nom article : ");
                                    maxNumberPaletteLabel.setText("Nombre d'article(s) maximum (Palette) :");
                                    maxNumberEuropePaletteLabel.setText("Nombre d'article(s) maximum (Palette Europe) :");
                                    maxNumberUSAPaletteLabel.setText("Nombre d'article(s) maximum (Palette USA) :");
                                }
                                shadowPane.setVisible(true);
                                articleSelectPane.setVisible(true);
                            });
                        } catch (IOException | ClassNotFoundException e) {
                            LOGGER.log(Level.SEVERE, "Error using the request \"GAC\" request in the operator storage 1 menu", e);
                        }
                        validated = true;
                        requestDone = true;
                    });
                    thread.start();
                }
            }
            else {
                confirmationPane2.setVisible(true);
                shadowPane.setVisible(true);
                confirmationLabel2.setText("Vous devez entrer un code barre valide");
            }
        });

        /*
          We put a trigger event when the user click on the button
          to set the articleSelectPane and shadow Panes visible.
         */
        modifyButton.setOnMouseClicked(event -> {
            if(validated) {
                shadowPane.setVisible(true);
                articleSelectPane.setVisible(true);
            }
            else {
                confirmationPane2.setVisible(true);
                shadowPane.setVisible(true);
                confirmationLabel2.setText("Vous devez entrer un code barre valide");
            }
        });

        /*
          We put a trigger event when the user click on the button
          to set the articleSelectPane and shadow Panes not visible
          and we also set the labels to their new values.
         */
        validateArticle.setOnMouseClicked((MouseEvent event) -> {
            shadowPane.setVisible(false);
            articleSelectPane.setVisible(false);

            nameLabel.setText("Nom article : "
                    +nameTextField.getText());
            maxNumberPaletteLabel.setText("Nombre d'article(s) maximum (Palette) : "
                    +maxNumberPaletteTextField.getText());
            maxNumberUSAPaletteLabel.setText("Nombre d'article(s) maximum (Palette USA) : "
                    +maxNumberUSAPaletteTextField.getText());
            maxNumberEuropePaletteLabel.setText("Nombre d'article(s) maximum (Palette Europe) : "
                    +maxNumberEuropePaletteTextField.getText());
            quantityLabel.setText("Quantité article : "
                    +quantityTextField.getText());
            expirationDateLabel.setText("Date d'expiration : "
                    +expirationDateTextField.getText());
            receptionDateLabel.setText("Date de réception : "
                    +receptionDateTextField.getText());
            serialNumberLabel.setText("Numéro de série : "
                    +serialNumberTextField.getText());
            customsNumberLabel.setText("Numéro de douane : "
                    +customsNumberTextField.getText());
        });

        /*
          We put a trigger event when the user click on the button
          to set the confirmation and shadow Panes visible and we also set the text to the correct confirmation.
         */
        storeButton.setOnMouseClicked(event -> {
            String quantity = quantityTextField.getText().trim();
            String name = nameTextField.getText().trim();
            String maxPalette = maxNumberPaletteTextField.getText().trim();
            String maxPaletteEU = maxNumberEuropePaletteTextField.getText().trim();
            String maxPaletteUSA = maxNumberUSAPaletteTextField.getText().trim();
            if (!validated) {
                confirmationPane2.setVisible(true);
                shadowPane.setVisible(true);
                confirmationLabel2.setText("Vous devez entrer un code barre valide");
            } else {
                if (!quantity.equals("") && !name.equals("") && !maxPalette.equals("") && !maxPaletteEU.equals("") && !maxPaletteUSA.equals("")) {
                    shadowPane.setVisible(true);
                    confirmationPane.setVisible(true);
                    confirmationLabel.setText("Voulez-vous vraiment commencer à stocker cet article ?");
                    storing = true;
                } else {
                    shadowPane.setVisible(true);
                    confirmationPane2.setVisible(true);
                    confirmationLabel2.setText("Vous devez entrer tous les champs obligatoires");
                }
            }
        });

        /*
          We put a trigger event when the user click on the button
          to set the confirmation and shadow Panes visible and we also set the text to the correct confirmation.
         */
        disconnectButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(true);
            confirmationPane.setVisible(true);
            confirmationLabel.setText("Voulez-vous vraiment vous déconnecter ?");
            disconnecting = true;
        });

        /*
          We put a trigger event when the user click on the button
          to set the confirmation and shadow Panes visible and we also set the text to the correct confirmation.
         */
        operatorMenuButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(true);
            confirmationPane.setVisible(true);
            confirmationLabel.setText("Voulez-vous vraiment revenir au menu carriste ?");
            backToMenu = true;
        });

        /*
          We put a trigger event when the user click on the button
          to set the shadow, the confirmation and the confirmation2 Panes not visible.
         */
        shadowPane.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane.setVisible(false);
            confirmationPane2.setVisible(false);
            articleSelectPane.setVisible(false);
        });

        /*
          We put a trigger event when the user click on the button
          to set the shadow and the confirmation Panes not visible.
         */
        refuseButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane.setVisible(false);
            backToMenu = false;
            disconnecting = false;
            storing = false;
        });

        /*
          We put a trigger event when the user click on the button
          to set the confirmation2 and shadow Panes not visible.
         */
        okButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane2.setVisible(false);
        });

        /*
          We put a trigger event when the user click on the button
          to switch menus depending on the current actions.
         */
        acceptButton.setOnMouseClicked(event -> {
            // if the user wanted to back to the operator menu
            if(backToMenu) {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interfaces/Cariste/Cariste.fxml"));
                fxmlLoader.setController(new OperatorController());
                try {
                    scene.setRoot(fxmlLoader.load());
                } catch (IOException e) {
                    LOGGER.log(Level.SEVERE, "Error while moving from the operator storage 1 menu to the operator menu", e);
                }
            }
            // if the user wanted to disconnect
            else if(disconnecting) {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interfaces/Connexion.fxml"));
                fxmlLoader.setController(new ConnectionController());
                try {
                    scene.setRoot(fxmlLoader.load());
                } catch (IOException e) {
                    LOGGER.log(Level.SEVERE, "Error while disconnecting the client", e);
                }
            }
            // if the user wanted to start storing the current article
            else if(storing) {
                String quantity = quantityTextField.getText().trim();
                String name = nameTextField.getText().trim();
                String maxPalette = maxNumberPaletteTextField.getText().trim();
                String maxPaletteEU = maxNumberEuropePaletteTextField.getText().trim();
                String maxPaletteUSA = maxNumberUSAPaletteTextField.getText().trim();
                String customs = customsNumberTextField.getText().trim();
                String expiration = expirationDateTextField.getText().trim();
                String reception = receptionDateTextField.getText().trim();
                String serial = serialNumberTextField.getText().trim();
                if (articleIsNew) {
                    article.setArticleCategory(new ArticleCategory(barCodeTextField.getText().trim(), name
                            , Integer.parseInt(maxPalette), Integer.parseInt(maxPaletteEU), Integer.parseInt(maxPaletteUSA)));
                }
                if(!customs.equals(""))
                    article.setCustomsNumber(Integer.parseInt(customs));
                else
                    article.setCustomsNumber(null);
                if(!expiration.equals(""))
                    article.setExpirationDate(expiration);
                else
                    article.setExpirationDate(null);
                if(!reception.equals(""))
                    article.setReceiptDate(reception);
                else
                    article.setReceiptDate(null);
                if(!serial.equals(""))
                    article.setSerialNumber(Integer.parseInt(serial));
                else
                    article.setSerialNumber(null);
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interfaces/Cariste/Stockage2.fxml"));
                fxmlLoader.setController(new OperatorStorage2Controller(article, quantity, articleIsNew));
                try {
                    scene.setRoot(fxmlLoader.load());
                } catch (IOException e) {
                    LOGGER.log(Level.SEVERE, "Error while moving from the operator storage 1 menu to the operator storage 2 menu", e);
                }
            }
        });
    }
}
