package com.gstock.client.controller;

import com.gstock.client.Main;
import com.gstock.client.model.Model;
import com.gstock.tools.Request;
import com.gstock.tools.Tool;
import com.gstock.tools.address.Address;
import com.gstock.tools.article.ArticleCategory;
import com.gstock.tools.article.ArticleToPick;
import com.gstock.tools.order.Order;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.Pane;
import javafx.util.Pair;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.URL;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * PickerPreparation2Controller is the class that handles all events in the first preparation menu.
 * It implements Initializable in order to use FXML components.
 */
public class PickerPreparation2Controller implements Initializable {

    /**
     * confirmationPane is a Java FX Pane component.
     * It is used to get a confirmation from the user that he confirm the preparation.
     */
    @FXML private Pane confirmationPane;

    /**
     * confirmationPane2 is a Java FX Pane component.
     * It is used to notice the user he can't leave the current menu until he finished.
     */
    @FXML private Pane confirmationPane2;

    /**
     * shadowPane is a Java FX Pane component.
     * It is used to add a "shadow" effect behind the confirmation pane.
     */
    @FXML private Pane shadowPane;



    /**
     * pickedUpButton is a Java FX Button component.
     * It is use by the user to confirm he picked up the selected article.
     */
    @FXML private Button pickedUpButton;

    /**
     * pickerMenuButton is a Java FX Button component.
     * The pickerMenuButton is here only a part of the interface that won't do anything.
     * It's only here so that the user isn't lost when he uses the application.
     */
    @FXML private Button pickerMenuButton;

    /**
     * disconnectButton is a Java FX Button component.
     * Same as pickerMenuButton.
     */
    @FXML private Button disconnectButton;

    /**
     * okButton is a Java FX Button component.
     * It is used by the user to confirm he understood what the confirmationPane2 said.
     */
    @FXML private Button okButton;

    /**
     * acceptButton is a Java FX Button component.
     * It is used by the user to confirm that he wants to prepare the order (final confirmation).
     */
    @FXML private Button acceptButton;

    /**
     * refuseButton is a Java FX Button component.
     * It is used by the user to cancel the confirmation.
     */
    @FXML private Button refuseButton;

    /**
     * This thing us used to store the data we receive to use it later
     */
    private TreeMap<Address, HashMap<ArticleCategory, Integer>> responseData;

    /**
     * articles is a variable used to store the list
     * of articles gathered from the server
     * to avoid making another request
     */
    private ArrayList<ArticleToPick> articles = new ArrayList<>();



    /**
     * order is the current selected order to process.
     */
    private Order order;



    /**
     * scene is a JavaFX List component.
     * It is used to show the articles of the selected order.
     */
    @FXML private ListView<ArticleToPick> articleListView;



    /**
     * scene is a JavaFX Label component.
     * It is used to show number of the current order.
     */
    @FXML private Label orderNumberLabel;

    /**
     * scene is a JavaFX Label component.
     * It is used to show the customer name who made the order.
     */
    @FXML private Label customerNameLabel;

    /**
     * scene is a JavaFX Label component.
     * It is used to show the number of products in the order.
     */
    @FXML private Label productNumberLabel;

    /**
     * warningLabel is a Java FX Label component.
     * It is used to set a different message depending on the confirmation.
     */
    @FXML private Label warningLabel;

    /**
     * loginLabel is a Java FX Button component.
     * It is used to print the current user's login.
     */
    @FXML private Label loginLabel;

    /**
     * warehouseLabel is a Java FX Button component.
     * It is used to print the current warehouse.
     */
    @FXML private Label warehouseLabel;



    private boolean startedPicking = false;

    /**
     * scene is a JavaFX Scene component.
     * It is used to add components to the current scene.
     */
    private final Scene scene;

    private Logger LOGGER = Logger.getLogger( this.getClass().getName() );

    /**
     * Default constructor of the PickerPreparation2Controller class.
     *
     * @see PickerPreparation2Controller
     */
    PickerPreparation2Controller(Order order) {
        this.order = order;
        this.scene = Main.scene;
    }

    /**
     * Method used to initialize all actions and events that can be triggered
     * when the user do an action.
     *
     * @param location Unused parameter
     * @param resources Unused parameter
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        loginLabel.setText(Model.getCurrentUser().getUsername());
        warehouseLabel.setText(Model.getCurrentUser().getDeposit().getName());

        orderNumberLabel.setText("COMMANDE : " + order.getId());
        int quantitySum = 0;
        for (Integer integer : order.getArticles().values())
            quantitySum += integer;
        productNumberLabel.setText("Nombre de produits : " + quantitySum);

        /*
          Thread used to get the articles/address list and
          the client name as a background task(without blocking
          the graphic part of the application).
         */
        Thread thread = new Thread(() -> {
            Request t = new Request("UCP", order.getId());
            Socket socket = Model.getSocket();

            try {

                ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                outputStream.writeObject(t);

                ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                Request response = (Request) inputStream.readObject();

                responseData = (TreeMap<Address, HashMap<ArticleCategory, Integer>>) response.getData();

                if (responseData != null) {
                    Platform.runLater(() -> {
                        for (Address adr : responseData.keySet()) {
                            for (ArticleCategory art : responseData.get(adr).keySet()) {
                                ArticleToPick articleToPick = new ArticleToPick(art, adr, responseData.get(adr).get(art));
                                articles.add(articleToPick);
                                articleListView.getItems().addAll(articleToPick);
                            }
                        }
                    });
                }

                t = new Request("GCN", order.getId());
                outputStream = new ObjectOutputStream(socket.getOutputStream());
                outputStream.writeObject(t);
                inputStream = new ObjectInputStream(socket.getInputStream());
                response = (Request) inputStream.readObject();

                String clientName = (String) response.getData();

                Platform.runLater(() -> customerNameLabel.setText("Nom Client : " + clientName));
            } catch (IOException | ClassNotFoundException e) {
                LOGGER.log(Level.SEVERE, "Error using the request \"UCP\" request in the picker preparation 2 menu", e);
            }
        });
        thread.start();

        /*
          We put a trigger event when the user click on the button
          to show the shadow and confirmation panes.
         */
        pickedUpButton.setOnMouseClicked(event -> {
            startedPicking = true;
            if (articleListView.getItems().size() > 0) {
                articleListView.getItems().removeAll(articleListView.getItems().remove(0));
                if(articleListView.getItems().size() == 0) {
                    shadowPane.setVisible(true);
                    confirmationPane.setVisible(true);
                }
            }
        });

        /*
          We put a trigger event when the user click on the button
          to set the second confirmation and shadow Panes visible and we also set the text to the 4pi  correct confirmation.
          if the user started picking up articles. If not, it vomes to the picker menu.
         */
        pickerMenuButton.setOnMouseClicked(event -> {
            if (startedPicking) {
                warningLabel.setText("Vous ne pouvez pas revenir au menu picker avant d'avoir fini");
                shadowPane.setVisible(true);
                confirmationPane2.setVisible(true);
            } else {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interfaces/Picker/Picker.fxml"));
                fxmlLoader.setController(new PickerController());
                try {
                    scene.setRoot(fxmlLoader.load());
                } catch (IOException e) {
                    LOGGER.log(Level.SEVERE, "Error while moving from the picker preparation 2 menu to the picker menu", e);
                }
            }
        });

        /*
          We put a trigger event when the user click on the button
          to set the second confirmation and shadow Panes visible and we also set the text to the correct confirmation.
         */
        disconnectButton.setOnMouseClicked(event -> {
            warningLabel.setText("Vous ne pouvez pas vous déconnecter avant d'avoir fini");
            shadowPane.setVisible(true);
            confirmationPane2.setVisible(true);
        });

        /*
          We put a trigger event when the user click on the button
          to hide the shadow and second confirmation panes.
         */
        okButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane2.setVisible(false);
        });

        /*
          We put a trigger event when the user click on the button
          to move to the picker menu.
         */
        acceptButton.setOnMouseClicked(event -> {

            HashMap<Integer, Pair<ArticleCategory, Integer>> currentOrder = new HashMap<>();

            for(Address adr : responseData.keySet()) {
                for (ArticleCategory ac : responseData.get(adr).keySet()) {
                    currentOrder.put(adr.getId(), new Pair<>(ac, responseData.get(adr).get(ac)));
                }
            }

            Request t = new Request("SCA", currentOrder);
            Socket socket = Model.getSocket();

            Request t1 = new Request("SCPE", order);

            try {
                ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                outputStream.writeObject(t);

                ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                Request response = (Request) inputStream.readObject();

                boolean responseData = (boolean)response.getData();

                if (!responseData)
                    return;

                Socket socket2 = Model.getSocket();
                outputStream = new ObjectOutputStream(socket2.getOutputStream());
                outputStream.writeObject(t1);

                inputStream = new ObjectInputStream(socket2.getInputStream());
                response = (Request) inputStream.readObject();

                responseData = (boolean)response.getData();
                if(!responseData)
                    return;

            } catch (IOException | ClassNotFoundException e) {
                LOGGER.log(Level.SEVERE, "Error using the request \"SCPE\" request in the picker modification 2 menu", e);
            }

            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interfaces/Picker/Picker.fxml"));
            fxmlLoader.setController(new PickerController());
            try {
                scene.setRoot(fxmlLoader.load());
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "Error while moving from the picker preparation 2 menu to the picker menu", e);
            }
        });

        /*
          We put a trigger event when the user click on the button
          to hide the shadow and confirmation panes.
         */
        refuseButton.setOnMouseClicked(event -> {
            articleListView.getItems().addAll(articles);
            shadowPane.setVisible(false);
            confirmationPane.setVisible(false);
        });

        /*
          We put a trigger event when the user click outside the confirmationPane
          to hide the shadow and confirmation panes.
         */
        shadowPane.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane2.setVisible(false);
            confirmationPane.setVisible(false);
        });

    }
}