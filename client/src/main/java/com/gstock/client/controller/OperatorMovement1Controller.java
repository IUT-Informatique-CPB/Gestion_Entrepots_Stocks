package com.gstock.client.controller;

import com.gstock.client.Main;
import com.gstock.client.model.Model;
import com.gstock.tools.Request;
import com.gstock.tools.Tool;
import com.gstock.tools.address.Address;
import com.gstock.tools.article.ArticleCategory;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.URL;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * OperatorMovement1Controller is the class that handles all events in the first movement menu.
 * It implements Initializable in order to use FXML components.
 */
public class OperatorMovement1Controller implements Initializable {

	/**
     * confirmationPane is a Java FX Pane component.
     * It is used to get a confirmation from the user to leave the current menu.
     */
    @FXML private Pane confirmationPane;

    /**
     * confirmationPane is a Java FX Pane component.
     * It is used to notice the user that he can't move to the next menu until he select an article.
     */
    @FXML private Pane confirmationPane2;

    /**
     * shadowPane is a Java FX Pane component.
     * It is used to add a "shadow" effect behind the confirmation pane.
     */
    @FXML private Pane shadowPane;



    /**
     * articleMovementButton is a Java FX Button component.
     * It is used to move the selected article to the selected address.
     */
    @FXML private Button articleMovementButton;

    /**
     * operatorMenuButton is a Java FX Button component.
     * It is used to return to the operator menu.
     */
    @FXML private Button operatorMenuButton;

    /**
     * disconnectButton is a Java FX Button component.
     * It is used to disconnect the user from the server.
     */
    @FXML private Button disconnectButton;

    /**
     * acceptButton is a Java FX Button component.
     * It is used to accept what is said in the confirmationPane.
     */
    @FXML private Button acceptButton;

    /**
     * refuseButton is a Java FX Button component.
     * It is used to refuse what is said in the confirmationPane.
     */
    @FXML private Button refuseButton;

    /**
     * okButton is a Java FX Button component.
     * It is used by the user to confirm he understood what is said in the confirmationPane2
     */
    @FXML private Button okButton;



    /**
     * loginLabel is a Java FX Label component.
     * It is used to print the current user's login.
     */
    @FXML private Label loginLabel;

    /**
     * warehouseLabel is a Java FX Label component.
     * It is used to print the current warehouse.
     */
    @FXML private Label warehouseLabel;



    /**
     * articleCategoryListView is a Java FX ListView component.
     * It is used to show all the articles available to move.
     */
    @FXML private ListView<ArticleCategory> articleCategoryListView;

    /**
     * addressListView is a Java FX ListView component.
     * It is used to show all the address where the quantity of the article selected is low.
     */
    @FXML private ListView<Address> addressListView;



    /**
     * quantityTextField is a Java FX TextField component.
     * It is used to show the quantity of article needed to refill the address selected previously.
     */
    @FXML private TextField quantityTextField;



    /**
     * articleQuantity is a int.
     * It is used to store the current article quantity stored in the selected address.
     */
    private HashMap<Address,Integer> articleQuantity = new HashMap<>();


    /**
     * selectedAdress is an Address.
     * It is used to store the selected Address in the addressListView.
     */
    private Address selectedAdress;



    /**
     * scene is a JavaFX Scene component.
     * It is used to add components to the current scene.
     */
    private final Scene scene;

    private Logger LOGGER = Logger.getLogger( this.getClass().getName() );

    /**
     * Default constructor of the OperatorMovement1Controller class.
     *
     * @see OperatorMovement1Controller
     */
    OperatorMovement1Controller() {
        this.scene = Main.scene;
    }

    /**
     * Method used to initialize all actions and events that can be triggered
     * when the user do an action.
     *
     * @param location Unused parameter
     * @param resources Unused parameter
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        loginLabel.setText(Model.getCurrentUser().getUsername());
        warehouseLabel.setText(Model.getCurrentUser().getDeposit().getName());

        /*
          This thread is used to fill the articleCategoryListView.
          We ask the "GAML" request to get all the article categories
          @see Request
         */
        Thread thread = new Thread(() -> {
            Request t = new Request("GAML", null);
            Socket socket = Model.getSocket();

            try {
                ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                outputStream.writeObject(t);

                ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                Request response = (Request) inputStream.readObject();

                TreeSet<ArticleCategory> arrayArticleCategory = (TreeSet<ArticleCategory>) response.getData();

                Platform.runLater(() -> {
                    if (arrayArticleCategory != null) {
                        for (ArticleCategory ac : arrayArticleCategory) {
                            articleCategoryListView.getItems().add(ac);
                        }
                    }
                });
            } catch (IOException | ClassNotFoundException e) {
                LOGGER.log(Level.SEVERE, "Error using the request \"GAML\" request in the operator movement 1 menu", e);
            }
        });
        thread.start();

        /*
          This thread is used to fill the addressListView.
          We ask the "GAMEL" request to get all the address that correspond to the selected article category
          @see Request
         */
        articleCategoryListView.setOnMouseClicked(event -> {
            if(articleCategoryListView.getSelectionModel().getSelectedItem() != null) {
                Thread subThread = new Thread(() -> {

                        Request t = new Request("GAMEL", articleCategoryListView.getSelectionModel().getSelectedItem().getId());
                        Socket socket = Model.getSocket();

                        try {
                            ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                            outputStream.writeObject(t);

                            ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                            Request response = (Request) inputStream.readObject();

                            TreeMap<Address,Integer> addresses = ((TreeMap<Address,Integer>) response.getData());

                            if (addresses != null) {
                                Platform.runLater(() -> {
                                    addressListView.getItems().clear();
                                    for (Map.Entry<Address, Integer> a : addresses.entrySet()) {
                                        selectedAdress = a.getKey();
                                        articleQuantity.put(a.getKey(),a.getValue());
                                        addressListView.getItems().add(selectedAdress);
                                    }
                                });
                            }
                        } catch (IOException | ClassNotFoundException e) {
                            LOGGER.log(Level.SEVERE, "Error using the request \"GAMEL\" request in the operator movement 1 menu", e);
                        }
                    });
                    subThread.start();
            }
        });

        /*
          If we select an address, we fill the quantity of articles in this address.
         */
        addressListView.setOnMouseClicked(event -> {
            if(addressListView.getSelectionModel().getSelectedItem() != null) {
                selectedAdress = addressListView.getSelectionModel().getSelectedItem();
                for (Map.Entry<Address, Integer> addressIntegerEntry : articleQuantity.entrySet()) {
                    if (addressIntegerEntry.getKey().getId() == selectedAdress.getId()) {
                        quantityTextField.setText(""+addressIntegerEntry.getValue());
                    }
                }
            }
        });

        /*
          We put a trigger event when the user click on the button
          to move to the second movement menu.
         */
        articleMovementButton.setOnMouseClicked(event -> {
            if (addressListView.getSelectionModel().getSelectedItem() != null) {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interfaces/Cariste/Mouvement2.fxml"));
                fxmlLoader.setController(new OperatorMovement2Controller(articleCategoryListView.getSelectionModel().getSelectedItem(), selectedAdress));
                try {
                    scene.setRoot(fxmlLoader.load());
                } catch (IOException e) {
                    LOGGER.log(Level.SEVERE, "Error while moving from the operator movement 1 menu to the operator movement 2 menu", e);
                }
            } else {
                shadowPane.setVisible(true);
                confirmationPane2.setVisible(true);
            }
        });

        /*
          We put a trigger event when the user click on the button
          to move to the operator menu.
         */
        operatorMenuButton.setOnMouseClicked(event -> {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interfaces/Cariste/Cariste.fxml"));
            fxmlLoader.setController( new OperatorController() );
            try {
                scene.setRoot(fxmlLoader.load());
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "Error while moving from the operator movement 1 menu to the operator menu", e);
            }
        });

        /*
          We put a trigger event when the user click on the button
          to set the confirmation and shadow Panes visible.
         */
        disconnectButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(true);
            confirmationPane.setVisible(true);
        });

        /*
          We put a trigger event when the user click on the button
          to disconnect : request to the server to disconnect,
          and we move to the connection menu.
         */
        acceptButton.setOnMouseClicked(event -> {
            try {
                // request to the server to disconnect
                Model.disconnectUser();
                // go to the main menu
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interfaces/Connexion.fxml"));
                fxmlLoader.setController( new ConnectionController() );
                scene.setRoot(fxmlLoader.load());
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "Error while disconnecting the client", e);
            }
        });

        /*
          We put a trigger event when the user click on the button
          to remove the confirmation and shadow Panes.
         */
        refuseButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane.setVisible(false);
        });

        /*
          We put a trigger event when the user click on the button
          to remove the second confirmation and shadow Panes.
         */
        okButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane2.setVisible(false);
        });

        /*
          We put a trigger event when the user click outside the confirmationPane
          to do the same as the refuseButton.
         */
        shadowPane.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane.setVisible(false);
            confirmationPane2.setVisible(false);
        });
    }

}
