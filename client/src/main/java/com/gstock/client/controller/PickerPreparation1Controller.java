package  com.gstock.client.controller;

import com.gstock.client.Main;
import com.gstock.client.model.Model;
import com.gstock.tools.Request;
import com.gstock.tools.Tool;
import com.gstock.tools.order.Order;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * PickerPreparation1Controller is the class that handles all events in the first preparation menu.
 * It implements Initializable in order to use FXML components.
 */
public class PickerPreparation1Controller implements Initializable {

	/**
     * confirmationPane is a Java FX Pane component.
     * It is used to get a confirmation from the user that he wants to prepare the selected order.
     */
    @FXML private Pane confirmationPane;

    /**
     * shadowPane is a Java FX Pane component.
     * It is used to add a "shadow" effect behind the confirmation pane.
     */
    @FXML private Pane shadowPane;



    /**
     * prepareButton is a Java FX Button component.
     * It is use by the user to confirm he wants prepare the selected order.
     */
    @FXML private Button prepareButton;

    /**
     * pickerMenuButton is a Java FX Button component.
     * It is used to return to the picker menu.
     */
    @FXML private Button pickerMenuButton;

    /**
     * disconnectButton is a Java FX Button component.
     * It is used to disconnect the user from the server.
     */
    @FXML private Button disconnectButton;

    /**
     * acceptButton is a Java FX Button component.
     * It is used by the user to confirm that he wants to prepare the order (final confirmation).
     */
    @FXML private Button acceptButton;

    /**
     * refuseButton is a Java FX Button component.
     * It is used by the user to cancel the confirmation.
     */
    @FXML private Button refuseButton;



    /**
     * loginLabel is a Java FX Button component.
     * It is used to print the current user's login.
     */
    @FXML private Label loginLabel;

    /**
     * warehouseLabel is a Java FX Button component.
     * It is used to print the current warehouse.
     */
    @FXML private Label warehouseLabel;



    /**
     * orderListView is a Java FX ListView component.
     * It is used to show all the commands that need to be prepared.
     */
    @FXML private ListView<Order> orderListView;



    /**
     * scene is a JavaFX Scene component.
     * It is used to add components to the current scene.
     */
    private final Scene scene;

    private Logger LOGGER = Logger.getLogger( this.getClass().getName() );

	/**
     * Default constructor of the PickerPreparation1Controller class.
     *
     * @see PickerPreparation1Controller
     */
    PickerPreparation1Controller() {
        this.scene = Main.scene;
    }

    /**
     * Method used to initialize all actions and events that can be triggered
     * when the user do an action.
     *
     * @param location Unused parameter
     * @param resources Unused parameter
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        loginLabel.setText(Model.getCurrentUser().getUsername());
        warehouseLabel.setText(Model.getCurrentUser().getDeposit().getName());

        /*
          This thread is used to fill the orderListView.
          We ask the "GCLC" request to get all the orders that aren't completed.
          @see Request
         */
        Thread thread = new Thread(() -> {
            Request t = new Request("GCLC", null);
            Socket socket = Model.getSocket();

            try {
                ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                outputStream.writeObject(t);

                ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                Request response = (Request)inputStream.readObject();

                ArrayList<Order> orders = (ArrayList<Order>)response.getData();

                if(orders != null){
                    Platform.runLater(() -> orderListView.getItems().addAll(orders));
                }
            } catch (IOException | ClassNotFoundException e) {
                LOGGER.log(Level.SEVERE, "Error using the request \"GCLC\" request in the picker preparation 1 menu", e);
            }
        });
        thread.start();

		/*
          We put a trigger event when the user click on the button
          to move to the second preparation menu.
         */
        prepareButton.setOnMouseClicked(event -> {
            if(orderListView.getSelectionModel().getSelectedItem() != null) {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interfaces/Picker/PreparationCommande2.fxml"));
                fxmlLoader.setController( new PickerPreparation2Controller(orderListView.getSelectionModel().getSelectedItem()) );
                try {
                    scene.setRoot(fxmlLoader.load());
                } catch (IOException e) {
                    LOGGER.log(Level.SEVERE, "Error while moving from the picker preparation 1 menu to the preparation 2 menu", e);
                }
            }
        });

        /*
          We put a trigger event when the user click on the button
          to move to the picker main menu.
         */
        pickerMenuButton.setOnMouseClicked(event -> {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interfaces/Picker/Picker.fxml"));

            fxmlLoader.setController( new PickerController() );
            try {
                scene.setRoot(fxmlLoader.load());
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "Error while moving from the picker preparation 1 menu to the picker menu", e);
            }
        });

        /*
          We put a trigger event when the user click on the button
          to set the confirmation and shadow Panes visible.
         */
        disconnectButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(true);
            confirmationPane.setVisible(true);
        });

        /*
          We put a trigger event when the user click on the button
          to disconnect : request to the server to disconnect,
          and we move to the connection menu.
         */
        acceptButton.setOnMouseClicked(event -> {
            try {
                // request to the server to disconnect
                Model.disconnectUser();
                // go to the main menu
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interfaces/Connexion.fxml"));
                fxmlLoader.setController( new ConnectionController() );
                scene.setRoot(fxmlLoader.load());
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "Error while disconnecting the client", e);
            }
        });

        /*
          We put a trigger event when the user click on the button
          to remove the confirmation and shadow Panes.
         */
        refuseButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane.setVisible(false);
        });

        /*
          We put a trigger event when the user click outside the confirmationPane
          to do the same as the refuseButton.
         */
        shadowPane.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane.setVisible(false);
        });

    }
}