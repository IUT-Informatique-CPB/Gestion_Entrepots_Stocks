package com.gstock.client.controller;

import com.gstock.client.Main;
import com.gstock.client.model.Model;
import com.gstock.tools.Request;
import com.gstock.tools.Tool;
import com.gstock.tools.article.ArticleCategory;
import com.gstock.tools.order.Order;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * PickerModification1Controller is the class that handles all events in the first modification menu.
 * It implements Initializable in order to use FXML components.
 */
public class PickerModification1Controller implements Initializable {

    /**
     * confirmationPane is a Java FX Pane component.
     * It is used to get a confirmation from the user to leave the current menu.
     */
    @FXML private Pane confirmationPane;

    /**
     * shadowPane is a Java FX Pane component.
     * It is used to add a "shadow" effect behind the confirmation pane.
     */
    @FXML private Pane shadowPane;



    /**
     * modifyButton is a Java FX Button component.
     * It is use by the user to confirm he wants to modify the order he selected.
     */
    @FXML private Button modifyButton;

    /**
     * pickerMenuButton is a Java FX Button component.
     * It is used to return to the picker menu.
     */
    @FXML private Button pickerMenuButton;

    /**
     * disconnectButton is a Java FX Button component.
     * It is used to disconnect the user from the server.
     */
    @FXML private Button disconnectButton;

    /**
     * acceptButton is a Java FX Button component.
     * It is used to accept what is said in the confirmationPane.
     */
    @FXML private Button acceptButton;

    /**
     * refuse is a Java FX Button component.
     * It is used to refuse what is said in the confirmationPane.
     */
    @FXML private Button refuseButton;



    /**
     * loginLabel is a Java FX Button component.
     * It is used to print the current user's login.
     */
    @FXML private Label loginLabel;

    /**
     * warehouseLabel is a Java FX Button component.
     * It is used to print the current warehouse.
     */
    @FXML private Label warehouseLabel;



    /**
     * orderListView is a Java FX ListView component.
     * It is used to show all the commands that need to be prepared.
     */
    @FXML private ListView<Order> orderListView;

    /**
     * scene is a JavaFX Scene component.
     * It is used to show the articles of the selected order.
     */
    @FXML private ListView<ArticleCategory> articleListView;



    /**
     * scene is a JavaFX Scene component.
     * It is used to add components to the current scene.
     */
    private final Scene scene;



    /**
     * requestDone is a boolean.
     * It is used to mark when all the requests of
     * the page are done
     */
    private boolean requestDone = true;

    private Logger LOGGER = Logger.getLogger( this.getClass().getName() );

    /**
     * Default constructor of the PickerModification1Controller class.
     *
     * @see PickerModification1Controller
     */
    PickerModification1Controller() {
        this.scene = Main.scene;
    }

    /**
     * Method used to initialize all actions and events that can be triggered
     * when the user do an action.
     *
     * @param location Unused parameter
     * @param resources Unused parameter
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {

        loginLabel.setText(Model.getCurrentUser().getUsername());
        warehouseLabel.setText(Model.getCurrentUser().getDeposit().getName());

        /*
          This thread is used to fill the orderListView.
          We ask the "GCLC" request to get all the orders that aren't completed
          @see Request
         */
        Thread thread = new Thread(() -> {
            Request t = new Request("GCLC", null);
            Socket socket = Model.getSocket();

            try {
                ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                outputStream.writeObject(t);

                ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                Request response = (Request) inputStream.readObject();

                ArrayList<Order> orders = (ArrayList<Order>) response.getData();

                if (orders != null) {
                    Platform.runLater(() -> orderListView.getItems().addAll(orders));
                }
            } catch (IOException | ClassNotFoundException e) {
                LOGGER.log(Level.SEVERE, "Error using the request \"GCLC\" request in the operator picker modification 1 menu", e);
            }
        });
        thread.start();

        /*
          This thread is used to fill the articleListView.
          We ask the "GCLA" request to get all the articles of the selected order.
          @see Request
         */
        orderListView.setOnMouseClicked(event -> {
            if (orderListView.getSelectionModel().getSelectedItem() != null) {
                if (requestDone) {
                    Thread subThread = new Thread(() -> {
                        requestDone = false;
                        Request t = new Request("GCLA", orderListView.getSelectionModel().getSelectedItem().getId());
                        Socket socket = Model.getSocket();

                        try {
                            ObjectOutputStream outputStream = new ObjectOutputStream(socket.getOutputStream());
                            outputStream.writeObject(t);

                            ObjectInputStream inputStream = new ObjectInputStream(socket.getInputStream());
                            Request response = (Request) inputStream.readObject();

                            Set<ArticleCategory> articles = ((HashMap<ArticleCategory, Integer>) response.getData()).keySet();

                            Platform.runLater(() -> {
                                articleListView.getItems().clear();
                                articleListView.getItems().addAll(articles);
                            });
                        } catch (IOException | ClassNotFoundException e) {
                            LOGGER.log(Level.SEVERE, "Error using the request \"GCLA\" request in the picker modification 1 menu", e);
                        }
                        requestDone = true;
                    });
                    subThread.start();
                }
            }
        });

        /*
          We put a trigger event when the user click on the button
          to move to the second modification menu.
         */
        modifyButton.setOnMouseClicked(event -> {
            if (orderListView.getSelectionModel().getSelectedItem() != null && requestDone) {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interfaces/Picker/ModificationCommande2.fxml"));
                fxmlLoader.setController(new PickerModification2Controller(orderListView.getSelectionModel().getSelectedItem()));
                try {
                    scene.setRoot(fxmlLoader.load());
                } catch (IOException e) {
                    LOGGER.log(Level.SEVERE, "Error while moving from the picker modification 1 menu to the picker modification 2 menu", e);
                }
            }
        });

        /*
          We put a trigger event when the user click on the button
          to move to the picker menu.
         */
        pickerMenuButton.setOnMouseClicked(event -> {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interfaces/Picker/Picker.fxml"));
            fxmlLoader.setController(new PickerController());
            try {
                scene.setRoot(fxmlLoader.load());
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "Error while moving from the picker modification 1 menu to the picker menu", e);
            }
        });

        /*
          We put a trigger event when the user click on the button
          to set the confirmation and shadow Panes visible.
         */
        disconnectButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(true);
            confirmationPane.setVisible(true);
        });

        /*
          We put a trigger event when the user click on the button
          to disconnect : request to the server to disconnect,
          and we move to the connection menu.
         */
        acceptButton.setOnMouseClicked(event -> {
            try {
                // request to the server to disconnect
                Model.disconnectUser();
                // go to the main menu
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("interfaces/Connexion.fxml"));
                fxmlLoader.setController(new ConnectionController());
                scene.setRoot(fxmlLoader.load());
            } catch (IOException e) {
                LOGGER.log(Level.SEVERE, "Error while disconnecting the client", e);
            }
        });

        /*
          We put a trigger event when the user click on the button
          to remove the confirmation and shadow Panes.
         */
        refuseButton.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane.setVisible(false);
        });

        /*
          We put a trigger event when the user click outside the confirmationPane
          to do the same as the refuseButton.
         */
        shadowPane.setOnMouseClicked(event -> {
            shadowPane.setVisible(false);
            confirmationPane.setVisible(false);
        });

    }
}