package com.gstock.tools.article;

import com.gstock.tools.address.Address;

public class ArticleToPick {
    private ArticleCategory article = null;
    private Address address = null;
    private Integer quantity = null;

    public ArticleToPick(ArticleCategory article, Address address, int quantity) {

        this.article = article;
        this.address = address;
        this.quantity = quantity;
    }

    public ArticleCategory getArticle() {
        return article;
    }

    public Address getAddress() {
        return address;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setArticle(ArticleCategory article) {
        this.article = article;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return address + " | Quantité : " + quantity + " | Nom Article : " + article;
    }
}
