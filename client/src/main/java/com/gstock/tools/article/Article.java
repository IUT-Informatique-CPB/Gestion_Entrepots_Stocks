package com.gstock.tools.article;

import java.io.Serializable;

public class Article implements Serializable, Comparable<Article> {
    //REAL ATTRIBUTES
    private Integer id;
    private Integer customsNumber;
    private Integer serialNumber;
    private String expirationDate;
    private String receiptDate;
    private ArticleCategory articleCategory;

    public Article() {}

    public Article(Integer customsNumber, Integer serialNumber, String expirationDate, String receiptDate, ArticleCategory articleCategory) {
        this.customsNumber = customsNumber;
        this.serialNumber = serialNumber;
        this.expirationDate = expirationDate;
        this.receiptDate = receiptDate;
        this.articleCategory = articleCategory;
    }

    public Article(int id, Integer customsNumber, Integer serialNumber, String expirationDate, String receiptDate, ArticleCategory articleCategory) {
        this.id = id;
        this.customsNumber = customsNumber;
        this.serialNumber = serialNumber;
        this.expirationDate = expirationDate;
        this.receiptDate = receiptDate;
        this.articleCategory = articleCategory;
    }

    public String show() {
        StringBuilder str = new StringBuilder();
        str.append('[').append(this.getClass().getSimpleName()).append("] { ");
        str.append("id='").append(id).append("', ");
        str.append("customsNumber='").append(customsNumber).append("', ");
        str.append("serialNumber='").append(serialNumber).append("', ");
        str.append("expirationDate='").append(expirationDate).append("', ");
        str.append("receiptDate='").append(receiptDate).append("', ");
        str.append("articleCategory (id)='").append(articleCategory.show()).append("' };");
        return str.toString();
    }

    @Override
    public String toString() {
        return articleCategory.getName();
    }

    public int getId() {
        return id;
    }

    public ArticleCategory getArticleCategory() {
        return articleCategory;
    }

    public Integer getCustomsNumber() {
        return customsNumber;
    }

    public Integer getSerialNumber() {
        return serialNumber;
    }

    public String getExpirationDate() {
        return expirationDate;
    }

    public String getReceiptDate() {
        return receiptDate;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setArticleCategory(ArticleCategory articleCategory) {
        this.articleCategory = articleCategory;
    }

    public void setCustomsNumber(Integer customsNumber) {
        this.customsNumber = customsNumber;
    }

    public void setExpirationDate(String expirationDate) {
        this.expirationDate = expirationDate;
    }

    public void setReceiptDate(String receiptDate) {
        this.receiptDate = receiptDate;
    }

    public void setSerialNumber(Integer serialNumber) {
        this.serialNumber = serialNumber;
    }

    @Override
    public int compareTo(Article o) {
        return id - o.id;
    }
}