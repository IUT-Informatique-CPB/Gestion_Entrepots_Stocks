package com.gstock.tools.article;

import java.io.Serializable;

public class ArticleCategory implements Serializable, Comparable<ArticleCategory> {

    private int id;
    private String barCode;
    private String name;
    private int maxPalette;
    private int maxPaletteUSA;
    private int maxPaletteEurope;

    public ArticleCategory(String barCode, String name, int maxPalette, int maxPaletteUSA, int maxPaletteEurope){
        this.barCode = barCode;
        this.name = name;
        this.maxPalette = maxPalette;
        this.maxPaletteUSA = maxPaletteUSA;
        this.maxPaletteEurope = maxPaletteEurope;
    }

    public ArticleCategory(int id, String barCode, String name, int maxPalette, int maxPaletteUSA, int maxPaletteEurope){
        this.id = id;
        this.barCode = barCode;
        this.name = name;
        this.maxPalette = maxPalette;
        this.maxPaletteUSA = maxPaletteUSA;
        this.maxPaletteEurope = maxPaletteEurope;
    }

    public String show() {
        StringBuilder str = new StringBuilder();
        str.append('[').append(this.getClass().getSimpleName()).append("] { ");
        str.append("id='").append(id).append("', ");
        str.append("name='").append(name).append("', ");
        str.append("barcode='").append(barCode).append("', ");
        str.append("maxPalette='").append(maxPalette).append("', ");
        str.append("maxPaletteUSA='").append(maxPaletteUSA).append("', ");
        str.append("maxPaletteEurope='").append(maxPaletteEurope).append("' };");
        return str.toString();
    }

    @Override
    public String toString() {
        return name;
    }

    public String getBarCode() {
        return barCode;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getMaxPalette() {
        return maxPalette;
    }

    public int getMaxPaletteEurope() {
        return maxPaletteEurope;
    }

    public int getMaxPaletteUSA() {
        return maxPaletteUSA;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMaxPalette(int maxPalette) {
        this.maxPalette = maxPalette;
    }

    public void setMaxPaletteEurope(int maxPaletteEurope) {
        this.maxPaletteEurope = maxPaletteEurope;
    }

    public void setMaxPaletteUSA(int maxPaletteUSA) {
        this.maxPaletteUSA = maxPaletteUSA;
    }

    @Override
    public int compareTo(ArticleCategory o) {
        return id - o.id;
    }
}
