package com.gstock.tools.address;

/**
 * Created by scolin002 on 27/03/18.
 */
public class AddressWithQuantity {
    private Address address = null;
    private Integer quantity = 0;

    public AddressWithQuantity(Address address, Integer quantity) {
        this.address = address;
        this.quantity = quantity;
    }


    public Integer getQuantity() {
        return quantity;
    }
    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Address getAddress() {
        return address;
    }
    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return address.toString() + " | Quantity : " + quantity.toString();
    }
}
