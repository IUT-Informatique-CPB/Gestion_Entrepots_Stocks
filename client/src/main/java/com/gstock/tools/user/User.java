package com.gstock.tools.user;

import com.gstock.tools.deposit.Deposit;

import java.io.Serializable;

public class User implements Serializable {

    //REAL ATTRIBUTES
    private int id;
    private String username;
    private String login;
    private Deposit deposit;

    public User(int id, String username, String login, Deposit deposit) {
        this.id = id;
        this.username = username;
        this.login = login;
        this.deposit = deposit;
    }

    public int getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public Deposit getDeposit() {
        return deposit;
    }

    public String getLogin() {
        return login;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setDeposit(Deposit deposit) {
        this.deposit = deposit;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String show() {
        StringBuilder str = new StringBuilder();
        str.append('[').append(this.getClass().getSimpleName()).append("] { ");
        str.append("id='").append(id).append("', ");
        str.append("username='").append(username).append("', ");
        str.append("login='").append(login).append("', ");
        str.append("deposit='").append(deposit).append("' };");
        return str.toString();
    }

    @Override
    public String toString() {
        return username;
    }
}
