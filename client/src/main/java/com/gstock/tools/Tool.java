package com.gstock.tools;

import javafx.scene.control.TextField;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Tool {
    public static boolean debug = false;

    public static String getStackTrace(Exception e) {
        StringWriter output = new StringWriter();
        e.printStackTrace(new PrintWriter(output));
        if (Tool.debug) {
            Logger.getLogger(Tool.class.getName()).log(Level.SEVERE, output.getBuffer().toString());
            System.exit(2);
        }
        return output.getBuffer().toString();
    }

    /**
     * This method transform a TextField as a numeric Field,
     * meaning that you can only enter numbers in it.
     * @param textField the texfield you want to transform
     */
    public static void setAsNumericField(TextField textField) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                textField.setText(newValue.replaceAll("[^\\d]", ""));
            }
        });
    }

    /**
     * This method transform a TextField as a numeric Field,
     * meaning that you can only enter numbers in it.
     * This method add as well a maximum and a minimum value to the number.
     * @param textField the texfield you want to transform
     * @param min minimum value
     * @param max maximum value
     */
    public static void setAsNumericField(TextField textField, int min, int max) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                textField.setText(newValue.replaceAll("[^\\d]", ""));
            }
            try {
                int value = Integer.parseInt(newValue);
                if (value < min)
                    textField.setText(String.valueOf(min));
                else if (value > max)
                    textField.setText(String.valueOf(max));
            }
            catch (NumberFormatException e) {
                textField.setText(String.valueOf(min));
            }
            catch (IllegalArgumentException e) {

            }
        });
    }

    /**
     * This method set a maximum digit for the value of the text field
     * @param textField the texfield you want to transform
     * @param maxDigit the amount of the digit you want
     */
    public static void setMaxDigit(TextField textField, int maxDigit) {
        textField.textProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue.length() > maxDigit) {
                textField.setText(oldValue);
            }
        });
    }
}
