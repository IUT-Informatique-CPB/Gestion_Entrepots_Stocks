package com.gstock.tools;

import java.io.Serializable;

public class Request implements Serializable {
    private String parameters;
    private Object data;

    public String getParameters() {
        return parameters;
    }

    public Object getData() {
        return data;
    }

    public Request(String parameters, Object data) {
        this.parameters = parameters;
        this.data = data;
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append('[').append(this.getClass().getSimpleName()).append("] { ");
        str.append("parameters='").append(parameters).append("', ");
        str.append("data='").append(data.toString()).append("', ");
        return str.toString();
    }
}
