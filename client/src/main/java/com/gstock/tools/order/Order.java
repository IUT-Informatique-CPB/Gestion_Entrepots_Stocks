package com.gstock.tools.order;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Order implements Serializable {
    //REAL ATTRIBUTES
    private int id;
    private String barCode;
    private String date;
    private Boolean completed;
    private Integer pickerId;
    private Integer operatorId;
    private Integer providerId;

    private Integer customerId;
    private Map<Integer, Integer> articles;

    public Order() {
        articles = new HashMap<>();
    }

    public Order(int id, String barCode, String date, Integer pickerId, Integer operatorId, Integer providerId, Integer customerId,
                 Boolean completed, Map<Integer, Integer> articles) {
        this.id = id;
        this.barCode = barCode;
        this.date = date;
        this.pickerId = pickerId;
        this.operatorId = operatorId;
        this.providerId = providerId;
        this.customerId = customerId;
        this.completed = completed;
        this.articles = articles;
    }

    public Map<Integer, Integer> getArticles() {
        return articles;
    }

    public void setArticles(Map<Integer, Integer> articles) {
        this.articles = articles;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Boolean getCompleted() {
        return completed;
    }

    public String getBarCode() {
        return barCode;
    }

    public void setBarCode(String barCode) {
        this.barCode = barCode;
    }

    public void setCompleted(Boolean completed) {
        this.completed = completed;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public Integer getOperatorId() {
        return operatorId;
    }

    public Integer getPickerId() {
        return pickerId;
    }

    public Integer getProviderId() {
        return providerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public void setOperatorId(Integer operatorId) {
        this.operatorId = operatorId;
    }

    public void setPickerId(Integer pickerId) {
        this.pickerId = pickerId;
    }

    public void setProviderId(Integer providerId) {
        this.providerId = providerId;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String show() {
        StringBuilder str = new StringBuilder();
        str.append('[').append(this.getClass().getSimpleName()).append("] { ");
        str.append("id='").append(id).append("', ");
        str.append("barcode='").append(barCode).append("', ");
        str.append("date='").append(date).append("', ");
        str.append("picker id='").append(pickerId).append("', ");
        str.append("operator id='").append(operatorId).append("', ");
        str.append("provider id='").append(providerId).append("', ");
        str.append("customer id='").append(customerId).append("', ");
        str.append("completed='").append(completed).append("', ");
        str.append("articles='").append(" {\n");
        for (Map.Entry<Integer, Integer> a : articles.entrySet())
            str.append("    ").append(a.getValue()).append(" * ").append("Article [").append(a.getKey().toString()).append("]\n");
        str.append("};");
        return str.toString();
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();
        str.append("id : ").append(id).append("   date : ").append(date);
        return str.toString();
    }
}