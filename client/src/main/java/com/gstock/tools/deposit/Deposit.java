package com.gstock.tools.deposit;

import java.io.Serializable;

public class Deposit implements Serializable {

    //REAL ATTRIBUTES
    private int id;
    private String address;
    private String name;


    public Deposit(int id,String address, String name) {
        this.id = id;
        this.address = address;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getAddress() {
        return address;
    }

    public String getName() {
        return name;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String show() {
        StringBuilder str = new StringBuilder();
        str.append('[').append(this.getClass().getSimpleName()).append("] { ");
        str.append("id='").append(id).append("', ");
        str.append("name='").append(name).append("' };");
        return str.toString();
    }

    @Override
    public String toString() {
        return name;
    }
}
