-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 27, 2018 at 11:05 PM
-- Server version: 5.5.57-MariaDB
-- PHP Version: 5.6.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `PT4`
--
CREATE DATABASE IF NOT EXISTS `PT4` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `PT4`;

-- --------------------------------------------------------

--
-- Table structure for table `ADDRESS`
--

CREATE TABLE `ADDRESS` (
  `id` int(11) NOT NULL,
  `Shop_id` int(11) NOT NULL,
  `Driveway_Code` int(11) NOT NULL,
  `Rank_Number` int(11) NOT NULL,
  `Position_Number` int(11) NOT NULL,
  `Size` varchar(255) NOT NULL,
  `Bar_Code` varchar(13) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ARTICLE`
--

CREATE TABLE `ARTICLE` (
  `id` int(11) NOT NULL,
  `Customs_Number` int(11) DEFAULT NULL,
  `Serial_Number` int(11) DEFAULT NULL,
  `Expiration_Date` varchar(255) DEFAULT NULL,
  `Receipt_Date` varchar(255) DEFAULT NULL,
  `ArticleCategory_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ARTICLE_CATEGORY`
--

CREATE TABLE `ARTICLE_CATEGORY` (
  `id` int(11) NOT NULL,
  `Bar_Code` varchar(13) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `MaxPalette` int(11) NOT NULL,
  `MaxPaletteUSA` int(11) NOT NULL,
  `MaxPaletteEurope` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `CONTAINS`
--

CREATE TABLE `CONTAINS` (
  `Order_id` int(11) NOT NULL,
  `Article_id` int(11) NOT NULL,
  `Articles_Number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `CUSTOMER`
--

CREATE TABLE `CUSTOMER` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `DEPOSIT`
--

CREATE TABLE `DEPOSIT` (
  `id` int(11) NOT NULL,
  `Address` varchar(255) NOT NULL,
  `Name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `ORDERS`
--

CREATE TABLE `ORDERS` (
  `id` int(11) NOT NULL,
  `Bar_Code` varchar(13) NOT NULL,
  `Date` varchar(255) NOT NULL,
  `Picker_id` int(11) DEFAULT NULL,
  `Operator_id` int(11) DEFAULT NULL,
  `Provider_id` int(11) DEFAULT NULL,
  `Customer_id` int(11) DEFAULT NULL,
  `Completed` tinyint(1) NOT NULL DEFAULT '0',
  `Deposit_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `POSSESSES`
--

CREATE TABLE `POSSESSES` (
  `Article_id` int(11) NOT NULL,
  `Address_id` int(11) NOT NULL,
  `Articles_Number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `PROVIDER`
--

CREATE TABLE `PROVIDER` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `SHOP`
--

CREATE TABLE `SHOP` (
  `id` int(11) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Deposit_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `USER`
--

CREATE TABLE `USER` (
  `id` int(11) NOT NULL,
  `Username` varchar(255) NOT NULL,
  `Login` varchar(255) NOT NULL,
  `Password` varchar(255) NOT NULL,
  `Deposit_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ADDRESS`
--
ALTER TABLE `ADDRESS`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Shop_id` (`Shop_id`);

--
-- Indexes for table `ARTICLE`
--
ALTER TABLE `ARTICLE`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ArticleCategory_id` (`ArticleCategory_id`);

--
-- Indexes for table `ARTICLE_CATEGORY`
--
ALTER TABLE `ARTICLE_CATEGORY`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Bar_Code` (`Bar_Code`);

--
-- Indexes for table `CONTAINS`
--
ALTER TABLE `CONTAINS`
  ADD KEY `Order_id` (`Order_id`),
  ADD KEY `CONTAINS_ibfk_2` (`Article_id`);

--
-- Indexes for table `CUSTOMER`
--
ALTER TABLE `CUSTOMER`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `DEPOSIT`
--
ALTER TABLE `DEPOSIT`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ORDERS`
--
ALTER TABLE `ORDERS`
  ADD PRIMARY KEY (`id`),
  ADD KEY `clé étrangère 4` (`Customer_id`),
  ADD KEY `Deposit_id` (`Deposit_id`),
  ADD KEY `ORDERS_ibfk_2` (`Operator_id`),
  ADD KEY `clé étrangère 3` (`Provider_id`),
  ADD KEY `ORDERS_ibfk_1` (`Picker_id`);

--
-- Indexes for table `POSSESSES`
--
ALTER TABLE `POSSESSES`
  ADD PRIMARY KEY (`Article_id`,`Address_id`),
  ADD KEY `clé étrangère 2` (`Address_id`);

--
-- Indexes for table `PROVIDER`
--
ALTER TABLE `PROVIDER`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `SHOP`
--
ALTER TABLE `SHOP`
  ADD PRIMARY KEY (`id`),
  ADD KEY `Deposit_id` (`Deposit_id`);

--
-- Indexes for table `USER`
--
ALTER TABLE `USER`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Login` (`Login`,`Password`),
  ADD KEY `Deposit_id` (`Deposit_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ADDRESS`
--
ALTER TABLE `ADDRESS`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37091;
--
-- AUTO_INCREMENT for table `ARTICLE`
--
ALTER TABLE `ARTICLE`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `ARTICLE_CATEGORY`
--
ALTER TABLE `ARTICLE_CATEGORY`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `CUSTOMER`
--
ALTER TABLE `CUSTOMER`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `DEPOSIT`
--
ALTER TABLE `DEPOSIT`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `ORDERS`
--
ALTER TABLE `ORDERS`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `PROVIDER`
--
ALTER TABLE `PROVIDER`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `SHOP`
--
ALTER TABLE `SHOP`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `USER`
--
ALTER TABLE `USER`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `ADDRESS`
--
ALTER TABLE `ADDRESS`
  ADD CONSTRAINT `ADDRESS_ibfk_1` FOREIGN KEY (`Shop_id`) REFERENCES `SHOP` (`id`);

--
-- Constraints for table `ARTICLE`
--
ALTER TABLE `ARTICLE`
  ADD CONSTRAINT `ARTICLE_ibfk_1` FOREIGN KEY (`ArticleCategory_id`) REFERENCES `ARTICLE_CATEGORY` (`id`);

--
-- Constraints for table `CONTAINS`
--
ALTER TABLE `CONTAINS`
  ADD CONSTRAINT `CONTAINS_ibfk_2` FOREIGN KEY (`Article_id`) REFERENCES `ARTICLE_CATEGORY` (`id`),
  ADD CONSTRAINT `CONTAINS_ibfk_1` FOREIGN KEY (`Order_id`) REFERENCES `ORDERS` (`id`);

--
-- Constraints for table `ORDERS`
--
ALTER TABLE `ORDERS`
  ADD CONSTRAINT `ORDERS_ibfk_1` FOREIGN KEY (`Picker_id`) REFERENCES `USER` (`id`),
  ADD CONSTRAINT `clé étrangère 3` FOREIGN KEY (`Provider_id`) REFERENCES `PROVIDER` (`id`),
  ADD CONSTRAINT `clé étrangère 4` FOREIGN KEY (`Customer_id`) REFERENCES `CUSTOMER` (`id`),
  ADD CONSTRAINT `ORDERS_ibfk_2` FOREIGN KEY (`Operator_id`) REFERENCES `USER` (`id`),
  ADD CONSTRAINT `ORDERS_ibfk_3` FOREIGN KEY (`Deposit_id`) REFERENCES `DEPOSIT` (`id`);

--
-- Constraints for table `POSSESSES`
--
ALTER TABLE `POSSESSES`
  ADD CONSTRAINT `clé étrangère 1` FOREIGN KEY (`Article_id`) REFERENCES `ARTICLE` (`id`),
  ADD CONSTRAINT `clé étrangère 2` FOREIGN KEY (`Address_id`) REFERENCES `ADDRESS` (`id`);

--
-- Constraints for table `SHOP`
--
ALTER TABLE `SHOP`
  ADD CONSTRAINT `SHOP_ibfk_1` FOREIGN KEY (`Deposit_id`) REFERENCES `DEPOSIT` (`id`);

--
-- Constraints for table `USER`
--
ALTER TABLE `USER`
  ADD CONSTRAINT `USER_ibfk_1` FOREIGN KEY (`Deposit_id`) REFERENCES `DEPOSIT` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
